export const routesConfig = {
  login: "/login",
  afterLogin: "/workspace",
  afterLogout: "/login",
  afterRegistration: "/login"
};
