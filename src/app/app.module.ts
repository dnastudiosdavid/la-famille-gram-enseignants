import {BrowserModule} from "@angular/platform-browser";
import {LOCALE_ID, NgModule} from "@angular/core";
import {AppComponent} from "./app.component";
import {provideHttpClient, withInterceptorsFromDi} from "@angular/common/http";
import {SharedModule} from "./shared/shared.module";
import {AuthModule} from "./auth/auth.module";
import {AppMaterialModule} from "./app-material.module";
import {RouterModule} from "@angular/router";
import {APP_ROUTES} from "./app.routes";
import {WorkspaceModule} from "./workspace/workspace.module";
import {DragulaModule} from "ng2-dragula";
import {StoreModule} from "@ngrx/store";
import {EffectsModule} from "@ngrx/effects";
import localeCH from "@angular/common/locales/fr-CH";
import localeCHExtra from "@angular/common/locales/extra/fr-CH";
import {registerLocaleData} from "@angular/common";
import {StoreDevtoolsModule} from "@ngrx/store-devtools";
import {CustomSerializer, reducers} from "./store";
import {StoreRouterConnectingModule} from "@ngrx/router-store";

// register locale
registerLocaleData(localeCH, "fr-CH", localeCHExtra);

@NgModule({
  declarations: [
    AppComponent
  ],
  bootstrap: [AppComponent], imports: [BrowserModule,
    SharedModule.forRoot(),
    DragulaModule.forRoot(),
    AuthModule,
    WorkspaceModule,
    AppMaterialModule,
    RouterModule.forRoot(APP_ROUTES, {}),
    StoreModule.forRoot(reducers, {
      runtimeChecks: {
        strictStateImmutability: false,
        strictActionImmutability: false
      }
    }),
    EffectsModule.forRoot([]),
    StoreRouterConnectingModule.forRoot({serializer: CustomSerializer}),
    StoreDevtoolsModule.instrument({
      maxAge: 5,
      connectInZone: true
    })], providers: [
    {provide: LOCALE_ID, useValue: "fr-CH"},
    provideHttpClient(withInterceptorsFromDi())
  ]
})
export class AppModule {
}
