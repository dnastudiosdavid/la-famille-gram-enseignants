import {Routes} from "@angular/router";
import {LoginComponent} from "./auth/login/login.component";
import {UnauthenticatedGuard} from "./auth/unauthenticated.guard";
import {RegisterComponent} from "./auth/register/register.component";
import {WORKSPACE_ROUTES} from "./workspace/workspace.routes";
import {AuthenticatedGuard} from "./auth/authenticated.guard";

export const APP_ROUTES: Routes = [
  {path: "login", component: LoginComponent, canActivate: [UnauthenticatedGuard]},
  {path: "register", component: RegisterComponent, canActivate: [UnauthenticatedGuard]},
  {path: "workspace", children: WORKSPACE_ROUTES, canActivate: [AuthenticatedGuard]},
  {path: "", redirectTo: "login", pathMatch: "full"}
];
