import {AuthConfig} from "angular-oauth2-oidc";
import {environment} from "../../environments/environment";

export const authConfig: AuthConfig = {

  // where to get the access & refresh tokens
  tokenEndpoint: `${environment.authEndpoint}`,

  // the client id
  clientId: "1_63jdx2o4d2sc8ko00kw408go8sso8sgsk00ogwwwss4gggwssk",

  // the client secret
  dummyClientSecret: "om0g6rsmhhwsckwokg8g80k4ssg404sosc808w4k4o4okookg",

  // set the scope for the permissions the client should request
  scope: "",

  requireHttps: environment.production
};
