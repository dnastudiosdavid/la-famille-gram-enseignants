import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {OAuthModule, OAuthStorage} from "angular-oauth2-oidc";
import {environment} from "../../environments/environment";
import {LoginComponent} from "./login/login.component";
import {ReactiveFormsModule} from "@angular/forms";
import {AppMaterialModule} from "../app-material.module";
import {BrowserModule} from "@angular/platform-browser";
import {RouterModule} from "@angular/router";
import {RegisterComponent} from "./register/register.component";
import {SharedModule} from "../shared/shared.module";
import {BrandHeaderComponent} from "./brand-header/brand-header.component";

@NgModule({
  declarations: [
    LoginComponent,
    RegisterComponent,
    BrandHeaderComponent
  ],
  imports: [
    BrowserModule,
    RouterModule,
    CommonModule,
    ReactiveFormsModule,
    OAuthModule.forRoot({
      resourceServer: {
        allowedUrls: [environment.apiEndpoint],
        sendAccessToken: true
      }
    }),
    AppMaterialModule,
    SharedModule
  ],
  providers: [
    {provide: OAuthStorage, useValue: localStorage}
  ],
  exports: [
    LoginComponent
  ]
})
export class AuthModule {
}
