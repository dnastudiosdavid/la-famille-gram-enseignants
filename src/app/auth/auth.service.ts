import {Injectable} from "@angular/core";
import {OAuthService} from "angular-oauth2-oidc";
import {HttpClient} from "@angular/common/http";
import {environment} from "../../environments/environment";
import {Observable, of} from "rxjs";
import {catchError, filter, map} from "rxjs/operators";
import {authConfig} from "./auth.config";
import {fromPromise} from "rxjs/internal/observable/innerFrom";

@Injectable({
  providedIn: "root"
})
export class AuthService {

  get IsLoggedIn(): Observable<boolean> {
    if (this.hasValidAccessToken()) {
      return of(true);
    }

    if (this.oauthService.getRefreshToken()) {
      if (new Date().getTime() >= this.oauthService.getAccessTokenExpiration()) {
        return this.refreshToken();
      }
    }

    return of(false);
  }

  /**
   * Ctor.
   * @param oauthService
   * @param httpClient
   */
  constructor(private oauthService: OAuthService,
              private httpClient: HttpClient) {
    this.configureWithNewConfigApi();
    this.startAutoRefresh();

    this.oauthService.events.pipe(
      filter(e => e.type === "token_received")
    ).subscribe(e => {
      this.startAutoRefresh();
    });
  }

  /**
   * Does the service have a valid access token?
   */
  hasValidAccessToken(): boolean {
    return this.oauthService.hasValidAccessToken();
  }

  /**
   * Registers a new account.
   * @param username
   * @param password
   * @param passwordRepeat
   */
  register(username: string, password: string, passwordRepeat: string): Observable<Object> {
    return this.httpClient.post(`${environment.apiEndpoint}/p/teacher`, {
      email: username,
      plainPassword: {
        first: password,
        second: passwordRepeat
      }
    });
  }

  /**
   * Tries to log the user in.
   * @param username
   * @param password
   */
  login(username: string, password: string): Promise<object> {
    return this.oauthService.fetchTokenUsingPasswordFlow(username, password);
  }

  /**
   * Logs the user out.
   */
  logout() {
    this.oauthService.logOut();
    window.location.reload();
  }

  /**
   * Configure oauth service.
   */
  private configureWithNewConfigApi() {
    this.oauthService.configure(authConfig);
  }

  /**
   * Starts the auto refresh of the token.
   */
  private startAutoRefresh() {
    if (this.oauthService.getRefreshToken()) {
      const expiration = this.oauthService.getAccessTokenExpiration();
      const now: Date = new Date();
      const delay: number = now.getTime() >= expiration ? 0 : expiration - now.getTime();
      setTimeout(async () => {
        this.refreshToken().subscribe();
      }, delay);
    }
  }

  private refreshToken(): Observable<boolean> {
    return fromPromise(this.oauthService.refreshToken())
      .pipe(
        map(obj => !!obj),
        catchError(_ => {
          this.oauthService.logOut();
          return of(false);
        })
      );
  }
}
