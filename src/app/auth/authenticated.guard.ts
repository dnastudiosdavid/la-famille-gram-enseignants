import {Injectable} from "@angular/core";
import {Router} from "@angular/router";
import {AuthService} from "./auth.service";
import {routesConfig} from "../app.config";
import {Observable, of} from "rxjs";
import {switchMap} from "rxjs/operators";

@Injectable({
  providedIn: "root"
})
export class AuthenticatedGuard {

  /**
   * Ctor.
   * @param router
   * @param authService
   */
  constructor(private router: Router,
              private authService: AuthService) {
  }

  canActivate(): Promise<boolean> | Observable<boolean> {
    return this.authService.IsLoggedIn.pipe(
      switchMap(isLoggedIn => isLoggedIn ? of(true) : this.router.navigate([routesConfig.login]))
    );
  }
}
