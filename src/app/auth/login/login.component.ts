import {Component} from "@angular/core";
import {UntypedFormBuilder, Validators} from "@angular/forms";
import {AuthService} from "../auth.service";
import {LoadingService} from "../../shared/loading-spinner/loading.service";
import {Router} from "@angular/router";
import {routesConfig} from "../../app.config";

@Component({
  selector: "app-login",
  templateUrl: "./login.component.html",
  styleUrls: ["./login.component.scss"]
})
export class LoginComponent {

  /**
   * Form controls.
   */
  loginForm = this.fb.group({
    username: ["", [Validators.required, Validators.email]],
    password: ["", Validators.required]
  });

  /**
   * True if the user entered an invalid user / password combination.
   */
  hasError: boolean;

  /**
   * Ctor.
   * @param authService
   * @param loadingService
   * @param router
   * @param fb
   */
  constructor(private authService: AuthService,
              private loadingService: LoadingService,
              private router: Router,
              private fb: UntypedFormBuilder) {
  }

  /**
   * Tries to logs the user in.
   */
  login() {
    this.hasError = false;
    this.loadingService.enable("login");
    this.authService.login(this.loginForm.value.username, this.loginForm.value.password).then(() => {
      this.loadingService.disable("login");
      this.router.navigate([routesConfig.afterLogin]);
    }).catch(() => {
      this.loadingService.disable("login");
      this.hasError = true;
    });
  }
}
