import {Component} from "@angular/core";
import {UntypedFormBuilder, Validators} from "@angular/forms";
import {AuthService} from "../auth.service";
import {UtilsService} from "../../shared/services/utils.service";
import {LoadingService} from "../../shared/loading-spinner/loading.service";
import {Router} from "@angular/router";
import {routesConfig} from "../../app.config";
import {MatSnackBar} from "@angular/material/snack-bar";

@Component({
  selector: "app-register",
  templateUrl: "./register.component.html",
  styleUrls: ["./register.component.scss"]
})
export class RegisterComponent {

  /**
   * Form controls.
   */
  registerForm = this.fb.group({
    username: ["", [Validators.required, Validators.email]],
    password: ["", Validators.required],
    passwordRepeat: ["", Validators.required]
  });

  /**
   * Array of form errors.
   */
  errors: string [];

  /**
   * Ctor.
   * @param authService
   * @param utilsService
   * @param router
   * @param snackBar
   * @param loadingService
   * @param fb
   */
  constructor(private authService: AuthService,
              private utilsService: UtilsService,
              private router: Router,
              private snackBar: MatSnackBar,
              private loadingService: LoadingService,
              private fb: UntypedFormBuilder) {
  }

  /**
   * Registers the user.
   */
  register() {
    this.loadingService.enable("register");
    this.errors = [];
    this.authService.register(this.registerForm.value.username, this.registerForm.value.password, this.registerForm.value.passwordRepeat)
      .subscribe(() => {
        this.loadingService.disable("register");
        this.router.navigate([routesConfig.afterRegistration]);
        this.snackBar.open("Compte créé avec succès! Vous pouvez vous connecter.", null, {
          duration: 7500,
          panelClass: ["snackbar-positive"]
        });
      }, (error) => {
        this.loadingService.disable("register");
        const errors: string[][] = this.utilsService.findNested(error.error.errors, "errors");
        if (errors.length > 0) {
          this.errors = [].concat.apply([], errors);
        }
      });
  }
}
