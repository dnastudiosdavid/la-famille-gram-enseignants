import {Injectable} from "@angular/core";
import {Router} from "@angular/router";
import {AuthService} from "./auth.service";
import {routesConfig} from "../app.config";

@Injectable({
  providedIn: "root"
})
export class UnauthenticatedGuard {

  /**
   * Ctor.
   *
   * @param router
   * @param authService
   */
  constructor(private router: Router,
              private authService: AuthService) {
  }

  canActivate(): boolean | Promise<boolean> {

    if (!this.authService.hasValidAccessToken()) {
      return true;
    }

    // logged in so redirect to root path
    return this.router.navigate([routesConfig.afterLogin]);
  }
}
