import {Component, Input} from "@angular/core";

@Component({
  selector: "app-alert",
  templateUrl: "./alert.component.html",
  styleUrls: ["./alert.component.scss"]
})
export class AlertComponent {

  /**
   * The alert type.
   */
  @Input()
  type: AlertType = AlertType.light;
}

export enum AlertType {
  negative,
  positive,
  warning,
  light
}
