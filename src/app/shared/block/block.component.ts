import {Component, HostBinding, Input} from "@angular/core";

@Component({
  selector: "app-block",
  templateUrl: "./block.component.html",
  styleUrls: ["./block.component.scss"]
})
export class BlockComponent {

  /**
   * Binding to the display attribute.
   */
  @HostBinding("style.display")
  @Input()
  display: string = "block";

  /**
   * True if the block should have padding.
   */
  @Input()
  hasPadding: boolean = true;
}
