import {Component, ViewEncapsulation} from "@angular/core";

/**
 * This component is a confirmation dialog.
 */
@Component({
  selector: "app-confirm-dialog",
  templateUrl: "./confirm-dialog.component.html",
  styleUrls: ["./confirm-dialog.component.scss"],
  encapsulation: ViewEncapsulation.None
})
export class ConfirmDialogComponent {

  /**
   * The displayed message.
   * TODO: extract this message from there.
   * @type {string}
   */
  message = "Cette action peut entrainer la suppression de données.";
}
