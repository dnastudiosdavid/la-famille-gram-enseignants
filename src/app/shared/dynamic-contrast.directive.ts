import {Directive, Inject, OnInit} from "@angular/core";
import {DOCUMENT} from "@angular/common";

@Directive({
  selector: "[appDynamicContrast]",
  host: {
    "[style.filter]": "\"url(#" + DynamicContrastDirective.SvgId + ")\"",
  }
})
export class DynamicContrastDirective implements OnInit {

  private static readonly SvgId = "dynamicBwFilter";

  // credit to: https://miunau.com/posts/dynamic-text-contrast-in-css/
  // tslint:disable-next-line:max-line-length
  private static readonly SvgFilter = "<svg xmlns=\"http://www.w3.org/2000/svg\" version=\"1.1\" height=\"0\" style=\"position:absolute; height:0;\"> <defs> <filter id=\"" + DynamicContrastDirective.SvgId + "\" color-interpolation-filters=\"sRGB\"><feColorMatrix type=\"matrix\" values=\"0.2126 0.7152 0.0722 0 0 0.2126 0.7152 0.0722 0 0 0.2126 0.7152 0.0722 0 0 0 0 0 1 0\"/><feMorphology operator=\"dilate\" radius=\"2\"/><feComponentTransfer> <feFuncR type=\"linear\" slope=\"-255\" intercept=\"128\"/> <feFuncG type=\"linear\" slope=\"-255\" intercept=\"128\"/> <feFuncB type=\"linear\" slope=\"-255\" intercept=\"128\"/> </feComponentTransfer><feComposite operator=\"in\" in2=\"SourceGraphic\"/> </filter> </defs> </svg>";

  constructor(@Inject(DOCUMENT) private document: Document) {
  }

  ngOnInit(): void {
    this.createFilterIfNeeded();
  }

  private createFilterIfNeeded(): void {
    if (!this.document.getElementById(DynamicContrastDirective.SvgId)) {
      this.document.body.insertAdjacentHTML("beforeend", DynamicContrastDirective.SvgFilter);
    }
  }
}
