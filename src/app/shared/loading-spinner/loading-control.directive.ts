import {Directive, HostBinding, Input, OnDestroy, OnInit} from "@angular/core";
import {LoadingService} from "./loading.service";

@Directive({
  selector: "[app-loading-control]"
})
export class LoadingControlDirective implements OnInit, OnDestroy {

  /**
   * Key of the spinner.
   */
  @Input()
  key: string;

  /**
   * If the control is set to reverse, it will
   * hides the component when the loading is
   * enabled.
   */
  @Input()
  reverse: boolean;

  /**
   * Binding to the display attribute.
   */
  @HostBinding("style.display")
  display: string = "none";

  /**
   * Gets the spinner key.
   */
  get Key(): string {
    return this.key;
  }

  /**
   * Constructor of the spinner.
   * @param loadingService
   */
  constructor(private loadingService: LoadingService) {
  }

  /**
   * Registers the control.
   */
  ngOnInit() {
    this.loadingService._register(this);
  }

  /**
   * Raised when the component gets destroyed.
   */
  ngOnDestroy() {
    this.loadingService._unregister(this);
  }

  /**
   * Shows the spinner.
   */
  show() {
    this.display = this.reverse ? "none" : "block";
  }

  /**
   * Hides the spinner.
   */
  hide() {
    this.display = this.reverse ? "block" : "none";
  }
}
