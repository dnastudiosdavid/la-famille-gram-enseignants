import {Injectable} from "@angular/core";
import {LoadingControlDirective} from "./loading-control.directive";

@Injectable({
  providedIn: "root"
})
export class LoadingService {

  /**
   * Cache of registered spinners.
   */
  private spinnerCache = new Set<LoadingControlDirective>();

  /**
   * Sets of enabled keys.
   */
  private enabledKeys = new Set<string>();

  /**
   * Registers a spinner in the cache.
   */
  _register(spinner: LoadingControlDirective) {
    this.spinnerCache.add(spinner);
    if (this.enabledKeys.has(spinner.Key)) {
      spinner.show();
    } else {
      spinner.hide();
    }
  }

  /**
   * Unregisters the given spinner from the cache.
   */
  _unregister(spinnerToRemove: LoadingControlDirective) {
    this.spinnerCache.forEach(spinner => {
      if (spinner === spinnerToRemove) {
        this.spinnerCache.delete(spinner);
      }
    });
  }

  /**
   * Enables all the loaders associated to the given key.
   */
  enable(key: string) {
    this.enabledKeys.add(key);
    this.spinnerCache.forEach(spinner => {
      if (spinner.Key === key) {
        spinner.show();
      }
    });
  }

  /**
   * Disables all the loaders associated to the given key.
   */
  disable(key: string) {
    this.enabledKeys.delete(key);
    this.spinnerCache.forEach(spinner => {
      if (spinner.Key === key) {
        spinner.hide();
      }
    });
  }

  /**
   * Returns the status of the given key.
   */
  isEnabled(key: string) {
    return this.enabledKeys.has(key);
  }
}
