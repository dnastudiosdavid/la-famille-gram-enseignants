import {IActivityAction} from "./activity-action-interface";
import {IActivityExercise} from "./activity-exercise-interface";

export abstract class ActivityAbstract {

  protected readonly id: number;

  protected readonly student: string;

  protected readonly date: Date;

  protected duration: number = 0;

  protected completionRate: number = 0;

  protected errorRate: number = 0;

  protected readonly exercise: IActivityExercise;

  protected readonly exerciseId: string;

  protected actions: IActivityAction[] = [];

  get Id(): number {
    return this.id || 10;
  }

  get Student(): string {
    return this.student;
  }

  get Date(): Date {
    return this.date;
  }

  get Duration(): number {
    return this.duration;
  }

  get CompletionRate(): number {
    return this.completionRate;
  }

  get ErrorRate(): number {
    return this.errorRate;
  }

  get Exercise(): IActivityExercise {
    return this.exercise;
  }

  get ExerciseId(): string {
    return this.exerciseId;
  }

  get Actions(): IActivityAction[] {
    return this.actions;
  }
}
