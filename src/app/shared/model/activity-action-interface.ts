export interface IActivityAction {
  time: Date;
  scope: string;
  action: string;
  outcome: string;
  data: any;
}
