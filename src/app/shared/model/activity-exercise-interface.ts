export interface IActivityExercise {
  name: string;
  code: string;
  id: number;
}
