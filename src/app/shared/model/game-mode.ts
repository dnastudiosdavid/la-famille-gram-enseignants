/**
 * This enum contains the different game modes available.
 */
export enum GameMode {
  Tirettes = "tirettes",
  GramClass = "gram-class",
  Chains = "chains",
  Silhouettes = "silhouettes"
}
