import {ActivityInputBlock} from "./activity-input-block";
import {Type} from "class-transformer";

/**
 * Workaround so we can build with AOT compilation and Angular CLI 1.5.0-1.5.4
 * @see https://github.com/angular/angular-cli/issues/8434
 */
function aib() {
  return ActivityInputBlock;
}

/**
 * This type represents a complete input provided
 * by the student.
 */
export class ActivityInputLine {

  /**
   * @see IsCorrect
   */
  private readonly isCorrect: boolean;

  /**
   * @see TimeTaken
   */
  private readonly timeTaken: number;

  /**
   * @see Blocks
   */
  @Type(aib)
  private readonly blocks: ActivityInputBlock [];

  /**
   * True if this part was considered as correct.
   */
  get IsCorrect(): boolean {
    return this.isCorrect;
  }

  /**
   * Blocks forming this line.
   */
  get Blocks(): ActivityInputBlock [] {
    return this.blocks;
  }

  /**
   * Gets the time taken for this input.
   */
  get TimeTaken(): number {
    return Math.round(this.timeTaken);
  }

  /**
   * Ctor.
   * @param isCorrect
   * @param blocks
   * @param timeTaken
   */
  constructor(isCorrect: boolean,
              blocks: ActivityInputBlock[],
              timeTaken: number) {
    this.isCorrect = isCorrect;
    this.blocks = blocks;
    this.timeTaken = timeTaken;
  }
}
