import {ActivityInputLine} from "./activity-input-line";
import {Type} from "class-transformer";
import {ActivityInputBlock} from "./activity-input-block";

/**
 * Workaround so we can build with AOT compilation and Angular CLI 1.5.0-1.5.4
 * @see https://github.com/angular/angular-cli/issues/8434
 */
function ail() {
  return ActivityInputLine;
}

/**
 * Workaround so we can build with AOT compilation and Angular CLI 1.5.0-1.5.4
 * @see https://github.com/angular/angular-cli/issues/8434
 */
function aib() {
  return ActivityInputBlock;
}

/**
 * This abstract class is a skelton of an activity step.
 */
export class ActivityStep {

  /**
   * @see InputLines
   */
  @Type(ail)
  private readonly inputLines: ActivityInputLine[];

  /**
   * @see InputLines
   */
  @Type(aib)
  private readonly givenLine: ActivityInputBlock[];

  /**
   * Get the instructions (title) of this step
   */
  get Instructions(): string {
    return this.instruction;
  }

  /**
   * All the given input by the student.
   */
  get InputLines(): ActivityInputLine[] {
    return this.inputLines;
  }

  /**
   * All the given input by the student.
   */
  get GivenLine(): ActivityInputBlock[] {
    return this.givenLine;
  }

  /**
   * Ctor.
   * @param {string} instruction
   * @param {number} solutionsAmount
   */
  constructor(private instruction: string,
              private solutionsAmount: number) {
    this.inputLines = [];
  }
}
