/**
 * This class is a template used to quickly build exercise by
 * providing a default structure with multiple sentences.
 */
export class ExerciseTemplate {

  /**
   * @see Name
   */
  private name: string;

  /**
   * @see Name
   */
  private code: string;

  /**
   * The name of this template.
   * @returns {string}
   */
  get Name(): string {
    return this.name;
  }

  /**
   * The code to the exercise.
   * @returns {string}
   */
  get Code(): string {
    return this.code;
  }

  /**
   * Ctor.
   * @param name
   * @param code
   */
  constructor(name: string, code: string) {
    this.name = name;
    this.code = code;
  }
}
