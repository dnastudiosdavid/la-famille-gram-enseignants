import {Sentence} from "./sentence";
import {Exclude, Type} from "class-transformer";
import {LeaderboardRank} from "./leaderboard-rank";
import {SentencesActivity} from "./sentences-activity";

/**
 * Workaround so we can build with AOT compilation and Angular CLI 1.5.0-1.5.4
 * @see https://github.com/angular/angular-cli/issues/8434
 */
function s() {
  return Sentence;
}

/**
 * Workaround so we can build with AOT compilation and Angular CLI 1.5.0-1.5.4
 * @see https://github.com/angular/angular-cli/issues/8434
 */
function l() {
  return LeaderboardRank;
}

/**
 * Workaround so we can build with AOT compilation and Angular CLI 1.5.0-1.5.4
 * @see https://github.com/angular/angular-cli/issues/8434
 */
function a() {
  return SentencesActivity;
}

/**
 * This class holds all data related to an exercise.
 */
export class Exercise {

  /**
   * @see Id
   */
  @Exclude({toPlainOnly: true})
  private id: number;

  /**
   * @see Code
   */
  @Exclude({toPlainOnly: true})
  private code: string;

  /**
   * @see Name
   */
  private name: string;

  /**
   * @see Language
   */
  private language: string;

  /**
   * @see AcceptsLeaderboard
   */
  private acceptsLeaderboard: boolean;

  /**
   * @see IsLocked
   */
  @Exclude()
  private isLocked: boolean;

  /**
   * TODO: this has nothing to to with the model (domain) this is more an application logic concern
   * @see IsLoaded
   */
  @Exclude()
  private isLoaded: boolean;

  /**
   * @see Sentences
   */
  @Type(s)
  private sentences: Sentence[];

  /**
   * We don't use the scores ATM.
   */
  @Exclude({toPlainOnly: true})
  @Type(l)
  private scores: LeaderboardRank [];

  /**
   * List of activities.
   */
  @Exclude({toPlainOnly: true})
  @Type(a)
  private activities: SentencesActivity [];

  /**
   * @see Modes
   */
  private modes: string[];

  /**
   * The id of this exercise.
   * @returns {string}
   */
  get Id(): number {
    return this.id;
  }

  /**
   * The generated code for this exercise.
   * @returns {string}
   */
  get Code(): string {
    return this.code;
  }

  /**
   * The exercise's language
   */
  get Language(): string {
    return this.language;
  }

  /**
   * The name of the exercise.
   */
  get Name(): string {
    return this.name;
  }

  /**
   * Sets the name of this exercise.
   */
  set Name(value: string) {
    this.name = value;
  }

  /**
   * The array of sentences contained by the exercise.
   */
  get Sentences(): Sentence[] {
    return this.sentences;
  }

  /**
   * Sets the sentences.
   */
  set Sentences(value: Sentence[]) {
    this.sentences = value;
  }

  /**
   * The array of modes.
   */
  get Modes(): string [] {
    return this.modes;
  }

  /**
   * Returns true if the exercise is protected (comes from the defaults)
   */
  get IsLocked(): boolean {
    return this.isLocked;
  }

  /**
   * Returns true if the exercise is new (not been saved yet)
   */
  get IsNew(): boolean {
    return this.code === undefined;
  }

  /**
   * Returns true if the exercise is loaded
   */
  get IsLoaded(): boolean {
    return this.isLoaded;
  }

  /**
   * Sets the loaded flag.
   */
  set IsLoaded(value: boolean) {
    this.isLoaded = value;
  }

  /**
   * Does this exercise accepts a leaderboard ?
   */
  get AcceptsLeaderboard(): boolean {
    return this.acceptsLeaderboard;
  }

  /**
   * Gets all the activities of the exercise.
   */
  get Activities(): SentencesActivity [] {
    return this.activities;
  }

  /**
   * Initializes a new instance of Exercise.
   */
  constructor(name: string, language: string, modes: string[] = ["tirettes", "chains", "gram-class", "silhouettes"], acceptsLeaderboard: boolean = true) {
    this.language = language;
    this.isLocked = false;
    this.isLoaded = true;
    this.name = name;
    this.sentences = [];
    this.modes = modes;
    this.acceptsLeaderboard = acceptsLeaderboard;
  }

  /**
   * Clones the given exercise.
   */
  static clone(source: Exercise): Exercise {
    const clone: Exercise = new Exercise(source.Name, source.Language, source.Modes, source.AcceptsLeaderboard);
    clone.isLoaded = source.isLoaded;
    clone.isLocked = source.isLocked;
    clone.sentences = source.Sentences;
    return clone;
  }

  /**
   * Adds a sentence to the exercise.
   * @param {Sentence} sentence
   */
  addSentence(sentence: Sentence) {
    this.sentences.push(sentence);
  }

  /**
   * Removes the given sentence from the list.
   * @param {Sentence} sentence
   */
  removeSentence(sentence: Sentence) {
    const index: number = this.sentences.indexOf(sentence);
    if (index !== -1) {
      this.sentences.splice(index, 1);
    }
  }

  /**
   * Locks the leaderboard. The lock only happens
   * on the client side, the server will always
   * accept new scores.
   */
  lockLeaderboard() {
    this.acceptsLeaderboard = false;
  }

  /**
   * Unlocks the leaderboard so that players
   * can register news scores.
   */
  unlockLeaderboard() {
    this.acceptsLeaderboard = true;
  }

  /**
   * Locks the exercise (meaning it *should* not be
   * editable from the view).
   */
  lock() {
    this.isLocked = true;
  }

  /**
   * Returns true if the exercise has the given mode.
   * @param {string} mode
   * @return {boolean}
   */
  hasMode(mode: string) {
    return this.modes.indexOf(mode) !== -1;
  }

  /**
   * Sets the modes.
   * @param {string[]} modes
   */
  setModes(modes: string[]) {
    this.modes = modes;
  }
}
