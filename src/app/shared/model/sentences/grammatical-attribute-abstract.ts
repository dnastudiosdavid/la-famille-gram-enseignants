import {IGrammaticalAttribute} from "./grammatical-attribute-interface";

/**
 * This is the base class for a grammatical attribute.
 * A grammatical attribute gives properties to a word
 * and its value is used to compare words together to
 * know if they correctly match in a chain in a sentence.
 */
export abstract class AbstractGrammaticalAttribute implements IGrammaticalAttribute {

  /**
   * Holds the value of the attribute.
   */
  protected value: string;

  /**
   * Holds the label displayed to the user.
   */
  protected displayLabel: string;

  /**
   * Initializes a new instance of a grammatical attribute.
   * @param {string} value
   * @param {string} displayLabel
   */
  constructor(value: string, displayLabel: string) {
    this.value = value;
    this.displayLabel = displayLabel;
  }

  /**
   * Gets the string representation of the values.
   */
  getValue(): string {
    return this.value;
  }

  /**
   * Gets the value to display to the user.
   */
  getDisplayLabel(): string {
    return this.displayLabel;
  }
}
