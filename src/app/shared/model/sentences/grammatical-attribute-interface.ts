/**
 * This interface exposes the methods of a
 * grammatical attribute.
 */
export interface IGrammaticalAttribute {

  /**
   * Gets the string representation of the values.
   * @returns {string}
   */
  getValue(): string;

  /**
   * Gets the value to display to the user.
   * @returns {string}
   */
  getDisplayLabel(): string;
}
