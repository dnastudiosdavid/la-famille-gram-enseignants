/**
 * This class represents a grammatical class of a word.
 */
export class GrammaticalClass {

  /**
   * @see Id
   */
  private id: string;

  /**
   * @see Name
   */
  private name: string;

  /**
   * @see RequireChain
   */
  private requireChain: boolean;

  /**
   * @see RequireGender
   */
  private requireGender: boolean;

  /**
   * The default gender.
   */
  private defaultGender: string;

  /**
   * @see RequireNumber
   */
  private requireNumber: boolean;

  /**
   * The default number.
   */
  private defaultNumber: string;

  /**
   * @see RequirePerson
   */
  private requirePerson: boolean;

  /**
   * @see RequireCase
   */
  private requireCase: boolean;

  /**
   * @see DefaultPerson
   */
  private defaultPerson: string;


  /**
   * @see DefaultCase
   */
  private defaultCase: string;

  /**
   * @see IsInvariable
   */
  private isInvariable: boolean;

  /**
   * The identifier of this grammatical class.
   */
  get Id(): string {
    return this.id;
  }

  set Id(value: string) {
    this.id = value;
  }

  /**
   * The name of this grammatical class.
   */
  get Name(): string {
    return this.name;
  }

  set Name(value: string) {
    this.name = value;
  }

  /**
   * Indicates if this class requires a chain
   * of agreement.
   */
  get RequireChain(): boolean {
    return this.requireChain;
  }

  set RequireChain(value: boolean) {
    this.requireChain = value;
  }

  /**
   * Indicates if the class requires a gender.
   */
  get RequireGender(): boolean {
    return this.requireGender;
  }

  set RequireGender(value: boolean) {
    this.requireGender = value;
  }

  /**
   * Indicates if the class requires a number.
   */
  get RequireNumber(): boolean {
    return this.requireNumber;
  }

  set RequireNumber(value: boolean) {
    this.requireNumber = value;
  }

  /**
   * Indicates if the class requires a person.
   */
  get RequirePerson(): boolean {
    return this.requirePerson;
  }

  set RequirePerson(value: boolean) {
    this.requirePerson = value;
  }

  /**
   * Indicates if the class requires a person.
   */
  get RequireCase(): boolean {
    return this.requireCase;
  }

  set RequireCase(value: boolean) {
    this.requireCase = value;
  }

  /**
   * The default person.
   */
  get DefaultPerson(): string {
    return this.defaultPerson;
  }

  set DefaultPerson(value: string) {
    this.defaultPerson = value;
  }

  /**
   * The default case.
   */
  get DefaultCase(): string {
    return this.defaultCase;
  }

  set DefaultCase(value: string) {
    this.defaultCase = value;
  }

  /**
   * The default number.
   */
  get DefaultNumber(): string {
    return this.defaultNumber;
  }

  set DefaultNumber(value: string) {
    this.defaultNumber = value;
  }

  /**
   * The default person.
   */
  get DefaultGender(): string {
    return this.defaultGender;
  }

  set DefaultGender(value: string) {
    this.defaultGender = value;
  }

  /**
   * Indicates if the class is invariable or not.
   */
  get IsInvariable(): boolean {
    return this.isInvariable;
  }

  set IsInvariable(value: boolean) {
    this.isInvariable = value;
  }

  /**
   * Initializes a new instance of a grammatical class.
   */
  public constructor(id: string,
                     name: string,
                     requireChain: boolean,
                     requireGender: boolean,
                     requireNumber: boolean,
                     requirePerson: boolean,
                     requireCase: boolean,
                     defaultGender: string = "",
                     defaultNumber: string = "",
                     defaultPerson: string = "",
                     defaultCase: string = "",
                     isInvariable: boolean = false) {
    this.id = id;
    this.name = name;
    this.requireChain = requireChain;
    this.requireGender = requireGender;
    this.requireNumber = requireNumber;
    this.requirePerson = requirePerson;
    this.requireCase = requireCase;
    this.defaultGender = defaultGender;
    this.defaultNumber = defaultNumber;
    this.defaultPerson = defaultPerson;
    this.defaultCase = defaultCase;
    this.isInvariable = isInvariable;
  }

  /**
   * Compares this class with another.
   *
   * @param {GrammaticalClass} otherClass
   * @return {boolean}
   */
  compare(otherClass: GrammaticalClass): boolean {
    return otherClass.Id === this.id;
  }
}
