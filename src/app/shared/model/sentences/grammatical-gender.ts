import {AbstractGrammaticalAttribute} from "./grammatical-attribute-abstract";

/**
 * This class represents a grammatical gender.
 */
export class GrammaticalGender extends AbstractGrammaticalAttribute {
}
