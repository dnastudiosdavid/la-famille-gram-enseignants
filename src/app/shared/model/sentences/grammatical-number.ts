import {AbstractGrammaticalAttribute} from "./grammatical-attribute-abstract";

/**
 * This class represents a grammatical number.
 */
export class GrammaticalNumber extends AbstractGrammaticalAttribute {
}
