import {AbstractGrammaticalAttribute} from "./grammatical-attribute-abstract";

/**
 * This class represents a grammatical person.
 */
export class GrammaticalPerson extends AbstractGrammaticalAttribute {
}
