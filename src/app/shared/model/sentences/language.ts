import {GrammaticalGender} from "./grammatical-gender";
import {Type} from "class-transformer";
import {GrammaticalClass} from "./grammatical-class";
import {GrammaticalPerson} from "./grammatical-person";
import {GrammaticalNumber} from "./grammatical-number";
import {GrammaticalCase} from "./grammatical-case";

export class Language {
  private name: string;

  private code: string;

  private flag: string;

  private availableModes: string[] = [];

  @Type(() => GrammaticalGender)
  private genders: GrammaticalGender[] = [];

  @Type(() => GrammaticalClass)
  private classes: GrammaticalClass[] = [];

  @Type(() => GrammaticalClass)
  private functions: GrammaticalClass[] = [];

  @Type(() => GrammaticalPerson)
  private persons: GrammaticalPerson[] = [];

  @Type(() => GrammaticalNumber)
  private numbers: GrammaticalNumber[] = [];

  @Type(() => GrammaticalCase)
  private cases: GrammaticalCase[] = [];

  get Name(): string {
    return this.name;
  }

  set Name(value: string) {
    this.name = value;
  }

  get Code(): string {
    return this.code;
  }

  set Code(value: string) {
    this.code = value;
  }

  get Flag(): string {
    return `${this.flag}.svg`;
  }

  set Flag(value: string) {
    this.flag = value;
  }

  get FlagCode(): string {
    return this.flag;
  }

  set FlagCode(value: string) {
    this.flag = value;
  }

  get Genders(): GrammaticalGender[] {
    return this.genders;
  }

  get Classes(): GrammaticalClass[] {
    return this.classes;
  }

  get Functions(): GrammaticalClass[] {
    return this.functions;
  }

  get Persons(): GrammaticalPerson[] {
    return this.persons;
  }

  get Numbers(): GrammaticalNumber[] {
    return this.numbers;
  }

  get Cases(): GrammaticalCase[] {
    return this.cases;
  }

  get AvailableModes(): string[] {
    return this.availableModes;
  }

  set AvailableModes(value: string[]) {
    this.availableModes = value;
  }

  get SupportsCases(): boolean {
    return this.cases.length > 0;
  }
}
