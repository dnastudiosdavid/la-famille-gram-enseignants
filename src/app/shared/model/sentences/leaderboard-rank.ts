/**
 * This entity is responsible for holding data about
 * a player result in the leaderboard.
 */
export class LeaderboardRank {

  /**
   * @see Id
   */
  private id: number;

  /**
   * @see Nickname
   */
  private nickname: string;

  /**
   * @see Score
   */
  private score: number;

  /**
   * @see Mode
   */
  private mode: string;

  /**
   * The rank's Id.
   * @returns {number}
   */
  get Id(): number {
    return this.id;
  }

  /**
   * The player's score.
   * @returns {number}
   */
  get Score(): number {
    return this.score;
  }

  /**
   * The nickname of the player.
   * @returns {string}
   */
  get Nickname(): string {
    return this.nickname;
  }

  /**
   * The mode the score was done for.
   * @returns {string}
   */
  get Mode(): string {
    return this.mode;
  }

  /**
   * Ctor.
   * @param {number} id
   * @param {string} nickname
   * @param {number} score
   * @param {string} mode
   */
  constructor(id: number, nickname: string, score: number, mode: string) {
    this.id = id;
    this.nickname = nickname;
    this.score = score;
    this.mode = mode;
  }

  /**
   * Renames the player.
   * @param nickname
   */
  rename(nickname: string) {
    this.nickname = nickname;
  }
}
