import {Chain} from "./chain";
import {Tirette} from "./tirette";
import {Type} from "class-transformer";

/**
 * Workaround so we can build with AOT compilation and Angular CLI 1.5.0-1.5.4
 * @see https://github.com/angular/angular-cli/issues/8434
 */
function t() {
  return Tirette;
}

/**
 * Workaround so we can build with AOT compilation and Angular CLI 1.5.0-1.5.4
 * @see https://github.com/angular/angular-cli/issues/8434
 */
function c() {
  return Chain;
}

/**
 * This class is a template used to quickly build sentences by
 * providing a default structure with a sentence.
 */
export class SentenceTemplate {

  /**
   * @see Name
   */
  private readonly name: string;

  /**
   * @see Degree
   */
  private degree: string;

  /**
   * @see Difficulty
   */
  private difficulty: string;

  /**
   * @see Tirettes
   */
  @Type(t)
  private readonly tirettes: Tirette[];

  /**
   * @see Chains
   */
  @Type(c)
  private readonly chains: Chain[];

  /**
   * The name of this template.
   * @returns {string}
   */
  get Name(): string {
    return this.name;
  }

  /**
   * The degree of this sentence.
   * @returns {string}
   */
  get Degree(): string {
    return this.degree;
  }

  /**
   * The difficulty corresponding to this sentence.
   * @returns {string}
   */
  get Difficulty(): string {
    return this.difficulty;
  }

  /**
   * The logical chains of agreement contained
   * in this sentence.
   * @returns {Chain[]}
   */
  get Chains(): Chain[] {
    return this.chains;
  }

  /**
   * The list of tirettes of this chain.
   * @returns {Tirette[]}
   */
  get Tirettes(): Tirette[] {
    return this.tirettes;
  }

  /**
   * Ctor.
   * @param name
   * @param degree
   * @param difficulty
   * @param tirettes
   * @param chains
   */
  constructor(name: string, degree: string, difficulty: string, tirettes: Tirette[], chains: Chain[]) {
    this.name = name;
    this.degree = degree;
    this.difficulty = difficulty;
    this.tirettes = tirettes;
    this.chains = chains;
  }
}
