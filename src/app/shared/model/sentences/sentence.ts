import {Chain} from "./chain";
import {Tirette} from "./tirette";
import {Type} from "class-transformer";
import {Word} from "./word";
import {SentenceTemplate} from "./sentence-template";

/**
 * Workaround so we can build with AOT compilation and Angular CLI 1.5.0-1.5.4
 * @see https://github.com/angular/angular-cli/issues/8434
 */
function t() {
  return Tirette;
}

/**
 * Workaround so we can build with AOT compilation and Angular CLI 1.5.0-1.5.4
 * @see https://github.com/angular/angular-cli/issues/8434
 */
function c() {
  return Chain;
}

/**
 * This class describes a sentence to be solved
 * by the player.
 */
export class Sentence {

  /**
   * The maximum chains in a sentence.
   * @type {number}
   */
  static readonly MAX_CHAINS_PER_SENTENCE = 5;

  /**
   * @see Name
   */
  private name: string;

  /**
   * @see Degree
   */
  private degree: string;

  /**
   * @see Difficulty
   */
  private difficulty: string;

  /**
   * @see Tirettes
   */
  @Type(t)
  private tirettes: Tirette[];

  /**
   * @see Chains
   */
  @Type(c)
  private chains: Chain[];

  /**
   * The name (used to identify) of this sentence.
   * @returns {string}
   */
  get Name(): string {
    return this.name;
  }

  /**
   * The degree of this sentence.
   * @returns {string}
   */
  get Degree(): string {
    return this.degree;
  }

  /**
   * The difficulty corresponding to this sentence.
   * @returns {string}
   */
  get Difficulty(): string {
    return this.difficulty;
  }

  /**
   * The logical chains of agreement contained
   * in this sentence.
   * @returns {Chain[]}
   */
  get Chains(): Chain[] {
    return this.chains;
  }

  /**
   * The list of tirettes of this chain.
   * @returns {Tirette[]}
   */
  get Tirettes(): Tirette[] {
    return this.tirettes;
  }

  /**
   * Sets the list of tirettes of this chain.
   * Required for 2-ways databinding
   * @returns {Tirette[]}
   */
  set Tirettes(value: Tirette[]) {
    this.tirettes = value;
  }

  /**
   * Initializes a new instance of a sentence.
   * @param {string} name the name of this sentence
   * @param {string} degree
   * @param {string} difficulty
   * @param {SentenceTemplate} template
   * @constructor
   */
  constructor(name: string, degree: string = null, difficulty: string = null, template: SentenceTemplate = null) {
    this.name = name;
    this.degree = degree;
    this.difficulty = difficulty;
    // the sentence will always have
    // at least one chain of agreement
    this.chains = template ? template.Chains : [];
    this.tirettes = template ? template.Tirettes : [];
  }

  /**
   * Renames the sentence.
   * @param {string} name
   * @param {string} degree
   * @param {string} difficulty
   */
  changeProperties(name: string, degree: string, difficulty: string) {
    this.name = name;
    this.degree = degree;
    this.difficulty = difficulty;
  }

  /**
   * Adds a tirette in the sentence.
   */
  addTirette(tirette: Tirette) {
    this.tirettes.push(tirette);
  }

  /**
   * Removes the given tirette from this sentence.
   * @param {Tirette} tirette
   */
  removeTirette(tirette: Tirette) {
    const index: number = this.tirettes.indexOf(tirette);
    if (index !== -1) {
      this.tirettes.splice(index, 1);
    }
  }

  /**
   * Adds a chain of agreement in the sentence.
   */
  addChain() {
    if (this.chains.length < Sentence.MAX_CHAINS_PER_SENTENCE) {
      this.chains.push(new Chain(this.getNextChainName()));
    }
  }

  /**
   * Adds a chain with a grammatical case.
   */
  addChainWithCase(cases: string[]) {
    if (this.chains.length < Sentence.MAX_CHAINS_PER_SENTENCE) {
      this.chains.push(new Chain(this.getNextChainName(), cases));
    }
  }

  /**
   * Removes a chain agreement from the sentence.
   * @param {Chain} chain
   */
  removeChain() {
    // we will never remove the first chain.
    if (this.chains.length > 1) {
      this.chains.splice(this.chains.length - 1, 1);
    }
  }

  /**
   * Updates a chain and makes sure that the tirettes are updated according to it.
   */
  updateChain(chain: Chain): void {
    for (const tirette of this.tirettes) {
      if (tirette.Chain && tirette.Chain.compare(chain)) {
        tirette.Chain = chain;
      }
    }
  }

  /**
   * Gets all the possible solutions for this sentence.
   * @returns {string[]}
   */
  getCorrectSentences(): string[] {
    const combinations: number[][] = this.getCombinations();
    const solutions: string[] = [];
    for (const combination of combinations) {
      if (this.isCorrect(combination.join(""))) {
        solutions.push(this.getTextByIndexes(combination.join("")));
      }
    }
    return solutions;
  }

  /**
   * Gets the correct combinations.
   * @returns {string[]}
   */
  getCorrectCombinations(): string[] {
    const combinations: number[][] = this.getCombinations();
    const solutions: string[] = [];
    for (const combination of combinations) {
      if (this.isCorrect(combination.join(""))) {
        solutions.push(combination.join(""));
      }
    }
    return solutions;
  }

  /**
   * Gets the words of the given combination of indexes.
   * @param {string} indexes
   * @returns {Word[]}
   */
  getWordsOf(indexes: string): Word[] {
    const words: Word[] = [];
    const splitted: string[] = indexes.split("");
    for (let i = 0; i < splitted.length; i++) {
      const tirette: Tirette = this.tirettes[i];
      words.push(tirette.Words[splitted[i]]);
    }
    return words;
  }

  /**
   * Returns true if the sentence made by the indexes is correct.
   */
  isCorrect(indexes: string): boolean {
    const splitted: string[] = indexes.split("");
    let result = true;
    const chains: any = {};
    // group words by chain
    for (let i = 0; i < splitted.length; i++) {
      const tirette: Tirette = this.tirettes[i];
      if (!tirette.Chain) {
        continue;
      }
      if (!chains.hasOwnProperty(tirette.Chain.Name)) {
        chains[tirette.Chain.Name] = [];
      }
      chains[tirette.Chain.Name].push(tirette.Words[splitted[i]]);
    }
    // compute the result
    for (const chainName in chains) {
      const chain = this.tirettes.find(tir => tir.Chain && tir.Chain.Name === chainName).Chain;
      if (chains.hasOwnProperty(chainName)) {
        let chainResult = true;
        for (const word of chains[chainName]) {
          chainResult = chainResult && word.compare(chains[chainName], chain);
        }
        result = result && chainResult;
      }
    }
    return result;
  }

  /**
   * Gets all the possible combinations of words in the sentence.
   */
  private getCombinations(): number[][] {
    const result: number[][] = [];
    const current: number[] = [];
    // start at [0,0,..]
    for (let i = 0; i < this.tirettes.length; i++) {
      current.push(0);
    }
    const firstTirette: Tirette = this.tirettes [0];
    if (firstTirette) {
      for (let i = 0; i < firstTirette.Words.length; i++) {

        current[0] = i;
        // trick to clone the array and
        // push a new one into the array
        // of combinations
        result.push(current.slice(0));

        if (i === firstTirette.Words.length - 1) {
          for (let j = 1; j < this.tirettes.length; j++) {
            const tirette: Tirette = this.tirettes[j];
            if (current[j] < tirette.Words.length - 1) {
              current[j]++;
              i = -1;
              break;
            } else {
              current[j] = 0;
            }
          }
        }
      }
    }
    return result;
  }

  /**
   * Gets the text value in a sentence given the indexes.
   */
  getTextByIndexes(indexes: string): string {
    const splitted: string[] = indexes.split("");
    const words: string[] = [];
    for (let i = 0; i < splitted.length; i++) {
      const word: Word = this.tirettes[i].Words[splitted[i]];
      if (word) {
        words.push(word.Value);
      }
    }
    return words.join(" ");
  }

  /**
   * Returns the next chain name.
   */
  private getNextChainName(): string {
    return "Chaine " + (this.chains.length + 1);
  }
}
