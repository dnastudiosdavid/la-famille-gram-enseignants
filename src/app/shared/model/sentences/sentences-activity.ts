import {ActivityStep} from "./activity-step";
import {Type} from "class-transformer";
import {List} from "linqts";
import {ActivityInputLine} from "./activity-input-line";
import {ActivityAbstract} from "../activity-abstract";

export class SentencesActivity extends ActivityAbstract {

  private readonly mode: string;

  @Type(() => ActivityStep)
  private readonly steps: ActivityStep [];

  get Mode(): string {
    return this.mode;
  }

  get Steps(): ActivityStep[] {
    return this.steps;
  }

  /**
   * Returns the amount of seconds the user was inactive.
   */
  getInactiveTime(): number {
    return this.duration - new List<ActivityStep>(this.steps).SelectMany(s => new List<ActivityInputLine>(s.InputLines)).Sum(i => i.TimeTaken);
  }
}
