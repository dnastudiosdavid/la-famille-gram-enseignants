import {WordGroupItf} from "./word-group-interface";

export interface ParagraphItf {
  groups: WordGroupItf [];
}
