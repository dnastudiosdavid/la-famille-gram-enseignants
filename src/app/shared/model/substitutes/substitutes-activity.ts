import {WordItf} from "./word-interface";
import {Exclude, Type} from "class-transformer";
import {ActivityAbstract} from "../activity-abstract";
import {SubstitutesUtils} from "./substitutes-utils";

export class SubstitutesActivity extends ActivityAbstract {

  @Exclude()
  private words: WordItf[];

  @Type(() => SubstitutesActivityStep)
  private readonly steps: SubstitutesActivityStep[] = [];

  private readonly text: string;

  get Text(): string {
    return this.text;
  }

  get Steps(): SubstitutesActivityStep[] {
    return this.steps;
  }

  getWordAt(index: number): string {
    if (!this.words || this.words.length === 0) {
      this.words = SubstitutesUtils.flatten(SubstitutesUtils.toStructuredText(this.text));
    }
    return this.words[index] ? this.words[index].value : "";
  }
}

export class SubstitutesActivityStep {

  private readonly time: number = 0;

  @Type(() => SubstitutesActivityStepGroup)
  private readonly groups: SubstitutesActivityStepGroup[] = [];

  get Time(): number {
    return this.time;
  }

  get Groups(): SubstitutesActivityStepGroup[] {
    return this.groups;
  }

  get IsCorrect(): boolean {
    return this.groups.filter(g => !g.IsCorrect).length === 0;
  }

  constructor(time: number,
              groups: SubstitutesActivityStepGroup[]) {
    this.time = time;
    this.groups = groups;
  }

  getCompletionRate(): number {
    return this.groups.reduce((acc, group) => acc + group.getCompletionRate(), 0) / this.groups.length;
  }

  getErrorRate(): number {
    return this.groups.reduce((acc, group) => acc + group.getErrorRate(), 0) / this.groups.length;
  }
}

export class SubstitutesActivityStepGroup {

  get Name(): string {
    return this.name;
  }

  get Color(): string {
    return this.color;
  }

  get Correct(): number [] {
    return this.correct;
  }

  get Missing(): number[] {
    return this.missing;
  }

  get Wrong(): number[] {
    return this.wrong;
  }

  get IsCorrect(): boolean {
    return this.wrong.length === 0 && this.missing.length === 0;
  }

  constructor(private name: string,
              private color: string,
              private correct: number[],
              private wrong: number[],
              private missing: number[]) {
  }

  getCompletionRate(): number {
    return this.correct.length / (this.correct.length + this.missing.length);
  }

  getErrorRate(): number {
    return this.wrong.length / (this.correct.length + this.missing.length);
  }
}
