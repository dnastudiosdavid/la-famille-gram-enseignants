import {ParagraphItf} from "./paragraph-interface";
import {Exclude, Type} from "class-transformer";
import {GroupItf} from "./group-interface";
import {SubstitutesActivity} from "./substitutes-activity";
import {SubstitutesUtils} from "./substitutes-utils";

export class SubstitutesExercise {

  @Exclude({toPlainOnly: true})
  protected id: number;

  @Exclude({toPlainOnly: true})
  protected code: string;

  protected name: string;

  private title: string;

  @Exclude()
  private guidelines: string[] = [];

  @Exclude()
  private sample: string;

  private solution: any = {};

  @Type(() => IChain)
  // TODO: should be renamed to group but this will break existing exercises
  private chains: IChain[] = [];

  private rawText: string = "";

  @Type(() => SubstitutesActivity)
  @Exclude({toPlainOnly: true})
  private activities: SubstitutesActivity[] = [];

  get IsNew(): boolean {
    return !this.id;
  }

  get Id(): number {
    return this.id;
  }

  get Code(): string {
    return this.code;
  }

  set Code(value: string) {
    this.code = value;
  }

  get Name(): string {
    return this.name;
  }

  set Name(value: string) {
    this.name = value;
  }

  get Guidelines(): string[] {
    return this.guidelines;
  }

  get Sample(): string {
    return this.sample;
  }

  get Solution(): any {
    return this.solution;
  }

  set Solution(value: any) {
    this.solution = value;
  }

  get Groups(): IChain[] {
    return this.chains;
  }

  get Text(): string {
    return this.rawText;
  }

  set Text(value: string) {
    this.rawText = value;
  }

  get Title(): string {
    return this.title;
  }

  set Title(value: string) {
    this.title = value;
  }

  get Activities(): SubstitutesActivity[] {
    return this.activities;
  }

  constructor(name: string, title: string) {
    this.name = name;
    this.title = title;
  }

  addGroup(group: GroupItf) {
    if (!this.chains.find(c => c.value === group.value)) {
      this.chains.push(group);
    }
  }

  removeGroup(group: GroupItf) {
    this.chains = this.chains.filter(c => c.value !== group.value);
    delete this.solution[group.value];
  }

  clearSolution() {
    this.solution = {};
  }

  /**
   * Non-words: space, . ! ? - _
   */
  getStructuredText(): ParagraphItf[] {
    return SubstitutesUtils.toStructuredText(this.rawText);
  }
}

export class IChain {
  value: string;
  label: string;
  color: string;
}
