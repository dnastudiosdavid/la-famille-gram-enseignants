import {GroupItf} from "./group-interface";

export interface WordItf {
  value: string;
  // TODO: should be renamed to group but this will break existing exercises therefore a migration will be required
  chain: GroupItf;
  interactable: boolean;
}

