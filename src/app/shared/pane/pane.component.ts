import {Component, Input} from "@angular/core";

@Component({
  selector: "app-pane",
  templateUrl: "./pane.component.html",
  styleUrls: ["./pane.component.scss"]
})
export class PaneComponent {

  /**
   * Width of the pane.
   */
  @Input()
  width: string;
}
