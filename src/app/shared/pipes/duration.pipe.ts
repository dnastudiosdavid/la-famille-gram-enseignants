import {Pipe, PipeTransform} from "@angular/core";

@Pipe({
  name: "duration"
})
export class DurationPipe implements PipeTransform {

  /**
   * Transforms the given value from seconds to H:i
   *
   * @param value
   * @param args
   */
  transform(value: number, args?: any): any {
    if (!value) {
      return `0s`;
    }
    const m: number = Math.floor(value / 60);
    const s: number = Math.round(value % 60);
    return m > 0 ? `${m}m ${s}s` : `${s}s`;
  }
}
