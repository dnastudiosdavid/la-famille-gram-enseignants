import {Injectable} from "@angular/core";
import {IStorageService} from "./storage-service-interface";

/**
 * This service abstracts any kind of storage method
 * and provides a single facade to the app to store
 * key-value pairs.
 */
@Injectable()
export class LocalStorageService implements IStorageService {

  /**
   * Stores the given value assignated
   * to the given key.
   * @param {string} key
   * @param value
   */
  store(key: string, value: any) {
    localStorage.setItem(key, JSON.stringify(value));
  }

  /**
   * Gets the value associated to the given key.
   * @param {string} key
   * @returns {any}
   */
  get(key: string): any {
    return JSON.parse(localStorage.getItem(key)) || null;
  }
}

/**
 * A mocked version of the local storage service.
 */
export class MockedLocalStorageService implements IStorageService {

  /**
   * The object which holds the key / data values.
   */
  private _keyStore: any;

  /**
   * Ctor.
   */
  constructor() {
    this._keyStore = {};
  }

  /**
   * Saves the given value to the key.
   * @param {string} key
   * @param value
   */
  store(key: string, value: any) {
    this._keyStore[key] = value;
  }

  /**
   * Gets the value associated to the given key.
   * @param {string} key
   * @returns any
   */
  get(key: string): any {
    if (this._keyStore.hasOwnProperty(key)) {
      return this._keyStore[key];
    }

    throw new Error("The item you are trying to find does not exist.");
  }
}
