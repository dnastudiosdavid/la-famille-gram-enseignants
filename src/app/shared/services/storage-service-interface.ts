/**
 * This interface describes a key-value pair
 * storage service.
 */
export interface IStorageService {

  /**
   * Stores the given value assignated
   * to the given key.
   * @param {string} key
   * @param value
   */
  store(key: string, value: any);

  /**
   * Gets the value associated to the given key.
   * @param {string} key
   * @returns {any}
   */
  get(key: string): any;
}
