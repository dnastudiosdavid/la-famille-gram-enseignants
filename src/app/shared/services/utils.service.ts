import {Injectable} from "@angular/core";

@Injectable({
  providedIn: "root"
})
export class UtilsService {

  /**
   * Finds nested.
   *
   * @param obj
   * @param key
   * @param memo
   */
  findNested(obj, key, memo = null) {
    let i,
      proto = Object.prototype,
      ts = proto.toString,
      hasOwn = proto.hasOwnProperty.bind(obj);

    if ("[object Array]" !== ts.call(memo)) {
      memo = [];
    }
    for (i in obj) {
      if (hasOwn(i)) {
        if (i === key) {
          memo.push(obj[i]);
        } else if ("[object Array]" === ts.call(obj[i]) || "[object Object]" === ts.call(obj[i])) {
          this.findNested(obj[i], key, memo);
        }
      }
    }
    return memo;
  }
}
