import {ModuleWithProviders, NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {LoadingSpinnerComponent} from "./loading-spinner/loading-spinner.component";
import {LoadingControlDirective} from "./loading-spinner/loading-control.directive";
import {AppMaterialModule} from "../app-material.module";
import {BlockComponent} from "./block/block.component";
import {AlertComponent} from "./alert/alert.component";
import {ConfirmDialogComponent} from "./confirm-dialog/confirm-dialog.component";
import {LocalStorageService} from "./services/local-storage.service";
import {PaneComponent} from "./pane/pane.component";
import {ToolbarComponent} from "./toolbar/toolbar.component";
import {RouterModule} from "@angular/router";
import {SnackbarComponent} from "./snackbar/snackbar.component";
import {FlagComponent} from "./flag/flag.component";
import {DynamicContrastDirective} from "./dynamic-contrast.directive";
import {DurationPipe} from "./pipes/duration.pipe";

@NgModule({
  declarations: [
    LoadingSpinnerComponent,
    LoadingControlDirective,
    BlockComponent,
    AlertComponent,
    ConfirmDialogComponent,
    PaneComponent,
    ToolbarComponent,
    SnackbarComponent,
    FlagComponent,
    DynamicContrastDirective,
    DurationPipe
  ],
  imports: [
    CommonModule,
    AppMaterialModule,
    RouterModule
  ],
  exports: [
    LoadingSpinnerComponent,
    LoadingControlDirective,
    BlockComponent,
    AlertComponent,
    ConfirmDialogComponent,
    PaneComponent,
    ToolbarComponent,
    SnackbarComponent,
    FlagComponent,
    DynamicContrastDirective,
    DurationPipe
  ]
})
export class SharedModule {
  static forRoot(): ModuleWithProviders<SharedModule> {
    return {
      ngModule: SharedModule,
      providers: [LocalStorageService]
    };
  }
}
