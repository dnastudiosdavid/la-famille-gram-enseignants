import {Component, Input} from "@angular/core";

@Component({
  selector: "app-toolbar",
  templateUrl: "./toolbar.component.html",
  styleUrls: ["./toolbar.component.scss"]
})
export class ToolbarComponent {

  /**
   * Where should the router head when clicking the
   * back button?
   */
  @Input()
  backLink: string;
}
