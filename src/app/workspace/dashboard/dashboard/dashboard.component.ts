import {Component, OnInit} from "@angular/core";
import {Store} from "@ngrx/store";
import {Observable} from "rxjs";
import {SentencesActivity} from "../../../shared/model/sentences/sentences-activity";
import * as fromActivity from "../../sentences/activity/store";
import {ActivityActions} from "../../sentences/activity/store";
import * as fromSubstitutes from "../../substitutes/store";
import {SubstitutesActivityActions} from "../../substitutes/store";
import {SubstitutesActivity} from "../../../shared/model/substitutes/substitutes-activity";

@Component({
  selector: "app-dashboard",
  templateUrl: "./dashboard.component.html",
  styleUrls: ["./dashboard.component.scss"]
})
export class DashboardComponent implements OnInit {

  sentencesActivities$: Observable<SentencesActivity[]> = this.sentencesStore.select(fromActivity.getLastActivities);

  substitutesActivities$: Observable<SubstitutesActivity[]> = this.substitutesStore.select(fromSubstitutes.getLastActivities);

  constructor(private sentencesStore: Store<fromActivity.ActivityFeatureState>,
              private substitutesStore: Store<fromActivity.ActivityFeatureState>) {
  }

  /**
   * Initialization.
   */
  ngOnInit() {
    this.sentencesStore.dispatch(new ActivityActions.FetchLast(10));
    this.substitutesStore.dispatch(new SubstitutesActivityActions.FetchLast(10));
  }
}
