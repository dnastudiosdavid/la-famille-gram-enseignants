import {MatSnackBar} from "@angular/material/snack-bar";
import {LoadingService} from "../shared/loading-spinner/loading.service";
import {SnackbarComponent} from "../shared/snackbar/snackbar.component";

/**
 * Base class for effects.
 */
export class AbstractEffects {

  /**
   * Ctor.
   * @param loadingService
   * @param snackBar
   */
  constructor(protected loadingService: LoadingService,
              protected snackBar: MatSnackBar) {
  }

  /**
   * Opens a loading stream.
   * @param key
   */
  protected open(key: string) {
    this.loadingService.enable(key);
  }

  /**
   * Closes a loading stream.
   * @param key
   * @param message
   * @param isSuccess
   */
  protected close(key: string, message: string = null, isSuccess: boolean = true) {
    this.loadingService.disable(key);
    if (message) {
      const type: string = isSuccess ? "positive" : "negative";
      this.snackBar.openFromComponent(SnackbarComponent, {
        data: {message: message},
        panelClass: [`snackbar-${type}`],
        duration: 5000
      });
    }
  }
}
