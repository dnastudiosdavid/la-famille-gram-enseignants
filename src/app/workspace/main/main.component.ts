import {Component} from "@angular/core";
import {routesConfig} from "../../app.config";
import {Router} from "@angular/router";
import {AuthService} from "../../auth/auth.service";

@Component({
  selector: "app-main",
  templateUrl: "./main.component.html",
  styleUrls: ["./main.component.scss"]
})
export class MainComponent {

  /**
   * Ctor.
   *
   * @param router
   * @param authService
   */
  constructor(private router: Router,
              private authService: AuthService) {
  }

  /**
   * Logs out.
   */
  logout() {
    this.authService.logout();
    this.router.navigate([routesConfig.afterLogout]);
  }
}
