import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {ActivityComponent} from "./components/activity/activity.component";
import {AppMaterialModule} from "../../../app-material.module";
import {SharedModule} from "../../../shared/shared.module";
import {DetailComponent} from "./components/detail/detail.component";
import {ListComponent} from "./components/list/list.component";
import {RouterModule} from "@angular/router";
import {StoreModule} from "@ngrx/store";
import {EffectsModule} from "@ngrx/effects";
import {ActivityEffects, reducers} from "./store";
import {ActivityResolver} from "./activity.resolver";

@NgModule({
  declarations: [ActivityComponent, DetailComponent, ListComponent],
  imports: [
    CommonModule,
    SharedModule,
    AppMaterialModule,
    RouterModule,
    StoreModule.forFeature("activityFeature", reducers),
    EffectsModule.forFeature([ActivityEffects])
  ],
  providers: [
    ActivityResolver
  ]
})
export class ActivityModule {
}
