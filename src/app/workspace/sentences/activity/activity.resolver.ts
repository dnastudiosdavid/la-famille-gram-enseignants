import {Injectable} from "@angular/core";

import {Store} from "@ngrx/store";
import {take} from "rxjs/operators";

import * as fromRoot from "../../../store";
import * as fromActivity from "./store";

@Injectable()
export class ActivityResolver {

  /**
   * Ctor.
   * @param store
   */
  constructor(private store: Store<fromRoot.State>) {
  }

  resolve() {
    return this.store.select(state => state.routerReducer.state.params.activityId)
      .pipe(take(1))
      .subscribe(id => this.store.dispatch(new fromActivity.ActivityActions.Select(id)));
  }
}
