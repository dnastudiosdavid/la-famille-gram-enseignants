import {Routes} from "@angular/router";
import {ActivityComponent} from "./components/activity/activity.component";
import {DetailComponent} from "./components/detail/detail.component";
import {ListComponent} from "./components/list/list.component";
import {ActivityResolver} from "./activity.resolver";

export const ACTIVITY_ROUTES: Routes = [
  {
    path: "", component: ActivityComponent, children: [
      {path: "", component: ListComponent},
      {path: "details", component: DetailComponent},
      {path: "details/:activityId", component: DetailComponent, resolve: {data: ActivityResolver}}
    ]
  }
];
