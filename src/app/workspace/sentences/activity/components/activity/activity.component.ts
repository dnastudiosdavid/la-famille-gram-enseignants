import {Component, OnInit} from "@angular/core";
import {Exercise} from "../../../../../shared/model/sentences/exercise";
import {Observable} from "rxjs";
import {select, Store} from "@ngrx/store";
import {ExerciseState} from "../../../exercises/store/state";
import {ExerciseActions} from "../../../exercises/store/exercise.actions";
import {ActivityState} from "../../store/reducers/activity.reducer";
import {Router} from "@angular/router";

@Component({
  selector: "app-activity",
  templateUrl: "./activity.component.html",
  styleUrls: ["./activity.component.scss"]
})
export class ActivityComponent implements OnInit {

  /**
   * All the exercises.
   */
  exercises$: Observable<Exercise []> = this.exerciseStore.pipe(select((state: any) => state.exerciseFeature.exercise.exercises));

  /**
   * The selected exercise.
   */
  selectedExercise$: Observable<Exercise> = this.exerciseStore.pipe(select((state: any) => state.exerciseFeature.exercise.selected));

  /**
   * Ctor.
   */
  constructor(private exerciseStore: Store<ExerciseState>,
              private activityStore: Store<ActivityState>,
              private router: Router) {
  }

  /**
   * Initialization.
   */
  ngOnInit() {
    this.fetchExercises();
  }

  /**
   * Selects the given exercise.
   * @param {Exercise} exercise
   */
  select(exercise: Exercise) {
    this.exerciseStore.dispatch(new ExerciseActions.Select(exercise));
    this.router.navigate(["/workspace/activity"]);
  }

  /**
   * Gets the exercises of the user.
   */
  fetchExercises() {
    this.exerciseStore.dispatch(new ExerciseActions.FetchAll());
  }
}
