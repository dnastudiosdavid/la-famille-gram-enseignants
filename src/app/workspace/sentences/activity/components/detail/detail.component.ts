import {Component, ElementRef, OnDestroy, OnInit, ViewChild} from "@angular/core";
import {Store} from "@ngrx/store";
import {ActivityState} from "../../store/reducers/activity.reducer";
import {delay, Observable, Subscription} from "rxjs";
import {SentencesActivity} from "../../../../../shared/model/sentences/sentences-activity";
import * as fromActivity from "../../store";
import {filter} from "rxjs/operators";

@Component({
  selector: "app-detail",
  templateUrl: "./detail.component.html",
  styleUrls: ["./detail.component.scss"]
})
export class DetailComponent implements OnInit, OnDestroy {

  /**
   * The selected activity.
   */
  activity$: Observable<SentencesActivity> = this.activityStore.select(fromActivity.getSelectedActivity);

  sub: Subscription;

  @ViewChild("player")
  player: ElementRef;

  /**
   * Ctor.
   * @param activityStore
   */
  constructor(private activityStore: Store<ActivityState>) {
  }

  ngOnInit(): void {
    this.sub = this.activity$
      .pipe(
        filter(a => !!a),
        // TODO: ready event from the container
        delay(1000)
      )
      .subscribe(a => {
        this.player?.nativeElement.contentWindow.postMessage(a, "*");
      });
  }

  ngOnDestroy(): void {
    this.sub?.unsubscribe();
  }
}
