import {Component, OnDestroy, OnInit} from "@angular/core";
import {Observable, Subscription} from "rxjs";
import {Exercise} from "../../../../../shared/model/sentences/exercise";
import {select, Store} from "@ngrx/store";
import {ExerciseState} from "../../../exercises/store/state";
import {SentencesActivity} from "../../../../../shared/model/sentences/sentences-activity";
import {Router} from "@angular/router";
import * as fromStore from "../../store/";
import {ActivityActions} from "../../store/";
import {LanguageService} from "../../../exercises/services/languages.service";
import {Language} from "../../../../../shared/model/sentences/language";
import * as fromActivity from "../../store";

@Component({
  selector: "app-list",
  templateUrl: "./list.component.html",
  styleUrls: ["./list.component.scss"]
})
export class ListComponent implements OnInit, OnDestroy {

  private static readonly ChunkSize = 30;

  /**
   * The selected exercise.
   */
  selectedExercise$: Observable<Exercise> = this.exerciseStore.pipe(select((state: any) => state.exerciseFeature.exercise.selected));
  loadedActivities$: Observable<SentencesActivity[]> = this.activityStore.select(fromActivity.getAllActivities);

  language: Language;

  hasMore: boolean = true;

  private exerciseId: number;

  private activitiesAmount: number = 0;

  private subs: Subscription = new Subscription();

  /**
   * Ctor.
   */
  constructor(private exerciseStore: Store<ExerciseState>,
              private activityStore: Store<fromStore.ActivityFeatureState>,
              private languageService: LanguageService,
              private router: Router) {
  }

  ngOnInit() {
    this.subs.add(this.exerciseStore
      .pipe(
        select((state: any) => state.exerciseFeature.exercise.selected)
      )
      .subscribe(async (exercise: Exercise) => {
        if (exercise) {
          this.language = await this.languageService.getLanguage(exercise.Language).toPromise();
          this.exerciseId = exercise.Id;
          this.loadActivities();
        }
      }));

    this.subs.add(this.loadedActivities$.subscribe(activities => {
      this.hasMore = activities.length > this.activitiesAmount && activities.length - this.activitiesAmount >= ListComponent.ChunkSize;
      this.activitiesAmount = activities.length;
    }));
  }

  ngOnDestroy(): void {
    this.subs.unsubscribe();
  }

  loadActivities(): void {
    this.exerciseStore.dispatch(new ActivityActions.Load(this.exerciseId, this.activitiesAmount, ListComponent.ChunkSize));
  }

  /**
   * Selects the given activity.
   * @param activity
   */
  select(activity: SentencesActivity) {
    void this.router.navigate(["workspace/activity/details", activity.Id]);
  }
}
