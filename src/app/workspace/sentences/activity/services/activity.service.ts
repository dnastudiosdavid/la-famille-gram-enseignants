import {Injectable} from "@angular/core";
import {SentencesActivity} from "../../../../shared/model/sentences/sentences-activity";
import {HttpClient, HttpParams} from "@angular/common/http";
import {Observable} from "rxjs";
import {environment} from "../../../../../environments/environment";
import {map} from "rxjs/operators";
import {plainToInstance} from "class-transformer";

@Injectable({
  providedIn: "root"
})
export class ActivityService {

  /**
   * Ctor.
   * @param httpClient
   */
  constructor(private httpClient: HttpClient) {
  }

  /**
   * Gets a chunk of activities of an exercise.
   */
  getActivities(exerciseId: number, start: number, limit: number): Observable<SentencesActivity[]> {
    return this.httpClient.get<SentencesActivity[]>(`${environment.apiEndpoint}/exercise/${exerciseId}/activity`,
      {params: new HttpParams().set("start", `${start}`).set("limit", `${limit}`)})
      .pipe(
        map(plain => plainToInstance(SentencesActivity, plain))
      );
  }

  /**
   * Gets an activity given its id.
   * @param id
   */
  getActivity(id: number): Observable<SentencesActivity> {
    return this.httpClient.get<SentencesActivity>(`${environment.apiEndpoint}/activity/${id}`)
      .pipe(
        map(plain => plainToInstance(SentencesActivity, plain))
      );
  }
}
