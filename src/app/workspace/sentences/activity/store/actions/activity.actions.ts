import {Action} from "@ngrx/store";
import {SentencesActivity} from "../../../../../shared/model/sentences/sentences-activity";

export namespace ActivityActions {

  export enum ActionTypes {
    FetchLast = "[Activity] Fetch last",
    FetchLastDone = "[Activity] Fetch last done",
    Select = "[Activity] Select",
    SelectDone = "[Activity] Select done",
    Load = "[Activity] Load activities",
    LoadDone = "[Activity] Load activities done",
  }

  export class FetchLast implements Action {
    readonly type = ActionTypes.FetchLast;

    constructor(public payload: number) {
    }
  }

  export class FetchLastDone implements Action {
    readonly type = ActionTypes.FetchLastDone;

    constructor(public payload: SentencesActivity[]) {
    }
  }

  export class Select implements Action {
    readonly type = ActionTypes.Select;

    constructor(public payload: number) {
    }
  }

  export class Load implements Action {
    readonly type = ActionTypes.Load;

    constructor(public exerciseId: number, public start: number, public limit: number) {
    }
  }

  export class LoadDone implements Action {
    readonly type = ActionTypes.LoadDone;

    constructor(public exerciseId, public payload: SentencesActivity[]) {
    }
  }

  export class SelectDone implements Action {
    readonly type = ActionTypes.SelectDone;

    constructor(public payload: SentencesActivity) {
    }
  }

  export type Actions = Select | SelectDone | FetchLast | FetchLastDone | Load | LoadDone;
}
