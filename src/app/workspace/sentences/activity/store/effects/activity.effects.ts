import {Injectable} from "@angular/core";
import {ExerciseService} from "../../../exercises/services/exercise.service";
import {LoadingService} from "../../../../../shared/loading-spinner/loading.service";
import {Actions, createEffect, ofType} from "@ngrx/effects";
import {combineLatest, Observable, of} from "rxjs";
import {Action, Store} from "@ngrx/store";
import {filter, map, mergeMap, switchMap, withLatestFrom} from "rxjs/operators";
import {ActivityActions} from "../actions";
import {SentencesActivity} from "../../../../../shared/model/sentences/sentences-activity";
import {MatSnackBar} from "@angular/material/snack-bar";
import {ActivityService} from "../../services/activity.service";
import {ExerciseActions} from "../../../exercises/store/exercise.actions";
import {ExerciseState} from "../../../exercises/store/state";
import {AbstractEffects} from "../../../../effects-abstract";

@Injectable()
export class ActivityEffects extends AbstractEffects {

  /**
   * This effect gets the last activities.
   */
  getLastActivities$: Observable<Action> = createEffect(() => this.actions$
    .pipe(
      ofType(ActivityActions.ActionTypes.FetchLast),
      switchMap((action: ActivityActions.FetchLast) => {
        this.open("activities");
        return this.exerciseService.getActivities(action.payload);
      }),
      map((activities: SentencesActivity []) => {
        this.close("activities");
        return new ActivityActions.FetchLastDone(activities);
      })
    ));

  /**
   * This effect gets the last activities.
   */
  getActivity$: Observable<Action> = createEffect(() => this.actions$
    .pipe(
      ofType(ActivityActions.ActionTypes.Select),
      switchMap((action: ActivityActions.Select) => {
        this.open("activity");
        return this.activityService.getActivity(action.payload);
      }),
      map((activity: SentencesActivity) => {
        this.close("activity");
        return new ActivityActions.SelectDone(activity);
      })
    ));

  /**
   * This effect gets the last activities.
   */
  getActivities$: Observable<Action> = createEffect(() => this.actions$
    .pipe(
      ofType(ActivityActions.ActionTypes.Load),
      mergeMap((action: ActivityActions.Load) => {
        this.open("activities");
        return combineLatest([
          of(action.exerciseId),
          this.activityService.getActivities(action.exerciseId, action.start, action.limit)
        ]);
      }),
      map(([exerciseId, activities]) => {
        this.close("activities");
        return new ActivityActions.LoadDone(exerciseId, activities);
      })
    ));

  /**
   * This effect selects the exercise corresponding
   * to the selected activity.
   */
  selectActivityExercise$: Observable<Action> = createEffect(() => this.actions$
    .pipe(
      ofType(ActivityActions.ActionTypes.SelectDone),
      withLatestFrom(this.exerciseStore$),
      filter(([action, state]: [ActivityActions.SelectDone, any]) => !state.exerciseFeature.exercise.selected || state.exerciseFeature.exercise.selected.Id !== action.payload.Exercise.id),
      map(([action, state]: [ActivityActions.SelectDone, any]) => new ExerciseActions.Select(action.payload.Exercise.id))
    ));

  /**
   * Ctor.
   * @param exerciseService
   * @param activityService
   * @param loadingService
   * @param snackBar
   * @param actions$
   * @param exerciseStore$
   */
  constructor(private exerciseService: ExerciseService,
              private activityService: ActivityService,
              protected loadingService: LoadingService,
              protected snackBar: MatSnackBar,
              private actions$: Actions,
              private exerciseStore$: Store<ExerciseState>) {
    super(loadingService, snackBar);
  }
}
