import {ActivityActions} from "../actions";
import {SentencesActivity} from "../../../../../shared/model/sentences/sentences-activity";

export interface ActivityState {
  exerciseId: number;
  activities: SentencesActivity[];
  lastActivities: SentencesActivity[];
  selectedActivity: SentencesActivity;
}

export const initialState: ActivityState = {
  exerciseId: null,
  activities: [],
  lastActivities: [],
  selectedActivity: null
};

export function activityReducer(state = initialState, action: ActivityActions.Actions): ActivityState {

  switch (action.type) {
    case ActivityActions.ActionTypes.Select:
      return state;

    case ActivityActions.ActionTypes.SelectDone:
      return {
        ...state,
        selectedActivity: action.payload
      };

    case ActivityActions.ActionTypes.FetchLast:
      return state;

    case ActivityActions.ActionTypes.FetchLastDone:
      return {
        ...state,
        lastActivities: action.payload
      };

    case ActivityActions.ActionTypes.Load:
      const sameExercise = state.exerciseId === action.exerciseId;

      return {
        ...state,
        activities: sameExercise ? state.activities : [],
        exerciseId: action.exerciseId
      };

    case ActivityActions.ActionTypes.LoadDone:
      return {
        ...state,
        activities: [...state.activities || [], ...action.payload]
      };

    default:
      return state;
  }
}

export const getLastActivities = (state: ActivityState) => state.lastActivities;
export const getAllActivities = (state: ActivityState) => state.activities;
export const getSelectedActivity = (state: ActivityState) => state.selectedActivity;
