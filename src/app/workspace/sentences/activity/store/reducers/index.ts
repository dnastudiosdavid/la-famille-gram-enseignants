import {ActionReducerMap, createFeatureSelector} from "@ngrx/store";
import * as fromActivity from "./activity.reducer";

export interface ActivityFeatureState {
  activity: fromActivity.ActivityState;
}

export const reducers: ActionReducerMap<ActivityFeatureState> = {
  activity: fromActivity.activityReducer
};

export const getActivityFeatureState = createFeatureSelector<ActivityFeatureState>("activityFeature");
