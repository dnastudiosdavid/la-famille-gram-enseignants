import {createSelector} from "@ngrx/store";
import * as fromFeature from "../reducers";
import * as fromActivity from "../reducers/activity.reducer";

export const getActivityState = createSelector(
  fromFeature.getActivityFeatureState,
  (state: fromFeature.ActivityFeatureState) => state.activity
);

export const getAllActivities = createSelector(
  getActivityState,
  fromActivity.getAllActivities
);

export const getLastActivities = createSelector(
  getActivityState,
  fromActivity.getLastActivities
);

export const getSelectedActivity = createSelector(
  getActivityState,
  fromActivity.getSelectedActivity
);
