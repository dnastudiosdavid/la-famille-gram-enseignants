import {Component, OnInit} from "@angular/core";
import {MatDialogRef} from "@angular/material/dialog";
import {GrammaticalCase} from "../../../../../shared/model/sentences/grammatical-case";

@Component({
  selector: "app-chain-dialog",
  templateUrl: "./chain-dialog.component.html",
  styleUrls: ["./chain-dialog.component.scss"]
})
export class ChainDialogComponent implements OnInit {

  cases: GrammaticalCase[];

  selectedCases: GrammaticalCase[];

  chainCases: string[];

  constructor(private dialogRef: MatDialogRef<ChainDialogComponent>) {
  }

  ngOnInit(): void {
    // fetch the correct reference
    if (this.chainCases) {
      this.selectedCases = this.cases.filter(c => this.chainCases.contains(c.getValue()));
    }
  }

  /**
   * Validates the given input.
   */
  validate() {
    this.dialogRef.close({
      cases: this.selectedCases.filter(c => !!c).map(c => c.getValue())
    });
  }

  /**
   * Cancels the operation and closes the dialog.
   */
  cancel() {
    this.dialogRef.close(null);
  }
}
