import {Component, OnInit, ViewEncapsulation} from "@angular/core";
import {MatDialogRef} from "@angular/material/dialog";
import {GameMode} from "../../../../../shared/model/game-mode";
import {firstValueFrom, Observable} from "rxjs";
import {ExerciseTemplate} from "../../../../../shared/model/sentences/exercise-template";
import {Language} from "../../../../../shared/model/sentences/language";
import {LanguageService} from "../../services/languages.service";

/**
 * This component provides a dialog to change the base values of an exercise.
 */
@Component({
  selector: "app-exercise-dialog",
  templateUrl: "./exercise-dialog.component.html",
  styleUrls: ["./exercise-dialog.component.scss"],
  encapsulation: ViewEncapsulation.None
})
export class ExerciseDialogComponent implements OnInit {

  /**
   * The name of the exercise.
   */
  name: string;

  /**
   * The selected language.
   */
  language: string;

  /**
   * This flag is set to true if the exercise
   * lets the players add their score.
   */
  hasLeaderboard: boolean;

  /**
   * Available modes.
   */
  availableModes: string[];

  /**
   * The base modes of the exercises.
   */
  baseModes: string[];

  /**
   * (de)activates the tirettes mode
   */
  hasTirettes: boolean;

  /**
   * (de)activates the gram class mode
   */
  hasGramClass: boolean;

  /**
   * (de)activates the silhouettes mode
   */
  hasSilhouettes: boolean;

  /**
   * (de)activates the chains mode
   */
  hasChains: boolean;

  /**
   * True if this is a new exercise.
   */
  isNew: boolean;

  /**
   * Sentence templates.
   */
  templates$: Observable<ExerciseTemplate[]>;

  /**
   * The selected template.
   */
  selectedTemplate: ExerciseTemplate;

  /**
   * Available languages.
   */
  languages$: Observable<Language[]>;

  /**
   * Ctor.
   */
  constructor(private dialogRef: MatDialogRef<ExerciseDialogComponent>,
              private languageService: LanguageService) {
    this.hasLeaderboard = true;
    this.hasTirettes = true;
    this.hasGramClass = true;
    this.hasChains = true;
    this.hasSilhouettes = true;
    this.isNew = false;
    this.baseModes = [];
    this.availableModes = [];
  }

  /**
   * Initialization of the component.
   */
  async ngOnInit() {
    this.hasTirettes = this.baseModes.indexOf(GameMode.Tirettes) !== -1;
    this.hasChains = this.baseModes.indexOf(GameMode.Chains) !== -1;
    this.hasGramClass = this.baseModes.indexOf(GameMode.GramClass) !== -1;
    this.hasSilhouettes = this.baseModes.indexOf(GameMode.Silhouettes) !== -1;
    // TODO: this should be retrieved from the state
    this.languages$ = this.languageService.getLanguages();
    this.language ??= (await firstValueFrom(this.languages$))[0].Code;
    void this.updateAvailableModes();
  }

  /**
   * Validates the given input.
   */
  validate() {
    const modes: string[] = [];
    if (this.hasTirettes) {
      modes.push(GameMode.Tirettes);
    }
    if (this.hasChains) {
      modes.push(GameMode.Chains);
    }
    if (this.hasGramClass) {
      modes.push(GameMode.GramClass);
    }
    if (this.hasSilhouettes) {
      modes.push(GameMode.Silhouettes);
    }
    this.dialogRef.close({
      template: this.selectedTemplate,
      name: this.name,
      language: this.language,
      leaderboard: this.hasLeaderboard,
      modes: modes
    });
  }

  /**
   * Handles when the language changes.
   */
  languageChangedHandler(): void {
    void this.updateAvailableModes();
  }

  /**
   * Cancels the operation and closes the dialog.
   */
  cancel() {
    this.dialogRef.close(null);
  }

  /**
   * Returns true if the given mode is available.
   */
  isAvailable(mode: string): boolean {
    return this.availableModes.indexOf(mode) !== -1;
  }

  private async updateAvailableModes(): Promise<void> {
    this.availableModes = (await firstValueFrom(this.languageService.getLanguage(this.language))).AvailableModes;
  }
}
