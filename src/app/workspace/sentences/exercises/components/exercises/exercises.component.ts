import {Component, OnDestroy, OnInit} from "@angular/core";
import {MatDialog, MatDialogRef} from "@angular/material/dialog";
import {DragulaService} from "ng2-dragula";
import * as _ from "lodash";
import {Sentence} from "../../../../../shared/model/sentences/sentence";
import {Exercise} from "../../../../../shared/model/sentences/exercise";
import {ExerciseDialogComponent} from "../exercise-dialog/exercise-dialog.component";
import {ConfirmDialogComponent} from "../../../../../shared/confirm-dialog/confirm-dialog.component";
import {SentenceDialogComponent} from "../sentence-dialog/sentence-dialog.component";
import {select, Store} from "@ngrx/store";
import {ExerciseState} from "../../store/state";
import {ExerciseActions} from "../../store/exercise.actions";
import {firstValueFrom, Observable, Subscription} from "rxjs";
import {SentenceService} from "../../services/sentence.service";
import {ExerciseService} from "../../services/exercise.service";
import {LoadDialogComponent} from "../load-dialog/load-dialog.component";
import {LeaderboardDialogComponent} from "../leaderboard-dialog/leaderboard-dialog.component";
import {Language} from "../../../../../shared/model/sentences/language";
import {LanguageService} from "../../services/languages.service";

@Component({
  selector: "app-exercises",
  templateUrl: "./exercises.component.html",
  styleUrls: ["./exercises.component.scss"]
})
export class ExercisesComponent implements OnInit, OnDestroy {

  /**
   * The available languages.
   */
  languages$: Observable<Language[]>;

  /**
   * The selected language.
   */
  language: string;

  /**
   * Language of the selected exercise.
   */
  exerciseLanguage: Language;

  /**
   * All the exercises.
   */
  exercises$: Observable<Exercise []> = this.store.pipe(select((state: any) => state.exerciseFeature.exercise.exercises));

  /**
   * The selected exercise.
   */
  exercise: Exercise;

  /**
   * Saved sentence ready to be copied.
   * - could be a service -
   */
  clipboardSentence: Sentence;

  /**
   * The drop subscription.
   */
  dropSubscription: Subscription;

  /**
   * Ctor.
   */
  constructor(private dragulaService: DragulaService,
              private store: Store<ExerciseState>,
              private dialog: MatDialog,
              private exerciseService: ExerciseService,
              private sentenceService: SentenceService,
              private languageService: LanguageService) {
  }

  /**
   * Initializes the component.
   */
  async ngOnInit() {
    this.dragulaService.createGroup("first-bag", {
      moves: (el: HTMLElement, container: HTMLElement, handle: HTMLElement | null) => {
        return handle?.classList.contains("exercise-move") ?? false;
      }
    });
    this.dropSubscription = this.dragulaService.drop("first-bag").subscribe(() => {
      this.save();
    });
    // TODO: this should be an action
    this.languages$ = this.languageService.getLanguages();
    this.fetchExercises();
    this.store
      .pipe(
        select((state: any) => state.exerciseFeature.exercise.selected)
      )
      .subscribe(async (exercise: Exercise) => {
        this.exercise = _.cloneDeep(exercise);
        if (this.exercise) {
          this.exerciseLanguage = await firstValueFrom(this.languageService.getLanguage(this.exercise.Language));
        }
      });
  }

  /**
   * Releases resources.
   */
  ngOnDestroy() {
    if (this.dragulaService.find("first-bag")) {
      this.dragulaService.destroy("first-bag");
    }
    if (this.dropSubscription != null) {
      this.dropSubscription.unsubscribe();
    }
  }

  /**
   * Saves the exercise.
   */
  save() {
    this.store.dispatch(new ExerciseActions.Save(this.exercise));
  }

  /**
   * Gets the exercises of the user.
   */
  fetchExercises() {
    this.store.dispatch(new ExerciseActions.FetchAll());
  }

  /**
   * Creates a new exercise.
   */
  newExercise() {
    const dialogRef: MatDialogRef<ExerciseDialogComponent> = this.dialog.open(ExerciseDialogComponent);
    dialogRef.componentInstance.name = "";
    dialogRef.componentInstance.language = this.language;
    dialogRef.componentInstance.isNew = true;
    dialogRef.componentInstance.templates$ = this.exerciseService.getTemplates();
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        const exercise: Exercise = new Exercise(result.name, result.language, result.modes);
        if (result.template) {
          this.exerciseService.getExerciseByCode(result.template.Code).subscribe((tplExercise: Exercise) => {
            exercise.Sentences = tplExercise.Sentences;
            this.store.dispatch(new ExerciseActions.Save(exercise));
          });
        } else {
          // save already
          this.store.dispatch(new ExerciseActions.Save(exercise));
        }
      }
    });
  }

  /**
   * Loads a new exercise.
   */
  loadExercise() {
    const dialogRef: MatDialogRef<LoadDialogComponent> = this.dialog.open(LoadDialogComponent);
    dialogRef.afterClosed().subscribe(code => {
      this.exerciseService.getExerciseByCode(code).subscribe(exercise => {
        this.store.dispatch(new ExerciseActions.Save(Exercise.clone(exercise)));
      });
    });
  }

  /**
   * Edits the exercise (its name).
   */
  editExercise() {
    const dialogRef: MatDialogRef<ExerciseDialogComponent> = this.dialog.open(ExerciseDialogComponent);
    dialogRef.componentInstance.name = this.exercise.Name;
    dialogRef.componentInstance.hasLeaderboard = this.exercise.AcceptsLeaderboard;
    dialogRef.componentInstance.baseModes = this.exercise.Modes;
    dialogRef.componentInstance.language = this.exercise.Language;
    dialogRef.componentInstance.isNew = false;
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.exercise.Name = result.name;
        if (result.leaderboard) {
          this.exercise.unlockLeaderboard();
        } else {
          this.exercise.lockLeaderboard();
        }
        this.exercise.setModes(result.modes);
        this.save();
      }
    });
  }

  /**
   * Selects the given exercise.
   * @param {Exercise} exercise
   */
  select(exercise: Exercise) {
    this.store.dispatch(new ExerciseActions.Select(exercise));
  }

  /**
   * Removes the current exercise from the list of loaded exercises.
   */
  remove() {
    const dialogRef: MatDialogRef<ConfirmDialogComponent> = this.dialog.open(ConfirmDialogComponent);
    dialogRef.afterClosed().subscribe(result => {
      if (result === "confirm") {
        this.store.dispatch(new ExerciseActions.Delete(this.exercise));
      }
    });
  }

  /**
   * Creates a sentence. Let the user chose a name
   * for the sentence and then add it to the current exercise.
   */
  async addSentence() {
    const templates = await this.sentenceService.getTemplates(this.exerciseLanguage.Code).toPromise();
    const dialogRef: MatDialogRef<SentenceDialogComponent> = this.dialog.open(SentenceDialogComponent);
    dialogRef.componentInstance.name = "";
    dialogRef.componentInstance.isNew = true;
    dialogRef.componentInstance.templates = templates;
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        const sentence: Sentence = new Sentence(result.name, null, null, result.template);
        this.exercise.addSentence(sentence);
        this.save();
      }
    });
  }

  /**
   * Removes the given sentence from the
   * current exercise.
   * @param {Sentence} sentence
   */
  removeSentence(sentence: Sentence) {
    const dialogRef: MatDialogRef<ConfirmDialogComponent> = this.dialog.open(ConfirmDialogComponent);
    dialogRef.afterClosed().subscribe(result => {
      if (result === "confirm") {
        this.exercise.removeSentence(sentence);
        this.save();
      }
    });
  }

  /**
   * Copies the given sentence.
   * @param {Sentence} sentence
   */
  copySentence(sentence: Sentence) {
    this.clipboardSentence = sentence;
  }

  /**
   * Pastes the sentence from the clipboard.
   */
  pasteSentence() {
    this.exercise.addSentence(_.cloneDeep(this.clipboardSentence));
    this.clipboardSentence = null;
    this.save();
  }

  /**
   * Returns true if the user can paste a sentence.
   */
  canPaste(): boolean {
    return this.clipboardSentence != null;
  }

  /**
   * Shows the leaderboard.
   */
  showLeaderboard() {
    const dialogRef: MatDialogRef<LeaderboardDialogComponent> = this.dialog.open(LeaderboardDialogComponent);
    dialogRef.componentInstance.exercise = this.exercise;
    dialogRef.afterClosed().subscribe(result => {
    });
  }
}
