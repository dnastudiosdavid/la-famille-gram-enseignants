import {Component, EventEmitter, Output} from "@angular/core";

@Component({
  selector: "app-languages-add",
  templateUrl: "./languages-attribute-add.component.html",
  styleUrls: ["./languages-attribute-add.component.scss"]
})
export class LanguagesAttributeAddComponent {

  @Output()
  onAdd: EventEmitter<[string, string]> = new EventEmitter<[string, string]>();

  add(id: string, label: string) {
    if (id === "" || label === "") {
      return;
    }
    this.onAdd.emit([id, label]);
  }
}
