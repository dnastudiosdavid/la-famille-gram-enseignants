import {Component, EventEmitter, Input, Output} from "@angular/core";

@Component({
  selector: "app-languages-line",
  templateUrl: "./languages-attribute-line.component.html",
  styleUrls: ["./languages-attribute-line.component.scss"]
})
export class LanguagesAttributeLineComponent {

  @Input()
  value: string;

  @Input()
  label: string;

  @Input()
  readonly: boolean;

  @Input()
  editable: boolean = false;

  @Output()
  onRemove: EventEmitter<void> = new EventEmitter<void>();

  @Output()
  onEdit: EventEmitter<void> = new EventEmitter<void>();

  remove() {
    this.onRemove.emit();
  }

  edit() {
    this.onEdit.emit();
  }
}
