import {Component, OnInit} from "@angular/core";
import {GrammaticalClass} from "../../../../../../shared/model/sentences/grammatical-class";
import {MatDialogRef} from "@angular/material/dialog";

@Component({
  selector: "app-languages-class-add-dialog",
  templateUrl: "./languages-class-add-dialog.component.html",
  styleUrls: ["./languages-class-add-dialog.component.scss"]
})
export class LanguagesClassAddDialogComponent implements OnInit {

  cls: GrammaticalClass;

  constructor(private dialogRef: MatDialogRef<LanguagesClassAddDialogComponent>) {
  }

  ngOnInit() {
    this.cls = this.cls || this.getNewClass();
  }

  add() {
    this.dialogRef.close({
      cls: this.cls
    });
  }

  cancel() {
    this.dialogRef.close({});
  }

  private getNewClass(): GrammaticalClass {
    return new GrammaticalClass(null, null, false, false, false, false, false, null, null, null, null, false);
  }
}
