import {Component, Input} from "@angular/core";
import {GrammaticalClass} from "../../../../../../shared/model/sentences/grammatical-class";

@Component({
  selector: "app-languages-class-line",
  templateUrl: "./languages-class-line.component.html",
  styleUrls: ["./languages-class-line.component.scss"]
})
export class LanguagesClassLineComponent {

  @Input()
  cls: GrammaticalClass;
}
