import {Component, OnInit} from "@angular/core";
import {Language} from "../../../../../shared/model/sentences/language";
import {Observable} from "rxjs";
import {LanguageService} from "../../services/languages.service";
import {first} from "rxjs/operators";
import {GrammaticalGender} from "../../../../../shared/model/sentences/grammatical-gender";
import {GrammaticalNumber} from "../../../../../shared/model/sentences/grammatical-number";
import {GrammaticalCase} from "../../../../../shared/model/sentences/grammatical-case";
import {MatDialog, MatDialogRef} from "@angular/material/dialog";
import {LanguagesClassAddDialogComponent} from "./languages-class-add-dialog/languages-class-add-dialog.component";
import {GrammaticalClass} from "../../../../../shared/model/sentences/grammatical-class";
import {FileService} from "../../services/file.service";
import {classToPlain, plainToInstance} from "class-transformer";
import * as _ from "lodash";
import {LocalStorageService} from "../../../../../shared/services/local-storage.service";

@Component({
  selector: "app-languages",
  templateUrl: "./languages.component.html",
  styleUrls: ["./languages.component.scss"]
})
export class LanguagesComponent implements OnInit {

  private static readonly ReplaceConfirm = "Êtes-vous sûr-e? Cette opération va remplacer la langue en édition.";
  private static readonly EraseConfirm = "Êtes-vous sûr-e?";
  private static readonly LanguageStorageKey = "editor-language";

  /**
   * The available languages.
   */
  languages$: Observable<Language[]>;

  languageTemplate: Language = new Language();

  language: Language = new Language();

  modes: string[] = ["tirettes", "gram-class", "chains", "silhouettes"];

  uiState: ILanguagesUIState = {
    tab: "edit"
  };

  constructor(private languageService: LanguageService,
              private dialog: MatDialog,
              private fileService: FileService,
              private storageService: LocalStorageService) {
  }

  ngOnInit() {
    this.languages$ = this.languageService.getLanguages();
    this.languages$.pipe(first()).subscribe(l => this.languageTemplate = l[0]);
    this.load();
  }

  new() {
    if (confirm(LanguagesComponent.ReplaceConfirm)) {
      this.language = new Language();
      this.selectTab("edit");
    }
  }

  export() {
    this.fileService.export(`${this.language.Code ? this.language.Code.toLowerCase() : "langue"}.json`, JSON.stringify(this.language));
  }

  copy() {
    if (confirm(LanguagesComponent.ReplaceConfirm)) {
      this.language = _.cloneDeep(this.languageTemplate);
      this.selectTab("edit");
      this.save();
    }
  }

  async import() {
    if (confirm(LanguagesComponent.ReplaceConfirm)) {
      const raw = await this.fileService.import();
      this.language = plainToInstance(Language, raw);
      this.save();
    }
  }

  selectTab(tab: "fromApp" | "edit") {
    if (this.uiState.tab !== tab) {
      this.uiState.tab = tab;
    }
  }

  addGender(attribute: [string, string]) {
    this.language.Genders.push(new GrammaticalGender(attribute[0], attribute[1]));
    this.save();
  }

  addNumber(attribute: [string, string]) {
    this.language.Numbers.push(new GrammaticalNumber(attribute[0], attribute[1]));
    this.save();
  }

  addCase(attribute: [string, string]) {
    this.language.Cases.push(new GrammaticalCase(attribute[0], attribute[1]));
    this.save();
  }

  removeGender(gender: GrammaticalGender) {
    if (!confirm(LanguagesComponent.EraseConfirm)) {
      return;
    }
    this.language.Genders.remove(gender);
    this.save();
  }

  removeNumber(nbr: GrammaticalNumber) {
    if (!confirm(LanguagesComponent.EraseConfirm)) {
      return;
    }
    this.language.Numbers.remove(nbr);
    this.save();
  }

  removeCase(cs: GrammaticalCase) {
    if (!confirm(LanguagesComponent.EraseConfirm)) {
      return;
    }
    this.language.Cases.remove(cs);
    this.save();
  }

  newClass() {
    const dialogRef: MatDialogRef<LanguagesClassAddDialogComponent> = this.dialog.open(LanguagesClassAddDialogComponent);
    dialogRef.afterClosed().subscribe(result => {
      if (result && result.cls) {
        this.language.Classes.push(result.cls);
        this.save();
      }
    });
  }

  removeClass(cls: GrammaticalClass) {
    if (!confirm(LanguagesComponent.EraseConfirm)) {
      return;
    }
    this.language.Classes.remove(cls);
    this.save();
  }

  editClass(cls: GrammaticalClass) {
    const dialogRef: MatDialogRef<LanguagesClassAddDialogComponent> = this.dialog.open(LanguagesClassAddDialogComponent);
    dialogRef.componentInstance.cls = _.cloneDeep(cls);
    dialogRef.afterClosed().subscribe(result => {
      if (result && result.cls) {
        this.language.Classes.splice(this.language.Classes.indexOf(cls), 1, result.cls);
        this.save();
      }
    });
  }

  newFunction() {
    const dialogRef: MatDialogRef<LanguagesClassAddDialogComponent> = this.dialog.open(LanguagesClassAddDialogComponent);
    dialogRef.afterClosed().subscribe(result => {
      if (result && result.cls) {
        this.language.Functions.push(result.cls);
        this.save();
      }
    });
  }

  removeFunction(fn: GrammaticalClass) {
    if (!confirm(LanguagesComponent.EraseConfirm)) {
      return;
    }
    this.language.Functions.remove(fn);
    this.save();
  }

  editFunction(fn: GrammaticalClass) {
    const dialogRef: MatDialogRef<LanguagesClassAddDialogComponent> = this.dialog.open(LanguagesClassAddDialogComponent);
    dialogRef.componentInstance.cls = _.cloneDeep(fn);
    dialogRef.afterClosed().subscribe(result => {
      if (result && result.cls) {
        this.language.Functions.splice(this.language.Functions.indexOf(fn), 1, result.cls);
        this.save();
      }
    });
  }

  onLanguageChangedHandler($event: any) {
    if (!$event.value) {
      return;
    }
    this.languageTemplate = $event.value;
  }

  onFieldUpdatedHandler() {
    this.save();
  }

  private save(): void {
    this.storageService.store(LanguagesComponent.LanguageStorageKey, classToPlain(this.language));
  }

  private load(): void {
    this.language = plainToInstance(Language, this.storageService.get(LanguagesComponent.LanguageStorageKey)) || new Language();
  }
}

interface ILanguagesUIState {
  tab: "fromApp" | "edit";
}
