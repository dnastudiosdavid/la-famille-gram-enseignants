import {Component, OnInit} from "@angular/core";
import {MatDialogRef} from "@angular/material/dialog";
import {LeaderboardRank} from "../../../../../shared/model/sentences/leaderboard-rank";
import {Exercise} from "../../../../../shared/model/sentences/exercise";
import {Observable} from "rxjs";
import {ExerciseState} from "../../store/state";
import {select, Store} from "@ngrx/store";
import {ExerciseActions} from "../../store/exercise.actions";
import FetchLeaderboard = ExerciseActions.FetchLeaderboard;

@Component({
  selector: "app-leaderboard-dialog",
  templateUrl: "./leaderboard-dialog.component.html",
  styleUrls: ["./leaderboard-dialog.component.scss"]
})
export class LeaderboardDialogComponent implements OnInit {

  /**
   * The selected mode.
   */
  mode = "tirettes";

  /**
   * The exercise.
   */
  exercise: Exercise;

  /**
   * Our leaderboard data.
   */
  leaderboard$: Observable<LeaderboardRank []> = this.store.pipe(select((state: any) => state.exerciseFeature.exercise.leaderboard));

  constructor(private dialogRef: MatDialogRef<LeaderboardDialogComponent>,
              private store: Store<ExerciseState>) {
  }

  /**
   * Gets the leaderboard.
   */
  ngOnInit() {
    this.fetchLeaderboard(this.mode);
  }

  /**
   * Closes the dialog.
   */
  close() {
    this.dialogRef.close();
  }

  /**
   * Bans the given rank.
   */
  ban(rank: LeaderboardRank) {
    this.store.dispatch(new ExerciseActions.Ban(this.exercise, rank));
  }

  /**
   * Handles when the mode changes.
   */
  onModeChangedHandler() {
    this.fetchLeaderboard(this.mode);
  }

  /**
   * Gets the leaderboard.
   */
  private fetchLeaderboard(mode: string) {
    this.store.dispatch(new FetchLeaderboard(this.exercise, mode));
  }
}
