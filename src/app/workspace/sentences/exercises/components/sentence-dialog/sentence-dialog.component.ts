import {Component, OnDestroy, OnInit, ViewEncapsulation} from "@angular/core";
import {MatDialogRef} from "@angular/material/dialog";
import {Subscription} from "rxjs";
import {UntypedFormBuilder, Validators} from "@angular/forms";
import {List} from "linqts";
import {Sentence} from "../../../../../shared/model/sentences/sentence";

/**
 * This component provides a dialog to change the base values of a sentence.
 */
@Component({
  selector: "app-sentence-dialog",
  templateUrl: "./sentence-dialog.component.html",
  styleUrls: ["./sentence-dialog.component.scss"],
  encapsulation: ViewEncapsulation.None
})
export class SentenceDialogComponent implements OnDestroy, OnInit {

  /**
   * The name the dialog is working on.
   */
  name: string;

  /**
   * True if this is a new sentence.
   */
  isNew: boolean;

  /**
   * The current degree.
   */
  degree: string;

  /**
   * The current degree.
   */
  difficulty: string;

  /**
   * The current degree.
   */
  tplDegree: string;

  /**
   * Available degrees.
   */
  tplDegrees: string[] = [];

  /**
   * The selected difficulty.
   */
  tplDifficulty: string;

  /**
   * The available difficulties.
   */
  tplDifficulties: string [];

  /**
   * The templates.
   */
  templates: Sentence[];

  /**
   * The selected template.
   */
  selectedTemplate: Sentence;

  /**
   * This flag is true when the user can
   * choose a difficulty and a degree for the
   * sentence.
   */
  hasAdvancedParameters: boolean;

  /**
   * The subscription of templates Observable.
   */
  subscription: Subscription;

  /**
   * Form controls.
   */
  form = this.fb.group({
    name: ["", [Validators.required]],
    degree: ["", []],
    difficulty: ["", []],
    tplDegree: ["", []],
    tplDifficulty: ["", []],
    template: ["", []]
  });

  /**
   * Ctor.
   */
  constructor(private dialogRef: MatDialogRef<SentenceDialogComponent>,
              private fb: UntypedFormBuilder) {
    this.templates = [];
  }

  ngOnInit() {
    if (this.isNew) {
      this.selectedTemplate = this.templates[0];
      this.tplDegrees = new List(this.templates).Select(t => t.Degree).Where(d => d !== null).Distinct().ToArray();
    }
  }

  /**
   * Releases resources.
   */
  ngOnDestroy() {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }

  /**
   * Validates the given input.
   */
  validate() {
    this.dialogRef.close({
      name: this.name,
      degree: this.degree,
      difficulty: this.difficulty,
      template: this.selectedTemplate
    });
  }

  /**
   * Gets the current step.
   */
  getStep(): number {
    return this.tplDegree ? this.tplDifficulty ? 2 : 1 : 0;
  }

  /**
   * Cancels the operation.
   */
  cancel() {
    this.dialogRef.close(null);
  }

  /**
   * Sets the degree.
   */
  setTplDegree(degree: string) {
    this.tplDegree = degree;
  }

  /**
   * Gets the difficulties from the template.
   * It depends on the selected degrees.
   */
  getTplDifficulties(): string [] {
    let list = new List<Sentence>(this.templates);
    if (this.tplDegree !== "*") {
      list = list.Where(t => t.Degree === this.tplDegree);
    }
    return list.Select(t => t.Difficulty).Where(v => v !== null).Distinct().ToArray();
  }

  /**
   * Get the filtered templates.
   */
  getFilteredTpl(): Sentence[] {
    let list = new List<Sentence>(this.templates);
    if (this.tplDegree && this.tplDegree !== "*") {
      list = list.Where(t => t.Degree === this.tplDegree);
    }
    if (this.tplDifficulty && this.tplDifficulty !== "*") {
      list = list.Where(t => t.Difficulty === this.tplDifficulty);
    }
    return list.ToArray();
  }

  /**
   * Copies the name of the template.
   */
  copyTplName() {
    if (this.selectedTemplate) {
      this.name = this.getTemplateName(this.selectedTemplate);
    }
  }

  /**
   * Gets the name of the template.
   */
  getTemplateName(template: Sentence): string {
    const combinations: string[] = template.getCorrectCombinations();
    if (combinations.length > 0) {
      return template.getTextByIndexes(combinations[0]);
    } else {
      return template.Name;
    }
  }
}
