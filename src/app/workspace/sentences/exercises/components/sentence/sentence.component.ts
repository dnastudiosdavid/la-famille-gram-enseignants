import {Component, OnDestroy, OnInit, ViewEncapsulation} from "@angular/core";
import {MatDialog, MatDialogRef} from "@angular/material/dialog";
import {DragulaService} from "ng2-dragula";
import {ActivatedRoute, Router} from "@angular/router";
import {Sentence} from "../../../../../shared/model/sentences/sentence";
import {GrammaticalClass} from "../../../../../shared/model/sentences/grammatical-class";
import {SentenceDialogComponent} from "../sentence-dialog/sentence-dialog.component";
import {ConfirmDialogComponent} from "../../../../../shared/confirm-dialog/confirm-dialog.component";
import {TiretteDialogComponent} from "../tirette-dialog/tirette-dialog.component";
import {WordDialogComponent} from "../word-dialog/word-dialog.component";
import {Tirette} from "../../../../../shared/model/sentences/tirette";
import {IGrammaticalAttribute} from "../../../../../shared/model/sentences/grammatical-attribute-interface";
import {Word} from "../../../../../shared/model/sentences/word";
import {SolutionsDialogComponent} from "../solutions-dialog/solutions-dialog.component";
import {select, Store} from "@ngrx/store";
import {Exercise} from "../../../../../shared/model/sentences/exercise";
import {ExerciseState} from "../../store/state";
import * as _ from "lodash";
import {ExerciseActions} from "../../store/exercise.actions";
import {SentenceService} from "../../services/sentence.service";
import {LanguageService} from "../../services/languages.service";
import {Language} from "../../../../../shared/model/sentences/language";
import {ChainDialogComponent} from "../chain-dialog/chain-dialog.component";
import {Chain} from "../../../../../shared/model/sentences/chain";

/**
 * This view is part of the exercise creation process
 * and lets the user create and configure a sentence
 * in an exercise.
 */
@Component({
  selector: "app-sentence",
  templateUrl: "./sentence.component.html",
  styleUrls: ["./sentence.component.scss"],
  encapsulation: ViewEncapsulation.None
})
export class SentenceComponent implements OnInit, OnDestroy {

  /**
   * The selected exercise.
   */
  exercise: Exercise;

  /**
   * The language data.
   */
  language: Language;

  /**
   * The selected sentence object.
   */
  sentence: Sentence;

  /**
   * List of available grammatical classes.
   */
  grammaticalClasses: GrammaticalClass[];

  /**
   * List of available grammatical functions.
   */
  grammaticalFunctions: GrammaticalClass[];

  /**
   * List of grammatical persons.
   */
  grammaticalPersons: IGrammaticalAttribute[];

  /**
   * List of grammatical genders.
   */
  grammaticalGenders: IGrammaticalAttribute[];

  /**
   * List of grammatical numbers.
   */
  grammaticalNumbers: IGrammaticalAttribute[];

  /**
   * List of grammatical cases.
   */
  grammaticalCases: IGrammaticalAttribute[];

  /**
   * The max chains per sentence, retrieved from the Sentence
   * model constant because we can't access it directly
   * from the template.
   * @type {number}
   */
  maxChains: number = Sentence.MAX_CHAINS_PER_SENTENCE;

  get GrammaticalCases(): IGrammaticalAttribute[] {
    return this.grammaticalCases || [];
  }

  /**
   * Ctor.
   */
  constructor(private route: ActivatedRoute,
              private dialog: MatDialog,
              private router: Router,
              private languageService: LanguageService,
              private store: Store<ExerciseState>,
              private dragulaService: DragulaService,
              private sentenceService: SentenceService) {
  }

  /**
   * Initialization of the component.
   */
  ngOnInit() {
    this.dragulaService.createGroup("first-bag", {
      moves(el, container, handle) {
        return handle.className === "sentence-tirette-class handle";
      }
    });
    this.route.params.subscribe(params => {
      const index: number = +params.index;
      this.store
        .pipe(select((state: any) => state.exerciseFeature.exercise.selected))
        .subscribe(async (exercise: Exercise) => {
          if (exercise == null) {
            this.router.navigate(["/workspace/exercises"]);
          } else {
            const clone: Exercise = _.cloneDeep(exercise);
            this.exercise = clone;
            this.sentence = clone.Sentences[index];
            const language = await this.languageService.getLanguage(this.exercise.Language).toPromise();
            this.grammaticalGenders = language.Genders;
            this.grammaticalPersons = language.Persons;
            this.grammaticalClasses = language.Classes;
            this.grammaticalFunctions = language.Functions;
            this.grammaticalClasses = language.Classes;
            this.grammaticalNumbers = language.Numbers;
            this.grammaticalCases = language.Cases;
            this.language = language;
          }
        });
    });
  }

  /**
   * Releases resources.
   */
  ngOnDestroy() {
    this.dragulaService.destroy("first-bag");
  }

  /**
   * Saves the sentence (the whole exercise actually).
   */
  save() {
    this.store.dispatch(new ExerciseActions.Save(this.exercise));
  }

  /**
   * Edits the sentence.
   */
  edit() {
    const dialogRef: MatDialogRef<SentenceDialogComponent> = this.dialog.open(SentenceDialogComponent);
    dialogRef.componentInstance.name = this.sentence.Name;
    dialogRef.componentInstance.degree = this.sentence.Degree;
    dialogRef.componentInstance.difficulty = this.sentence.Difficulty;
    dialogRef.componentInstance.hasAdvancedParameters = this.exercise.Code === this.sentenceService.TemplateExerciseCode;
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.sentence.changeProperties(result.name, result.degree, result.difficulty);
      }
    });
  }

  /**
   * Adds a new tirette in the sentence.
   */
  addTirette() {
    const dialogRef: MatDialogRef<TiretteDialogComponent> = this.dialog.open(TiretteDialogComponent);
    dialogRef.componentInstance.chains = this.sentence.Chains;
    dialogRef.componentInstance.tirette = new Tirette();
    dialogRef.componentInstance.grammaticalClasses = this.grammaticalClasses;
    dialogRef.componentInstance.grammaticalFunctions = this.grammaticalFunctions;
    dialogRef.componentInstance.addChainCallback = () => {
      this.addChain(true);
    };
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.sentence.addTirette(result);
      }
    });
  }

  /**
   * Adds a word to the given tirette.
   * @param {Tirette} tirette
   */
  addWordTo(tirette: Tirette) {
    const dialogRef: MatDialogRef<WordDialogComponent> = this.dialog.open(WordDialogComponent);
    dialogRef.componentInstance.isLastTirette = this.sentence.Tirettes.indexOf(tirette) === this.sentence.Tirettes.length - 1;
    dialogRef.componentInstance.grammaticalClass = tirette.GrammaticalClass;
    dialogRef.componentInstance.setGenders(this.grammaticalGenders);
    dialogRef.componentInstance.setPersons(this.grammaticalPersons);
    dialogRef.componentInstance.setNumbers(this.grammaticalNumbers);
    dialogRef.componentInstance.setCases(this.GrammaticalCases);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        tirette.addWord(result);
      }
    });
  }

  /**
   * Removes the word from the given tirette.
   * @param {Word} word
   * @param {Tirette} tirette
   */
  removeWordFrom(word: Word, tirette: Tirette) {
    tirette.removeWord(word);
  }

  /**
   * Edits the given word.
   * @param {Word} word
   * @param {Tirette} tirette
   */
  editWord(word: Word, tirette: Tirette) {
    const dialogRef: MatDialogRef<WordDialogComponent> = this.dialog.open(WordDialogComponent);
    dialogRef.componentInstance.word = word;
    dialogRef.componentInstance.isLastTirette = this.sentence.Tirettes.indexOf(tirette) === this.sentence.Tirettes.length - 1;
    dialogRef.componentInstance.grammaticalClass = tirette.GrammaticalClass;
    dialogRef.componentInstance.setGenders(this.grammaticalGenders);
    dialogRef.componentInstance.setPersons(this.grammaticalPersons);
    dialogRef.componentInstance.setNumbers(this.grammaticalNumbers);
    dialogRef.componentInstance.setCases(this.GrammaticalCases);
    dialogRef.afterClosed().subscribe(newWord => {
      if (newWord) {
        word = newWord;
      }
    });
  }

  /**
   * Edits the given tirette.
   * @param {Tirette} tirette
   */
  editTirette(tirette: Tirette) {
    const dialogRef: MatDialogRef<TiretteDialogComponent> = this.dialog.open(TiretteDialogComponent);
    dialogRef.componentInstance.chains = this.sentence.Chains;
    dialogRef.componentInstance.tirette = _.cloneDeep(tirette);
    dialogRef.componentInstance.grammaticalClasses = this.grammaticalClasses;
    dialogRef.componentInstance.grammaticalFunctions = this.grammaticalFunctions;
    dialogRef.componentInstance.addChainCallback = () => {
      this.addChain();
    };
    dialogRef.afterClosed().subscribe((newTirette: Tirette) => {
      if (newTirette) {
        tirette.GrammaticalClass = newTirette.GrammaticalClass;
        tirette.Chain = newTirette.Chain;
      }
    });
  }

  /**
   * Removes the given tirette from the sentence.
   */
  removeTirette(tirette: Tirette) {
    const dialogRef: MatDialogRef<ConfirmDialogComponent> = this.dialog.open(ConfirmDialogComponent);
    dialogRef.afterClosed().subscribe(result => {
      if (result === "confirm") {
        this.sentence.removeTirette(tirette);
      }
    });
  }

  /**
   * Adds a chain.
   */
  addChain(skipDialog: boolean = false) {
    if (skipDialog || !this.language.SupportsCases) {
      this.sentence.addChain();
    } else {
      const dialogRef: MatDialogRef<ChainDialogComponent> = this.dialog.open(ChainDialogComponent);
      dialogRef.componentInstance.cases = this.language.Cases;
      dialogRef.afterClosed().subscribe((result) => {
        if (result) {
          this.sentence.addChainWithCase(result.cases);
        }
      });
    }
  }

  /**
   * Edits a chain.
   */
  editChain(chain: Chain) {
    const dialogRef: MatDialogRef<ChainDialogComponent> = this.dialog.open(ChainDialogComponent);
    dialogRef.componentInstance.chainCases = chain.Cases;
    dialogRef.componentInstance.cases = this.language.Cases;
    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        chain.Cases = result.cases;
        this.sentence.updateChain(chain);
      }
    });
  }

  /**
   * Removes the given chain.
   */
  removeChain() {
    this.sentence.removeChain();
  }

  /**
   * Generates the solutions of the sentence.
   */
  generateSolutions() {
    const dialogRef: MatDialogRef<SolutionsDialogComponent> = this.dialog.open(SolutionsDialogComponent);
    dialogRef.componentInstance.showSolutions(this.sentence.getCorrectSentences());
  }
}
