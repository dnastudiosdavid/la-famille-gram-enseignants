import {Component, ViewEncapsulation} from "@angular/core";

/**
 * This component shows the solutions of a sentence.
 */
@Component({
  selector: "app-solutions-dialog",
  templateUrl: "./solutions-dialog.component.html",
  styleUrls: ["./solutions-dialog.component.scss"],
  encapsulation: ViewEncapsulation.None
})
export class SolutionsDialogComponent {

  /**
   * Array of solutions.
   */
  solutions: string[];

  /**
   * Shows the given solutions.
   * @param {string[]} solutions
   */
  showSolutions(solutions: string[]) {
    this.solutions = solutions || [];
  }
}
