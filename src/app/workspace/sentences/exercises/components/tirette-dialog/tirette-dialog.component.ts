import {Component, ViewEncapsulation} from "@angular/core";
import {SentenceDialogComponent} from "../sentence-dialog/sentence-dialog.component";
import {MatDialogRef} from "@angular/material/dialog";
import {Tirette} from "../../../../../shared/model/sentences/tirette";
import {Chain} from "../../../../../shared/model/sentences/chain";
import {GrammaticalClass} from "../../../../../shared/model/sentences/grammatical-class";

/**
 * This component lets the user edit data about a tirette.
 */
@Component({
  selector: "app-tirette-dialog",
  templateUrl: "./tirette-dialog.component.html",
  styleUrls: ["./tirette-dialog.component.scss"],
  encapsulation: ViewEncapsulation.None
})
export class TiretteDialogComponent {

  /**
   * The tirette model.
   */
  tirette: Tirette;

  /**
   * The available chains.
   */
  chains: Chain[];

  /**
   * The grammatical classes available.
   */
  grammaticalClasses: GrammaticalClass[];

  /**
   * The grammatical functions available.
   */
  grammaticalFunctions: GrammaticalClass[];

  /**
   * Callack raised when the add chain button is clicked.
   */
  addChainCallback: () => void;

  /**
   * Ctor.
   * @param {MatDialogRef<SentenceDialogComponent>} dialogRef
   */
  constructor(private dialogRef: MatDialogRef<SentenceDialogComponent>) {
  }

  /**
   * Returns true if the input is valid.
   * @returns {boolean}
   */
  isValid(): boolean {
    return this.tirette.GrammaticalClass != null
      && (!this.tirette.GrammaticalClass.RequireChain || this.tirette.Chain != null);
  }

  /**
   * Called from the component when the
   * grammatical class changes.
   * @param event
   */
  onClassChangedHandler(event: any) {
    const grammaticalClass: GrammaticalClass = event.value as GrammaticalClass;
    if (!grammaticalClass.RequireChain) {
      this.tirette.Chain = null;
    }
  }

  /**
   * Handles when the user clicks on the
   * "add chain" button.
   */
  addChain() {
    if (this.addChainCallback) {
      this.addChainCallback();
      this.tirette.Chain = this.chains[this.chains.length - 1];
    }
  }

  /**
   * Validates the given input.
   */
  validate() {
    this.dialogRef.close(this.tirette);
  }

  /**
   * Cancels the operation.
   */
  cancel() {
    this.dialogRef.close(null);
  }
}
