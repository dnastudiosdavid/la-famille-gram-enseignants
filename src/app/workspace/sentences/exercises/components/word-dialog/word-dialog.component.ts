import {Component, OnDestroy, OnInit, ViewEncapsulation} from "@angular/core";
import {MatDialogRef} from "@angular/material/dialog";
import {IGrammaticalAttribute} from "../../../../../shared/model/sentences/grammatical-attribute-interface";
import {Word} from "../../../../../shared/model/sentences/word";
import {GrammaticalClass} from "../../../../../shared/model/sentences/grammatical-class";

/**
 * This class wraps an attribute and adds
 * a flag to know if it is selected or not.
 */
export class AttributeWrapper {

  /**
   * @see IsSelected
   */
  private isSelected: boolean;

  /**
   * @see Attribute
   */
  private attribute: IGrammaticalAttribute;

  /**
   * Returns true if the attribute is selected.
   * @returns {boolean}
   */
  get IsSelected(): boolean {
    return this.isSelected;
  }

  /**
   * Sets the selected flag.
   * @returns {boolean}
   */
  set IsSelected(value: boolean) {
    this.isSelected = value;
  }

  /**
   * Gets the wrapped attribute.
   * @returns {IGrammaticalAttribute}
   */
  get Attribute(): IGrammaticalAttribute {
    return this.attribute;
  }

  /**
   * Constructor of the attribute.
   * @param {IGrammaticalAttribute} attribute
   */
  constructor(attribute: IGrammaticalAttribute) {
    this.attribute = attribute;
  }
}

/**
 * This dialog lets the user add or edit words
 * in a tirette.
 */
@Component({
  selector: "app-word-dialog",
  templateUrl: "./word-dialog.component.html",
  styleUrls: ["./word-dialog.component.scss"],
  encapsulation: ViewEncapsulation.None
})
export class WordDialogComponent implements OnInit, OnDestroy {

  /**
   * The grammatical class of the tirette
   * where we want to add the word.
   */
  grammaticalClass: GrammaticalClass;

  /**
   * List of grammatical persons.
   */
  grammaticalPersons: AttributeWrapper[];

  /**
   * List of grammatical genders.
   */
  grammaticalGenders: AttributeWrapper[];

  /**
   * List of grammatical numbers.
   */
  grammaticalNumbers: AttributeWrapper[];

  /**
   * List of grammatical cases.
   */
  grammaticalCases: AttributeWrapper[];

  /**
   * True if this word is in the last tirette.
   */
  isLastTirette: boolean = false;

  /**
   * The word object.
   */
  word: Word;

  /**
   * Constructor of the dalog.
   * @param {MatDialogRef<WordDialogComponent>} dialogRef
   */
  constructor(private dialogRef: MatDialogRef<WordDialogComponent>) {
  }

  /**
   * Initialization of the component.
   */
  ngOnInit() {
    if (!this.word) {
      this.word = new Word();
    } else {
      this.setValuesOf(this.word);
    }

    setTimeout(() => this.word.migrate(), 10);
  }

  /**
   * Releases resources.
   */
  ngOnDestroy() {
    this.word = null;
  }

  /**
   * Sets the persons available.
   * @param {IGrammaticalAttribute[]} grammaticalPersons
   */
  setPersons(grammaticalPersons: IGrammaticalAttribute[]) {
    this.grammaticalPersons = this.wrap(grammaticalPersons);
  }

  /**
   * Sets the genders available.
   * @param {IGrammaticalAttribute[]} grammaticalGenders
   */
  setGenders(grammaticalGenders: IGrammaticalAttribute[]) {
    this.grammaticalGenders = this.wrap(grammaticalGenders);
  }

  /**
   * Sets the numbers available.
   * @param {IGrammaticalAttribute[]} grammaticalNumbers
   */
  setNumbers(grammaticalNumbers: IGrammaticalAttribute[]) {
    this.grammaticalNumbers = this.wrap(grammaticalNumbers);
  }

  /**
   * Sets the numbers available.
   */
  setCases(grammaticalCases: IGrammaticalAttribute[]) {
    this.grammaticalCases = this.wrap(grammaticalCases);
  }

  /**
   * Cancels the operation.
   */
  cancel() {
    this.dialogRef.close(null);
  }

  /**
   * Validates the new word.
   */
  validate() {
    this.word.Persons = this.grammaticalClass.RequirePerson ?
      this.getAttributeValue(this.grammaticalPersons) : this.grammaticalClass.DefaultPerson;

    this.word.Genders = this.grammaticalClass.RequireGender ?
      this.getAttributeValue(this.grammaticalGenders) : this.grammaticalClass.DefaultGender;

    this.word.Numbers = this.grammaticalClass.RequireNumber ?
      this.getAttributeValue(this.grammaticalNumbers) : this.grammaticalClass.DefaultNumber;

    this.word.Cases = this.grammaticalClass.RequireCase ?
      this.getAttributeValue(this.grammaticalCases) : this.grammaticalClass.DefaultCase;

    this.dialogRef.close(this.word);
  }

  /**
   * Sets the values of the given word
   * to the wrappers.
   * @param {Word} word
   */
  private setValuesOf(word: Word) {
    this.setWordValuesTo(this.grammaticalNumbers, word.NumbersArray);
    this.setWordValuesTo(this.grammaticalPersons, word.PersonsArray);
    this.setWordValuesTo(this.grammaticalGenders, word.GendersArray);
    this.setWordValuesTo(this.grammaticalCases || [], word.CasesArray);
  }

  /**
   * Sets the word values to the wrappers.
   */
  private setWordValuesTo(wrappers: AttributeWrapper[], values: string[]) {
    for (const wrapper of wrappers) {
      wrapper.IsSelected = values.indexOf(wrapper.Attribute.getValue()) !== -1;
    }
  }

  /**
   * Adds the selected attributes to the word.
   * @param {AttributeWrapper[]} wrappers
   */
  private getAttributeValue(wrappers: AttributeWrapper[]): string {
    let result = "";
    for (const wrapper of wrappers) {
      if (wrapper.IsSelected) {
        result += `${result === "" ? "" : Word.ValueSeparator}${wrapper.Attribute.getValue()}`;
      }
    }
    return result;
  }

  /**
   * Wraps the given attributes.
   * @param {IGrammaticalAttribute[]} attributes
   * @returns {AttributeWrapper[]}
   */
  private wrap(attributes: IGrammaticalAttribute[]): AttributeWrapper[] {
    const wrappers: AttributeWrapper[] = [];
    for (const attribute of attributes) {
      const wrapper: AttributeWrapper = new AttributeWrapper(attribute);
      wrappers.push(wrapper);
    }
    return wrappers;
  }
}
