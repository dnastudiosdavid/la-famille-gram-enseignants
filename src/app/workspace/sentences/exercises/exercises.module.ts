import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {ExercisesComponent} from "./components/exercises/exercises.component";
import {ExerciseDialogComponent} from "./components/exercise-dialog/exercise-dialog.component";
import {SentenceDialogComponent} from "./components/sentence-dialog/sentence-dialog.component";
import {LoadDialogComponent} from "./components/load-dialog/load-dialog.component";
import {AppMaterialModule} from "../../../app-material.module";
import {DragulaModule} from "ng2-dragula";
import {RouterModule} from "@angular/router";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {SharedModule} from "../../../shared/shared.module";
import {SentenceComponent} from "./components/sentence/sentence.component";
import {TiretteDialogComponent} from "./components/tirette-dialog/tirette-dialog.component";
import {WordDialogComponent} from "./components/word-dialog/word-dialog.component";
import {SolutionsDialogComponent} from "./components/solutions-dialog/solutions-dialog.component";
import {StoreModule} from "@ngrx/store";
import {exerciseReducer} from "./store/exercise.reducer";
import {ExerciseEffects} from "./store/exercise.effects";
import {EffectsModule} from "@ngrx/effects";
import {LeaderboardDialogComponent} from "./components/leaderboard-dialog/leaderboard-dialog.component";
import {ChainDialogComponent} from "./components/chain-dialog/chain-dialog.component";
import {LanguagesComponent} from "./components/languages/languages.component";
import {LanguagesAttributeAddComponent} from "./components/languages/languages-attribute-add/languages-attribute-add.component";
import {LanguagesAttributeLineComponent} from "./components/languages/languages-attribute-line/languages-attribute-line.component";
import {LanguagesClassLineComponent} from "./components/languages/languages-class-line/languages-class-line.component";
import {LanguagesClassAddDialogComponent} from "./components/languages/languages-class-add-dialog/languages-class-add-dialog.component";

@NgModule({
  declarations: [
    ExercisesComponent,
    SentenceComponent,
    WordDialogComponent,
    LoadDialogComponent,
    ExerciseDialogComponent,
    SentenceDialogComponent,
    TiretteDialogComponent,
    SolutionsDialogComponent,
    LeaderboardDialogComponent,
    ChainDialogComponent,
    LanguagesComponent,
    LanguagesAttributeAddComponent,
    LanguagesAttributeLineComponent,
    LanguagesClassLineComponent,
    LanguagesClassAddDialogComponent
  ],
  imports: [
    CommonModule,
    AppMaterialModule,
    SharedModule,
    DragulaModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule,
    StoreModule.forFeature("exerciseFeature", {
      exercise: exerciseReducer
    }),
    EffectsModule.forFeature([ExerciseEffects])
  ]
})
export class ExercisesModule {
}
