import {Routes} from "@angular/router";
import {ExercisesComponent} from "./components/exercises/exercises.component";
import {SentenceComponent} from "./components/sentence/sentence.component";
import {LanguagesComponent} from "./components/languages/languages.component";

export const EXERCISES_ROUTES: Routes = [
  {path: "", component: ExercisesComponent},
  {path: "languages", component: LanguagesComponent},
  {path: "sentence", component: SentenceComponent}
];
