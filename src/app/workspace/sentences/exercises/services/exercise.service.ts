import {Injectable} from "@angular/core";
import {Exercise} from "../../../../shared/model/sentences/exercise";
import {Observable} from "rxjs";
import {HttpClient, HttpParams} from "@angular/common/http";
import {environment} from "../../../../../environments/environment";
import {classToPlain, plainToInstance} from "class-transformer";
import {map} from "rxjs/operators";
import {SentencesActivity} from "../../../../shared/model/sentences/sentences-activity";
import {ExerciseTemplate} from "../../../../shared/model/sentences/exercise-template";
import {LeaderboardRank} from "../../../../shared/model/sentences/leaderboard-rank";

@Injectable({
  providedIn: "root"
})
export class ExerciseService {

  /**
   * Ctor.
   * @param httpClient
   */
  constructor(private httpClient: HttpClient) {
  }

  /**
   * Get the exercise templates.
   * @return {Observable<ExerciseTemplate[]>}
   */
  getTemplates(): Observable<ExerciseTemplate[]> {
    return new Observable(observer => {
      observer.next([
        new ExerciseTemplate("4H (2P)", "f8fd20a02bf7"),
        new ExerciseTemplate("5H (3P)", "9d5d6affb15c"),
        new ExerciseTemplate("6H (4P)", "9495ac37de94"),
        new ExerciseTemplate("7H (5P)", "dea7c05ccb30"),
        new ExerciseTemplate("8H (6P)", "f9421ddc218f")]);
      observer.complete();
    });
  }

  /**
   * Saves the exercise.
   * @param exercise
   */
  saveExercise(exercise: Exercise): Observable<Exercise> {
    if (exercise.IsNew) {
      return this.httpClient.post(`${environment.apiEndpoint}/exercise`, classToPlain(exercise)).pipe(
        map<any, Exercise>(e => plainToInstance(Exercise, e))
      );
    } else {
      return this.httpClient.put(`${environment.apiEndpoint}/exercise/${exercise.Id}`, classToPlain(exercise)).pipe(
        map<any, Exercise>(e => plainToInstance(Exercise, e))
      );
    }
  }

  /**
   * Gets all the exercises of the current user.
   */
  getExercises(): Observable<Exercise[]> {
    return this.httpClient.get<Exercise[]>(`${environment.apiEndpoint}/exercise`).pipe(
      map(plain => plainToInstance(Exercise, plain))
    );
  }

  /**
   * Gets all the activities of the current user.
   * @param limit
   */
  getActivities(limit: number): Observable<SentencesActivity[]> {
    let params: HttpParams = new HttpParams();
    params = params.set("limit", limit.toString());
    return this.httpClient.get<SentencesActivity[]>(`${environment.apiEndpoint}/activity`, {
      params
    }).pipe(
      map(plain => plainToInstance(SentencesActivity, plain))
    );
  }

  /**
   * Gets the details of an exercise.
   */
  getExercise(id: number): Observable<Exercise> {
    return this.httpClient.get<Exercise>(`${environment.apiEndpoint}/exercise/${id}`).pipe(
      map(plain => plainToInstance(Exercise, plain))
    );
  }

  /**
   * Gets the details of an exercise.
   */
  getExerciseByCode(code: string): Observable<Exercise> {
    return this.httpClient.get<Exercise>(`${environment.apiEndpoint}/p/exercise/${code}`).pipe(
      map(plain => plainToInstance(Exercise, plain))
    );
  }

  /**
   * Gets the rank of the current exercise.
   */
  getLeaderboard(code: string, mode: string): Observable<LeaderboardRank[]> {
    const params = new HttpParams().set("mode", mode);
    return this.httpClient.get<LeaderboardRank[]>(`${environment.apiEndpoint}/p/exercise/${code}/score`, {params})
      .pipe(
        map(plain => plainToInstance(LeaderboardRank, plain))
      );
  }

  /**
   * Gets the details of an exercise.
   * @param exercise
   */
  deleteExercise(exercise: Exercise): Observable<Exercise> {
    return this.httpClient.delete<Exercise>(`${environment.apiEndpoint}/exercise/${exercise.Id}`).pipe(
      map(ex => exercise)
    );
  }

  /**
   * Bans the given rank.
   */
  ban(exercise: Exercise, rank: LeaderboardRank): Observable<LeaderboardRank []> {
    rank.rename(this.getCoolNickname());
    return this.httpClient.put<LeaderboardRank[]>(`${environment.apiEndpoint}/exercise/${exercise.Id}/score/${rank.Id}`, rank)
      .pipe(
        map(plain => plainToInstance(LeaderboardRank, plain))
      );
  }

  /**
   * Gets a cool nickname.
   */
  private getCoolNickname(): string {
    const ar = ["Alpha 5", "Andros", "Bulk", "Billy", "Ernie", "Goldar", "Kimberly", "Tommy", "Adam Park", "Rita Repulsa", "Zack"];
    return ar[Math.floor(Math.random() * ar.length)];
  }
}
