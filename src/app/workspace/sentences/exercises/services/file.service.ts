import {Injectable} from "@angular/core";
import {fromEvent} from "rxjs";
import {filter, map, switchMap} from "rxjs/operators";

@Injectable({
  providedIn: "root"
})
export class FileService {

  export(filename: string, text: string): void {
    const element = document.createElement("a");
    element.setAttribute("href", "data:text/plain;charset=utf-8," + encodeURIComponent(text));
    element.setAttribute("download", filename);
    element.style.display = "none";
    document.body.appendChild(element);
    element.click();
    document.body.removeChild(element);
  }

  async import(): Promise<any> {
    return new Promise<any>((accept, reject) => {
      const input = document.createElement("input");
      input.type = "file";

      fromEvent(input, "change")
        .pipe(
          map(event => (event.target as HTMLInputElement).files[0]),
          filter(file => file.type === "application/json"),
          switchMap(file => this.parseJsonFile(file))
        ).subscribe(raw => {
        accept(raw);
      });

      input.click();
    });
  }

  async parseJsonFile(file: File): Promise<any> {
    return new Promise((resolve, reject) => {
      const fileReader = new FileReader();
      fileReader.onload = event => resolve(JSON.parse(fileReader.result.toString()));
      fileReader.onerror = error => reject(error);
      fileReader.readAsText(file);
    });
  }

}
