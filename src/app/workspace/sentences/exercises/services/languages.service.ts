import {HttpClient} from "@angular/common/http";
import {Injectable} from "@angular/core";
import {Observable, of} from "rxjs";
import {map, share, switchMap, tap} from "rxjs/operators";
import {plainToInstance} from "class-transformer";
import {Language} from "../../../../shared/model/sentences/language";
import {environment} from "../../../../../environments/environment";

@Injectable({
  providedIn: "root"
})
export class LanguageService {

  /**
   * The cached languages array.
   */
  private languages: Language[];

  /**
   * Ctor.
   * @param httpClient
   */
  constructor(private httpClient: HttpClient) {
  }

  public getLanguage(code: string): Observable<Language> {
    return this.getLanguages().pipe(
      map(languages => languages.find(l => l.Code === code))
    );
  }

  /**
   * Returns the available languages.
   * 1. Uses a cached version
   * 2. Ask the API and cache the result
   */
  public getLanguages(): Observable<Language[]> {
    return of(this.languages).pipe(
      switchMap(languages => languages ? of(this.languages) : this.httpClient.get(`${environment.languagesEndpoint}?t=${Date.now()}`).pipe(
          map((raw: []) => plainToInstance(Language, raw) || []),
          tap(toCache => this.languages = toCache),
          share()
        )
      )
    );
  }
}
