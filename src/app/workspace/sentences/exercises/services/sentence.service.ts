import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import * as _ from "lodash";
import {Observable, Observer, of} from "rxjs";
import {ExerciseService} from "./exercise.service";
import {Sentence} from "../../../../shared/model/sentences/sentence";

@Injectable({
  providedIn: "root"
})
export class SentenceService {

  /**
   * Code of the template exercise.
   */
  private readonly templateExerciseCode: string = "db8379c72ec6";

  /**
   * Gets the template exercise code.
   */
  get TemplateExerciseCode(): string {
    return this.templateExerciseCode;
  }

  /**
   * Ctor.
   * @param httpClient
   * @param exerciseService
   */
  constructor(private httpClient: HttpClient,
              private exerciseService: ExerciseService) {
  }

  /**
   * Get the sentence templates.
   */
  getTemplates(language: string): Observable<Sentence[]> {
    if (language !== `fr`) {
      return of([]);
    }
    return new Observable((observer: Observer<Sentence[]>) => {
      this.exerciseService.getExerciseByCode(this.templateExerciseCode).subscribe(exercise => {
        observer.next(exercise.Sentences.map(s => _.cloneDeep(s)));
        observer.complete();
      }, (error) => {
        observer.error(`Unable to fetch the template exercise. Failed with error: ${error}`);
      });
    });
  }
}
