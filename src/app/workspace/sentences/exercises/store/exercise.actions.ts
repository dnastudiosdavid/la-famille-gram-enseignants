import {Action} from "@ngrx/store";
import {Exercise} from "../../../../shared/model/sentences/exercise";
import {LeaderboardRank} from "../../../../shared/model/sentences/leaderboard-rank";

export namespace ExerciseActions {

  export enum ActionTypes {
    Select = "[Exercise] Select",
    Save = "[Exercise] Save",
    SaveDone = "[Exercise] Save done",
    FetchDone = "[Exercise] fetch done",
    FetchFailed = "[Exercise] fetch failed",
    FetchAll = "[Exercise] Fetch all",
    FetchAllDone = "[Exercise] Fetch all done",
    FetchLeaderboard = "[Exercise] Fetch leaderboard",
    FetchLeaderboardDone = "[Exercise] Fetch leaderboard done",
    Ban = "[Exercise] ban leaderboard rank",
    Delete = "[Exercise] Delete",
    DeleteDone = "[Exercise] Delete done"
  }

  export class Save implements Action {
    readonly type = ActionTypes.Save;

    constructor(public payload: Exercise) {
    }
  }

  export class FetchFailed implements Action {
    readonly type = ActionTypes.FetchFailed;

    constructor() {
    }
  }

  export class FetchDone implements Action {
    readonly type = ActionTypes.FetchDone;

    constructor(public payload: Exercise) {
    }
  }

  export class Select implements Action {
    readonly type = ActionTypes.Select;

    constructor(public payload: Exercise | number) {
    }
  }

  export class SaveDone implements Action {
    readonly type = ActionTypes.SaveDone;

    constructor(public payload: Exercise) {
    }
  }

  export class FetchAll implements Action {
    readonly type = ActionTypes.FetchAll;

    constructor() {
    }
  }

  export class FetchAllDone implements Action {
    readonly type = ActionTypes.FetchAllDone;

    constructor(public payload: Exercise []) {
    }
  }

  export class Delete implements Action {
    readonly type = ActionTypes.Delete;

    constructor(public payload: Exercise) {
    }
  }

  export class DeleteDone implements Action {
    readonly type = ActionTypes.DeleteDone;

    constructor(public payload: Exercise) {
    }
  }

  export class FetchLeaderboard implements Action {
    readonly type = ActionTypes.FetchLeaderboard;

    constructor(public exercise: Exercise, public mode: string) {
    }
  }

  export class FetchLeaderboardDone implements Action {
    readonly type = ActionTypes.FetchLeaderboardDone;

    constructor(public payload: LeaderboardRank[]) {
    }
  }

  export class Ban implements Action {
    readonly type = ActionTypes.Ban;

    constructor(public exercise: Exercise, public rank: LeaderboardRank) {
    }
  }

  export type Actions =
    Select
    | FetchDone
    | Save
    | SaveDone
    | FetchFailed
    | FetchAll
    | FetchAllDone
    | FetchLeaderboard
    | FetchLeaderboardDone
    | Ban
    | Delete
    | DeleteDone;
}
