import {Injectable} from "@angular/core";
import {combineLatest, Observable, of} from "rxjs";
import {Actions, createEffect, ofType} from "@ngrx/effects";
import {catchError, filter, map, switchMap, withLatestFrom} from "rxjs/operators";
import {Action, Store} from "@ngrx/store";
import {ExerciseService} from "../services/exercise.service";
import {ExerciseActions} from "./exercise.actions";
import {Exercise} from "../../../../shared/model/sentences/exercise";
import {LoadingService} from "../../../../shared/loading-spinner/loading.service";
import {MatSnackBar} from "@angular/material/snack-bar";
import {AbstractEffects} from "../../../effects-abstract";
import {ExerciseState} from "./state";
import {LanguageService} from "../services/languages.service";
import FetchLeaderboard = ExerciseActions.FetchLeaderboard;
import FetchLeaderboardDone = ExerciseActions.FetchLeaderboardDone;
import Ban = ExerciseActions.Ban;

@Injectable()
export class ExerciseEffects extends AbstractEffects {

  /**
   * This effect fetches the details of an exercise.
   */

  getExercise$: Observable<Action> = createEffect(() => this.actions$
    .pipe(
      ofType(ExerciseActions.ActionTypes.Select),
      switchMap((action: ExerciseActions.Select) => {
        this.open("exercise");
        let $fetch: Observable<Exercise>;
        if (action.payload instanceof Exercise) {
          $fetch = this.exerciseService.getExercise(action.payload.Id);
        } else if (typeof action.payload === "number") {
          $fetch = this.exerciseService.getExercise(action.payload);
        }

        return $fetch.pipe(
          switchMap(exercise => combineLatest([of(exercise), this.languageService.getLanguage(exercise.Language)])),
          map(([exercise, language]) => {
            this.close("exercise");
            return new ExerciseActions.FetchDone(exercise);
          }),
          catchError(() => {
            this.close("exercise", "Erreur", false);
            return of(new ExerciseActions.FetchFailed());
          })
        );
      })
    ));

  /**
   * This effect fetches the details of an exercise.
   */

  deleteExercise$: Observable<Action> = createEffect(() => this.actions$
    .pipe(
      ofType(ExerciseActions.ActionTypes.Delete),
      switchMap((action: ExerciseActions.Delete) => {
        this.open("exercise");
        return this.exerciseService.deleteExercise(action.payload);
      }),
      map((exercise: Exercise) => {
        this.close("exercise", "Suppression effectuée!");
        return new ExerciseActions.DeleteDone(exercise);
      })
    ));

  /**
   * This effect triggers the get exercises from
   * the exercise service.
   */

  getExercises$: Observable<Action> = createEffect(() => this.actions$
    .pipe(
      ofType(ExerciseActions.ActionTypes.FetchAll),
      switchMap(() => {
        this.open("exercises");
        return this.exerciseService.getExercises();
      }),
      map((exercises: Exercise []) => {
        this.close("exercises");
        return new ExerciseActions.FetchAllDone(exercises);
      })
    ));

  /**
   * Gets a leaderboard.
   */

  getLeaderboard$: Observable<Action> = createEffect(() => this.actions$
    .pipe(
      ofType(ExerciseActions.ActionTypes.FetchLeaderboard),
      switchMap((action: FetchLeaderboard) => {
        this.open("leaderboard");
        return this.exerciseService.getLeaderboard(action.exercise.Code, action.mode);
      }),
      map(ranks => {
        this.close("leaderboard");
        return new FetchLeaderboardDone(ranks);
      })
    ));

  /**
   * This effect is raised when the user wants
   * to ban a nickname from the leaderboard.
   */

  banRank$: Observable<Action> = createEffect(() => this.actions$
    .pipe(
      ofType(ExerciseActions.ActionTypes.Ban),
      switchMap((action: Ban) => {
        this.open("leaderboard");
        return this.exerciseService.ban(action.exercise, action.rank);
      }),
      map(ranks => {
        this.close("leaderboard", "Pseudo censuré!");
        return new FetchLeaderboardDone(ranks);
      })
    ));

  /**
   * This effect gets a default exercise from the
   * exercise service if a FetchAll action happens.
   */

  selectDefaultExercise$: Observable<Action> = createEffect(() => this.actions$
    .pipe(
      ofType(ExerciseActions.ActionTypes.FetchAllDone),
      withLatestFrom(this.store$),
      filter(([action, state]: [ExerciseActions.FetchAllDone, any]) => {
        return !state.exerciseFeature.exercise.selected;
      }),
      map(([action, state]: [ExerciseActions.FetchAllDone, any]) => {
        if (action.payload.length > 0) {
          return new ExerciseActions.Select(action.payload[0]);
        } else {
          return new ExerciseActions.FetchFailed();
        }
      })
    ));

  /**
   * This effect uses the exercise service to save the
   * exercise from the action.
   */

  saveExercises$: Observable<Action> = createEffect(() => this.actions$
    .pipe(
      ofType(ExerciseActions.ActionTypes.Save),
      switchMap((action: ExerciseActions.Save) => {
        this.open("exercise");
        return this.exerciseService.saveExercise(action.payload);
      }),
      map((exercise: Exercise) => {
        this.close("exercise", "Opération réussie!");
        return new ExerciseActions.SaveDone(exercise);
      }),
      catchError((_, caught) => {
        this.close("exercise", "Erreur", false);
        return caught;
      })
    ));

  /**
   * Ctor.
   * @param exerciseService
   * @param languageService
   * @param loadingService
   * @param snackBar
   * @param actions$
   * @param store$
   */
  constructor(private exerciseService: ExerciseService,
              private languageService: LanguageService,
              protected loadingService: LoadingService,
              protected snackBar: MatSnackBar,
              private actions$: Actions,
              private store$: Store<ExerciseState>) {
    super(loadingService, snackBar);
  }
}
