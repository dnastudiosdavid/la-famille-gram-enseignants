import {ExerciseState, initialState} from "./state";
import {ExerciseActions} from "./exercise.actions";
import {plainToInstance} from "class-transformer";
import {Exercise} from "../../../../shared/model/sentences/exercise";

export function exerciseReducer(state: ExerciseState = initialState, action: ExerciseActions.Actions) {
  switch (action.type) {

    case ExerciseActions.ActionTypes.Select:
      return state;

    case ExerciseActions.ActionTypes.FetchDone:
      return {
        ...state,
        selected: action.payload
      };

    case ExerciseActions.ActionTypes.Save:
      return state;

    case ExerciseActions.ActionTypes.SaveDone:
      const itemExists: boolean = state.exercises.filter(ex => ex.Id === action.payload.Id).length >= 1;
      if (!itemExists) {
        return {
          ...state,
          selected: action.payload,
          exercises: [
            ...state.exercises,
            action.payload
          ]
        };
      } else {
        return {
          ...state,
          selected: action.payload,
          exercises: [...state.exercises.map(ex => ex.Id === action.payload.Id ? action.payload : plainToInstance(Exercise, {...ex}))]
        };
      }

    case ExerciseActions.ActionTypes.FetchAll:
      return state;

    case ExerciseActions.ActionTypes.FetchAllDone:
      return {
        ...state,
        exercises: action.payload
      };

    case ExerciseActions.ActionTypes.FetchLeaderboard:
      return state;

    case ExerciseActions.ActionTypes.FetchLeaderboardDone:
      return {
        ...state,
        leaderboard: action.payload
      };

    case ExerciseActions.ActionTypes.Delete:
      return state;

    case ExerciseActions.ActionTypes.DeleteDone:
      return {
        ...state,
        selected: null,
        exercises: [...state.exercises.filter(e => e.Id != action.payload.Id)]
      };

    default:
      return state;
  }
}
