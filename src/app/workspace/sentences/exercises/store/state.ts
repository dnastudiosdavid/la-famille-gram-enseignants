import {Exercise} from "../../../../shared/model/sentences/exercise";
import {LeaderboardRank} from "../../../../shared/model/sentences/leaderboard-rank";

export interface ExerciseState {
  exercises: Exercise[];
  selected: Exercise;
  leaderboard: LeaderboardRank [];
}

export const initialState: ExerciseState = {
  exercises: [],
  selected: null,
  leaderboard: []
};
