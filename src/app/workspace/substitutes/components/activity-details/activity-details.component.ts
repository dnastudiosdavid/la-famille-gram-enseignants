import {Component, OnDestroy, OnInit} from "@angular/core";
import {Observable, Subscription} from "rxjs";
import {select, Store} from "@ngrx/store";
import {SubstitutesState} from "../../store/reducers/substitutes.reducer";
import {SubstitutesActivity} from "../../../../shared/model/substitutes/substitutes-activity";
import {DomSanitizer} from "@angular/platform-browser";

@Component({
  selector: "app-activity-details",
  templateUrl: "./activity-details.component.html",
  styleUrls: ["./activity-details.component.scss"]
})
export class ActivityDetailsComponent implements OnInit, OnDestroy {
  /**
   * The selected activity.
   */
  activity$: Observable<SubstitutesActivity> = this.store.pipe(select((state: any) => state.substitutesFeature.exercise.selectedActivity));

  uiState = {
    visible: []
  };

  sub: Subscription = new Subscription();

  /**
   * Ctor.
   */
  constructor(private store: Store<SubstitutesState>,
              protected sanitizer: DomSanitizer) {
  }

  ngOnInit(): void {
    this.sub.add(this.activity$.subscribe(a => {
        if (a?.Steps.length > 0) {
          this.uiState.visible = [0];
        }
      }
    ));
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }

  toggleVisibility(i: number): void {
    if (!this.uiState.visible.contains(i)) {
      this.uiState.visible.push(i);
    } else {
      this.uiState.visible.remove(i);
    }
  }

  isVisible(i: number): boolean {
    return this.uiState.visible.contains(i);
  }
}
