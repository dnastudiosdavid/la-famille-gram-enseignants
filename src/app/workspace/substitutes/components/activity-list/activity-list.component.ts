import {Component, OnDestroy, OnInit} from "@angular/core";
import {Observable, Subscription} from "rxjs";
import {SubstitutesExercise} from "../../../../shared/model/substitutes/substitutes-exercise";
import * as fromExercise from "../../store";
import {SubstitutesActivityActions} from "../../store";
import {Store} from "@ngrx/store";
import {SubstitutesState} from "../../store/reducers/substitutes.reducer";
import {SubstitutesActivity} from "../../../../shared/model/substitutes/substitutes-activity";

@Component({
  selector: "app-activity-list",
  templateUrl: "./activity-list.component.html",
  styleUrls: ["./activity-list.component.scss"]
})
export class ActivityListComponent implements OnInit, OnDestroy {

  private static readonly ChunkSize = 30;

  /**
   * All the exercises.
   */
  selectedExercise$: Observable<SubstitutesExercise> = this.store.select(fromExercise.getSelectedExercise);
  loadedActivities$: Observable<SubstitutesActivity[]> = this.store.select(fromExercise.getAllActivities);

  hasMore: boolean = true;

  private exerciseId: number;

  private activitiesAmount: number = 0;

  private subs: Subscription = new Subscription();

  constructor(private store: Store<SubstitutesState>) {
  }

  ngOnInit() {
    this.subs.add(this.selectedExercise$
      .subscribe((exercise: SubstitutesExercise) => {
        if (exercise) {
          this.exerciseId = exercise.Id;
          this.loadActivities();
        }
      }));

    this.subs.add(this.loadedActivities$.subscribe(activities => {
      this.hasMore = activities.length > this.activitiesAmount && activities.length - this.activitiesAmount >= ActivityListComponent.ChunkSize;
      this.activitiesAmount = activities.length;
    }));
  }

  ngOnDestroy(): void {
    this.subs.unsubscribe();
  }

  loadActivities(): void {
    this.store.dispatch(new SubstitutesActivityActions.Load(this.exerciseId, this.activitiesAmount, ActivityListComponent.ChunkSize));
  }
}
