import {Component} from "@angular/core";
import {Observable} from "rxjs";
import * as fromSubstitutes from "../../store";
import * as fromRoot from "../../../../store";
import {Store} from "@ngrx/store";
import {SubstitutesExercise} from "../../../../shared/model/substitutes/substitutes-exercise";
import {ActivatedRoute, Router} from "@angular/router";
import {ExercisesListAbstract} from "../exercises-list-abstract";

@Component({
  selector: "app-activity",
  templateUrl: "./activity.component.html",
  styleUrls: ["./activity.component.scss"]
})
export class ActivityComponent extends ExercisesListAbstract {

  exercises$: Observable<SubstitutesExercise[]> = this.rootStore.select(fromSubstitutes.getAllExercises);
  selectedExercise$: Observable<SubstitutesExercise> = this.rootStore.select(fromSubstitutes.getSelectedExercise);

  constructor(protected rootStore: Store<fromRoot.State>,
              protected router: Router,
              protected activatedRoute: ActivatedRoute) {
    super(rootStore, router, activatedRoute);
  }
}
