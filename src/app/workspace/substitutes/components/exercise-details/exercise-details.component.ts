import {Component, OnDestroy, OnInit} from "@angular/core";
import {Observable, Subscription} from "rxjs";
import {Store} from "@ngrx/store";
import {SubstitutesState} from "../../store/reducers/substitutes.reducer";
import * as fromExercise from "../../store";
import {SubstitutesActions} from "../../store";
import {SubstitutesExercise} from "../../../../shared/model/substitutes/substitutes-exercise";
import {ParagraphItf} from "../../../../shared/model/substitutes/paragraph-interface";
import {WordItf} from "../../../../shared/model/substitutes/word-interface";
import {GroupItf} from "../../../../shared/model/substitutes/group-interface";
import {MatDialog, MatDialogRef} from "@angular/material/dialog";
import {ConfirmDialogComponent} from "../../../../shared/confirm-dialog/confirm-dialog.component";
import {ExerciseDialogComponent} from "../exercise/exercise-dialog/exercise-dialog.component";
import {Actions, ofType} from "@ngrx/effects";
import {Router} from "@angular/router";
import {ExerciseGroupDialogComponent} from "./exercise-group-dialog/exercise-group-dialog.component";
import {SubstitutesUtils} from "../../../../shared/model/substitutes/substitutes-utils";

@Component({
  selector: "app-exercise-details",
  templateUrl: "./exercise-details.component.html",
  styleUrls: ["./exercise-details.component.scss"]
})
export class ExerciseDetailsComponent implements OnInit, OnDestroy {

  constructor(private store: Store<SubstitutesState>,
              private actions$: Actions,
              private dialog: MatDialog,
              private router: Router) {
  }

  /**
   * The selected exercise.
   */
  exercise$: Observable<SubstitutesExercise> = this.store.select(fromExercise.getSelectedExercise);

  paragraphs: ParagraphItf[];

  group: GroupItf;

  mode: EditMode = EditMode.Write;

  Modes = EditMode;

  confirmEdit: boolean = false;

  private exercise: SubstitutesExercise;

  private sub: Subscription = new Subscription();

  ngOnInit() {
    this.sub.add(this.exercise$.subscribe(ex => {
      if (ex) {
        this.paragraphs = ex.getStructuredText();
        this.exercise = ex;
        this.selectGroup(ex.Groups[0]);
        this.refreshSolution();
        this.confirmEdit = Object.keys(this.exercise.Solution).length > 0;
      }
    }));

    this.sub.add(this.actions$
      .pipe(
        ofType(SubstitutesActions.ActionTypes.DeleteDone)
      ).subscribe(_ => this.router.navigate(["/workspace/substitutes/exercise"])));
  }

  ngOnDestroy() {
    if (this.sub) {
      this.sub.unsubscribe();
    }
  }

  /**
   * Edits exercise data.
   */
  edit() {
    const dialogRef: MatDialogRef<ExerciseDialogComponent> = this.dialog.open(ExerciseDialogComponent);
    dialogRef.componentInstance.name = this.exercise.Name;
    dialogRef.componentInstance.title = this.exercise.Title;
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.exercise.Name = result.name;
        this.exercise.Title = result.title;
        this.store.dispatch(new SubstitutesActions.Save(this.exercise));
      }
    });
  }

  /**
   * Saves the exercises.
   */
  save() {
    this.store.dispatch(new SubstitutesActions.Save(this.exercise));
  }

  /**
   * Deletes the exercise.
   */
  delete() {
    const dialogRef: MatDialogRef<ConfirmDialogComponent> = this.dialog.open(ConfirmDialogComponent);
    dialogRef.afterClosed().subscribe(result => {
      if (result === "confirm") {
        this.store.dispatch(new SubstitutesActions.Delete(this.exercise));
      }
    });
  }

  /**
   * Adds a new group to the exercise.
   */
  addGroup() {
    const dialogRef: MatDialogRef<ExerciseGroupDialogComponent> = this.dialog.open(ExerciseGroupDialogComponent);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        const group = {
          label: result.label,
          value: result.value,
          color: result.color
        };
        this.exercise.addGroup(group);
        this.group = group;
      }
    });
  }

  /**
   * Removes the selected group. Any solution which includes
   * that group will be also removed.
   */
  deleteSelectedGroup() {
    if (!this.group) {
      return;
    }
    const dialogRef: MatDialogRef<ConfirmDialogComponent> = this.dialog.open(ConfirmDialogComponent);
    dialogRef.afterClosed().subscribe(result => {
      if (result === "confirm") {
        this.exercise.removeGroup(this.group);
        this.refreshSolution();
        this.group = this.exercise.Groups[0] || null;
      }
    });
  }

  /**
   * Applies the selected group to the given word.
   */
  select(word: WordItf) {
    if (!word.interactable || this.group === null) {
      return;
    }
    if (word.chain === this.group) {
      word.chain = null;
    } else {
      word.chain = this.group;
    }
    this.exercise.Solution = this.getInput();
  }

  /**
   * Selects the group the user wants to apply.
   */
  selectGroup(group: GroupItf) {
    this.group = group;
  }

  /**
   * Shows the solution.
   */
  refreshSolution() {
    const flat = SubstitutesUtils.flatten(this.paragraphs);

    // remove chains which have been deleted
    for (const word of flat) {
      if (word.chain && !this.exercise.Groups.find(c => c.value === word.chain.value)) {
        word.chain = null;
      }
    }

    for (const group in this.exercise.Solution) {
      if (this.exercise.Solution.hasOwnProperty(group)) {
        for (let i = 0; i < flat.length; i++) {
          const word: WordItf = flat[i];
          // case 1: the word is part of the solution
          // case 2: the word had this chain but is not part of the solution anymore
          if (this.exercise.Solution[group].includes(i)) {
            word.chain = this.exercise.Groups.find(c => c.value === group);
          } else if (word.chain && (word.chain.value === group)) {
            word.chain = null;
          }
        }
      }
    }
  }

  /**
   * Gets a data structure representing the player input.
   */
  private getInput(): any {
    const paragraphs = this.paragraphs;
    if (!paragraphs) {
      return [];
    }
    const res: any = {};
    const flat: WordItf[] = paragraphs.map(p => p.groups)
      .reduce((a, b) => a.concat(b), [])
      .map(g => g.words)
      .reduce((a, b) => a.concat(b), []);
    for (let i = 0; i < flat.length; i++) {
      const group: GroupItf = flat[i].chain;
      if (group) {
        if (!res[group.value]) {
          res[group.value] = [];
        }
        res[group.value].push(i);
      }
    }
    return res;
  }

  /**
   * Gets the background color of a word.
   * TODO: seems like we could REALLY optimize this method by caching the results ?
   */
  getBackgroundColor(paragraph: ParagraphItf, word: WordItf): string {
    if (this.wordIncludesAny(word, ["?", ".", ":", ","])) {
      return null;
    }
    if (word.interactable) {
      return word.chain ? word.chain.color : null;
    }
    const flat: WordItf[] = paragraph.groups.map(g => g.words)
      .reduce((a, b) => a.concat(b), []);
    const wordIndex: number = flat.indexOf(word);
    const previousIndex: number = wordIndex - 1;
    const nextIndex: number = wordIndex + 1;
    if (previousIndex >= 0 && nextIndex < flat.length) {
      const previousWord: WordItf = flat[previousIndex];
      const nextWord: WordItf = flat[nextIndex];
      if (previousWord.interactable && previousWord.chain && nextWord.interactable && previousWord.chain === nextWord.chain) {
        return previousWord.chain.color;
      }
    }
    return null;
  }

  /**
   * Switches to solution mode.
   */
  solutionMode() {
    this.paragraphs = this.exercise.getStructuredText();
    this.refreshSolution();
    this.mode = EditMode.Groups;
  }

  /**
   * Switches to write mode.
   */
  writeMode() {
    this.mode = EditMode.Write;
    this.confirmEdit = Object.keys(this.exercise.Solution).length > 0;
  }

  /**
   * Clear all the solution.
   */
  clearSolution() {
    const dialogRef: MatDialogRef<ConfirmDialogComponent> = this.dialog.open(ConfirmDialogComponent);
    dialogRef.afterClosed().subscribe(result => {
      if (result === "confirm") {
        // TODO: harmoniser
        this.exercise.clearSolution();
        this.paragraphs = this.exercise.getStructuredText();
        this.refreshSolution();
      }
    });
  }

  /**
   * Returns true if the word includes any needles given.
   */
  private wordIncludesAny(word: WordItf, needles: string[]): boolean {
    for (const needle of needles) {
      if (word.value.includes(needle)) {
        return true;
      }
    }
    return false;
  }
}

enum EditMode {
  Groups,
  Write
}
