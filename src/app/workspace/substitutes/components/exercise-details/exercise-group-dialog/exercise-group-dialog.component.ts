import {Component, OnInit} from "@angular/core";
import {MatDialogRef} from "@angular/material/dialog";

/**
 * This component provides a dialog to change the base values of an exercise.
 */
@Component({
  selector: "app-exercise-dialog",
  templateUrl: "./exercise-group-dialog.component.html",
  styleUrls: ["./exercise-group-dialog.component.scss"],
})
export class ExerciseGroupDialogComponent implements OnInit {

  label: string;

  color: string;

  colors: string[] = ["#C0392B", "#E74C3C", "#D35400", "#E67E22", "#F39C12", "#F1C40F", "#27AE60", "#2ECC71", "#1ABC9C", "#16A085", "#2980B9", "#3498DB", "#9B59B6", "#8E44AD", "#7D3C98"];

  get Value(): string {
    return (this.label || "").normalize("NFD")
      .replace(/ /gm, "-")
      .replace(/\p{P}/gu, "")
      .replace(/\p{Diacritic}/gu, "");
  }

  constructor(private dialogRef: MatDialogRef<ExerciseGroupDialogComponent>) {
  }

  ngOnInit(): void {
  }

  validate(): void {
    this.dialogRef.close({
      label: this.label,
      value: this.Value,
      color: this.color
    });
  }

  cancel(): void {
    this.dialogRef.close(null);
  }

  selectColor(color: string) {
    this.color = color;
  }
}

