import {Component} from "@angular/core";
import {Store} from "@ngrx/store";
import {ActivatedRoute, Router} from "@angular/router";
import {ExercisesListAbstract} from "../exercises-list-abstract";
import * as fromRoot from "../../../../store";

@Component({
  selector: "app-exercise-list",
  templateUrl: "./exercise-list.component.html",
  styleUrls: ["./exercise-list.component.scss"]
})
export class ExerciseListComponent extends ExercisesListAbstract {

  constructor(protected rootStore: Store<fromRoot.State>,
              protected router: Router,
              protected activatedRoute: ActivatedRoute) {
    super(rootStore, router, activatedRoute);
  }
}
