import {Component, OnInit} from "@angular/core";
import {MatDialogRef} from "@angular/material/dialog";

/**
 * This component provides a dialog to change the base values of an exercise.
 */
@Component({
  selector: "app-exercise-dialog",
  templateUrl: "./exercise-dialog.component.html",
  styleUrls: ["./exercise-dialog.component.scss"],
})
export class ExerciseDialogComponent implements OnInit {

  name: string;

  title: string;

  constructor(private dialogRef: MatDialogRef<ExerciseDialogComponent>) {
  }

  ngOnInit(): void {
  }

  validate(): void {
    this.dialogRef.close({
      name: this.name,
      title: this.title
    });
  }

  cancel(): void {
    this.dialogRef.close(null);
  }
}

