import {Component} from "@angular/core";
import {Observable} from "rxjs";
import {SubstitutesExercise} from "../../../../shared/model/substitutes/substitutes-exercise";
import * as fromExercise from "../../store";
import {SubstitutesActions} from "../../store";
import {Store} from "@ngrx/store";
import {SubstitutesState} from "../../store/reducers/substitutes.reducer";
import {MatDialog, MatDialogRef} from "@angular/material/dialog";
import {ExerciseDialogComponent} from "./exercise-dialog/exercise-dialog.component";

@Component({
  selector: "app-exercise",
  templateUrl: "./exercise.component.html",
  styleUrls: ["./exercise.component.scss"]
})
export class ExerciseComponent {

  /**
   * All the exercises.
   */
  exercises$: Observable<SubstitutesExercise[]> = this.store.select(fromExercise.getAllExercises);

  /**
   * All the exercises.
   */
  selectedExercise$: Observable<SubstitutesExercise> = this.store.select(fromExercise.getSelectedExercise);

  /**
   * Ctor.
   */
  constructor(private dialog: MatDialog,
              private store: Store<SubstitutesState>) {
  }


  /**
   * Creates a new exercise.
   */
  newExercise() {
    const dialogRef: MatDialogRef<ExerciseDialogComponent> = this.dialog.open(ExerciseDialogComponent);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        const exercise: SubstitutesExercise = new SubstitutesExercise(result.name, result.title);
        this.store.dispatch(new SubstitutesActions.Save(exercise));
      }
    });
  }
}
