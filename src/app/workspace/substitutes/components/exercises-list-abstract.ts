import {Directive, OnDestroy, OnInit} from "@angular/core";
import {Subscription} from "rxjs";
import {Store} from "@ngrx/store";
import {ActivatedRoute, Router} from "@angular/router";
import {filter, switchMap, take} from "rxjs/operators";
import * as fromRoot from "../../../store";
import * as fromSubstitutes from "../store";

@Directive()
export abstract class ExercisesListAbstract implements OnInit, OnDestroy {

  private sub: Subscription;

  /**
   * Ctor.
   */
  protected constructor(protected rootStore: Store<fromRoot.State>,
                        protected router: Router,
                        protected activatedRoute: ActivatedRoute) {
  }

  ngOnDestroy(): void {
    if (this.sub) {
      this.sub.unsubscribe();
    }
  }

  ngOnInit(): void {
    // redirect to default activity
    this.sub = this.rootStore.select(state => state.routerReducer.state.params).pipe(
      take(1),
      filter(params => !params.id),
      switchMap(_ => this.rootStore.select(fromSubstitutes.getSelectedExercise)),
      filter(ex => !!ex),
      take(1)
    ).subscribe(ex => {
      void this.router.navigate([ex.Id], {relativeTo: this.activatedRoute});
    });
  }
}
