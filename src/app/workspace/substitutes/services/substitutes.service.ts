import {Injectable} from "@angular/core";
import {HttpClient, HttpParams} from "@angular/common/http";
import {classToPlain, plainToInstance} from "class-transformer";
import {SubstitutesExercise} from "../../../shared/model/substitutes/substitutes-exercise";
import {Observable} from "rxjs";
import {environment} from "../../../../environments/environment";
import {map} from "rxjs/operators";
import {SubstitutesActivity} from "../../../shared/model/substitutes/substitutes-activity";

@Injectable({
  providedIn: "root"
})
export class SubstitutesService {

  private static readonly Endpoint = `${environment.apiEndpoint}/substitutes`;

  /**
   * Ctor.
   */
  constructor(private http: HttpClient) {
  }

  /**
   * Saves the exercise.
   * @param exercise
   */
  saveExercise(exercise: SubstitutesExercise): Observable<SubstitutesExercise> {
    if (exercise.IsNew) {
      return this.http.post(`${SubstitutesService.Endpoint}/exercise`, classToPlain(exercise)).pipe(
        map<any, SubstitutesExercise>(e => plainToInstance(SubstitutesExercise, e))
      );
    } else {
      return this.http.put(`${SubstitutesService.Endpoint}/exercise/${exercise.Id}`, classToPlain(exercise)).pipe(
        map<any, SubstitutesExercise>(e => plainToInstance(SubstitutesExercise, e))
      );
    }
  }

  /**
   * Gets the details of an exercise.
   * @param exercise
   */
  deleteExercise(exercise: SubstitutesExercise): Observable<SubstitutesExercise> {
    return this.http.delete<SubstitutesExercise>(`${SubstitutesService.Endpoint}/exercise/${exercise.Id}`).pipe(
      map(ex => exercise)
    );
  }

  /**
   * Gets the details of an exercise.
   */
  getExercise(id: number): Observable<SubstitutesExercise> {
    return this.http.get<SubstitutesExercise>(`${SubstitutesService.Endpoint}/exercise/${id}`).pipe(
      map(plain => plainToInstance(SubstitutesExercise, plain))
    );
  }

  /**
   * Gets all the user's exercises.
   */
  getExercises(): Observable<SubstitutesExercise[]> {
    return this.http.get<SubstitutesExercise[]>(`${SubstitutesService.Endpoint}/exercise`).pipe(
      map(plain => plainToInstance(SubstitutesExercise, plain))
    );
  }

  /**
   * Gets the details of an exercise.
   */
  getActivity(id: number): Observable<SubstitutesActivity> {
    return this.http.get<SubstitutesActivity>(`${SubstitutesService.Endpoint}/activity/${id}`).pipe(
      map(plain => plainToInstance(SubstitutesActivity, plain))
    );
  }

  /**
   * Gets all the user's exercises.
   */
  getActivities(exerciseId: number, start: number, limit: number): Observable<SubstitutesActivity[]> {
    return this.http.get<SubstitutesActivity[]>(`${SubstitutesService.Endpoint}/exercise/${exerciseId}/activity`,
      {params: new HttpParams().set("start", `${start}`).set("limit", `${limit}`)})
      .pipe(
        map(plain => plainToInstance(SubstitutesActivity, plain))
      );
  }

  /**
   * Gets all the user's exercises.
   */
  getLastActivities(limit: number): Observable<SubstitutesActivity[]> {
    return this.http.get<SubstitutesActivity[]>(`${SubstitutesService.Endpoint}/activity`,
      {params: new HttpParams().set("limit", `${limit}`)})
      .pipe(
        map(plain => plainToInstance(SubstitutesActivity, plain))
      );
  }
}
