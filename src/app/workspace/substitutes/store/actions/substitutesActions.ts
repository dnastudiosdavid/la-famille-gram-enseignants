import {Action} from "@ngrx/store";
import {SubstitutesExercise} from "../../../../shared/model/substitutes/substitutes-exercise";

export namespace SubstitutesActions {

  export enum ActionTypes {
    FetchAll = "[Substitutes] Fetch last",
    FetchFailed = "[Substitutes] Fetch failed",
    FetchAllDone = "[Substitutes] Fetch last done",
    Select = "[Substitutes] Select",
    SelectDone = "[Substitutes] Select done",
    Save = "[Substitutes] Save",
    SaveDone = "[Substitutes] Save done",
    SaveFailed = "[Substitutes] Save failed",
    Delete = "[Substitutes] Delete",
    DeleteDone = "[Substitutes] Delete done"
  }

  export class Save implements Action {
    readonly type = ActionTypes.Save;

    constructor(public payload: SubstitutesExercise) {
    }
  }

  export class SaveDone implements Action {
    readonly type = ActionTypes.SaveDone;

    constructor(public payload: SubstitutesExercise) {
    }
  }

  export class Delete implements Action {
    readonly type = ActionTypes.Delete;

    constructor(public payload: SubstitutesExercise) {
    }
  }

  export class DeleteDone implements Action {
    readonly type = ActionTypes.DeleteDone;

    constructor(public payload: SubstitutesExercise) {
    }
  }

  export class FetchAll implements Action {
    readonly type = ActionTypes.FetchAll;

    constructor() {
    }
  }

  export class FetchAllDone implements Action {
    readonly type = ActionTypes.FetchAllDone;

    constructor(public payload: SubstitutesExercise[]) {
    }
  }

  export class Select implements Action {
    readonly type = ActionTypes.Select;

    constructor(public payload: number) {
    }
  }

  export class SelectDone implements Action {
    readonly type = ActionTypes.SelectDone;

    constructor(public payload: SubstitutesExercise) {
    }
  }

  export class FetchFailed implements Action {
    readonly type = ActionTypes.FetchFailed;

    constructor() {
    }
  }

  export type Actions = Select | SelectDone | FetchAll | FetchAllDone | Save | SaveDone | Delete | DeleteDone | FetchFailed;
}
