import {Action} from "@ngrx/store";
import {SubstitutesActivity} from "../../../../shared/model/substitutes/substitutes-activity";

export namespace SubstitutesActivityActions {

  export enum ActionTypes {
    FetchLast = "[Substitutes activity] Fetch last",
    FetchLastDone = "[Substitutes activity] Fetch last done",
    FetchAll = "[Substitutes activity] Fetch last",
    FetchFailed = "[Substitutes activity] Fetch failed",
    FetchAllDone = "[Substitutes activity] Fetch last done",
    Select = "[Substitutes activity] Select",
    SelectDone = "[Substitutes activity] Select done",
    Load = "[Substitutes Activity] Load activities",
    LoadDone = "[Substitutes Activity] Load activities done",
  }

  export class FetchLast implements Action {
    readonly type = ActionTypes.FetchLast;

    constructor(public payload: number) {
    }
  }

  export class FetchLastDone implements Action {
    readonly type = ActionTypes.FetchLastDone;

    constructor(public payload: SubstitutesActivity[]) {
    }
  }

  export class FetchAll implements Action {
    readonly type = ActionTypes.FetchAll;

    constructor() {
    }
  }

  export class FetchAllDone implements Action {
    readonly type = ActionTypes.FetchAllDone;

    constructor(public payload: SubstitutesActivity[]) {
    }
  }

  export class Select implements Action {
    readonly type = ActionTypes.Select;

    constructor(public payload: number) {
    }
  }

  export class SelectDone implements Action {
    readonly type = ActionTypes.SelectDone;

    constructor(public payload: SubstitutesActivity) {
    }
  }

  export class FetchFailed implements Action {
    readonly type = ActionTypes.FetchFailed;

    constructor() {
    }
  }

  export class Load implements Action {
    readonly type = ActionTypes.Load;

    constructor(public exerciseId: number, public start: number, public limit: number) {
    }
  }

  export class LoadDone implements Action {
    readonly type = ActionTypes.LoadDone;

    constructor(public payload: SubstitutesActivity[]) {
    }
  }

  export type Actions = FetchLast | FetchLastDone | Select | SelectDone | FetchAll | FetchAllDone | FetchFailed | Load | LoadDone;
}
