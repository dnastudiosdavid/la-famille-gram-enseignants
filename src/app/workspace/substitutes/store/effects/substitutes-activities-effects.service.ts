import {Injectable} from "@angular/core";
import {LoadingService} from "../../../../shared/loading-spinner/loading.service";
import {Actions, createEffect, ofType} from "@ngrx/effects";
import {Observable, of} from "rxjs";
import {Action, Store} from "@ngrx/store";
import {catchError, filter, map, switchMap, tap, withLatestFrom} from "rxjs/operators";
import {SubstitutesActions, SubstitutesActivityActions} from "../actions";
import {AbstractEffects} from "../../../effects-abstract";
import {MatSnackBar} from "@angular/material/snack-bar";
import {SubstitutesService} from "../../services/substitutes.service";
import {SubstitutesState} from "../reducers/substitutes.reducer";
import {SubstitutesActivity} from "../../../../shared/model/substitutes/substitutes-activity";
import {ActivityActions} from "../../../sentences/activity/store";
import {ExerciseActions} from "../../../sentences/exercises/store/exercise.actions";


@Injectable()
export class SubstitutesActivityEffects extends AbstractEffects {

  /**
   * This effect gets the last activities.
   */
  getLastActivities$: Observable<Action> = createEffect(() => this.actions$
    .pipe(
      ofType(SubstitutesActivityActions.ActionTypes.FetchLast),
      switchMap((action: SubstitutesActivityActions.FetchLast) => {
        this.open("substitutes-activities");
        return this.substitutesService.getLastActivities(action.payload);
      }),
      map((activities: SubstitutesActivity []) => {
        this.close("substitutes-activities");
        return new SubstitutesActivityActions.FetchLastDone(activities);
      })
    ));

  /**
   * Gets all the activities.
   */
  getActivities$: Observable<Action> = createEffect(() => this.actions$
    .pipe(
      ofType(SubstitutesActivityActions.ActionTypes.Load),
      tap(_ => this.open("substitutes-activities")),
      switchMap((action: SubstitutesActivityActions.Load) => this.substitutesService.getActivities(action.exerciseId, action.start, action.limit)),
      map((activities: SubstitutesActivity[]) => {
        this.close("substitutes-activities");
        return new SubstitutesActivityActions.LoadDone(activities);
      }),
      catchError((_, caught) => {
        this.close("substitutes-activities", "Erreur", false);
        return caught;
      })
    ));

  /**
   * Gets an activity.
   */
  getActivity$: Observable<Action> = createEffect(() => this.actions$
    .pipe(
      ofType(SubstitutesActivityActions.ActionTypes.Select),
      switchMap((action: SubstitutesActivityActions.Select) => {
        this.open("substitutes-activity");
        return this.substitutesService.getActivity(action.payload);
      }),
      map((activity: SubstitutesActivity) => {
        this.close("substitutes-activity");
        return new SubstitutesActivityActions.SelectDone(activity);
      }),
      catchError(() => {
        this.close("substitutes-activity", "Erreur", false);
        return of(new SubstitutesActivityActions.FetchFailed());
      })
    ));

  constructor(private substitutesService: SubstitutesService,
              protected loadingService: LoadingService,
              protected snackBar: MatSnackBar,
              private actions$: Actions,
              private store$: Store<SubstitutesState>) {
    super(loadingService, snackBar);
  }
}
