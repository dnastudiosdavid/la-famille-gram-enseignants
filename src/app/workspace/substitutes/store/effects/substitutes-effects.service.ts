import {Injectable} from "@angular/core";
import {LoadingService} from "../../../../shared/loading-spinner/loading.service";
import {Actions, createEffect, ofType} from "@ngrx/effects";
import {Observable, of} from "rxjs";
import {Action, Store} from "@ngrx/store";
import {catchError, map, switchMap, tap} from "rxjs/operators";
import {SubstitutesActions} from "../actions";
import {AbstractEffects} from "../../../effects-abstract";
import {MatSnackBar} from "@angular/material/snack-bar";
import {SubstitutesService} from "../../services/substitutes.service";
import {SubstitutesExercise} from "../../../../shared/model/substitutes/substitutes-exercise";
import {SubstitutesState} from "../reducers/substitutes.reducer";

@Injectable()
export class SubstitutesEffects extends AbstractEffects {

  /**
   * This effect uses the exercise service to save the
   * exercise from the action.
   */

  saveExercises$: Observable<Action> = createEffect(() => this.actions$
    .pipe(
      ofType(SubstitutesActions.ActionTypes.Save),
      switchMap((action: SubstitutesActions.Save) => {
        this.open("substitutes-exercise");
        return this.substitutesService.saveExercise(action.payload);
      }),
      map((exercise: SubstitutesExercise) => {
        this.close("substitutes-exercise", "Opération réussie!");
        return new SubstitutesActions.SaveDone(exercise);
      }),
      catchError(() => {
        this.close("substitutes-exercise", "Erreur", false);
        return of(new SubstitutesActions.FetchFailed());
      })
    ));

  /**
   * This effect fetches the details of an exercise.
   */

  deleteExercise$: Observable<Action> = createEffect(() => this.actions$
    .pipe(
      ofType(SubstitutesActions.ActionTypes.Delete),
      switchMap((action: SubstitutesActions.Delete) => {
        this.open("substitutes-exercise");
        return this.substitutesService.deleteExercise(action.payload);
      }),
      map((exercise: SubstitutesExercise) => {
        this.close("substitutes-exercise", "Suppression effectuée!");
        return new SubstitutesActions.DeleteDone(exercise);
      }),
      catchError(() => {
        this.close("substitutes-exercise", "Erreur", false);
        return of(null);
      })
    ));

  /**
   * Gets all the exercises.
   */

  getExercises$: Observable<Action> = createEffect(() => this.actions$
    .pipe(
      ofType(SubstitutesActions.ActionTypes.FetchAll),
      tap(_ => this.open("substitutes-exercises")),
      switchMap(_ => this.substitutesService.getExercises()),
      map((exercises: SubstitutesExercise[]) => {
        this.close("substitutes-exercises");
        return new SubstitutesActions.FetchAllDone(exercises);
      }),
      catchError(() => {
        this.close("substitutes-exercise", "Erreur", false);
        return of(null);
      })
    ));

  /**
   * Gets an exercise.
   */

  getExercise$: Observable<Action> = createEffect(() => this.actions$
    .pipe(
      ofType(SubstitutesActions.ActionTypes.Select),
      switchMap((action: SubstitutesActions.Select) => {
        this.open("substitutes-exercise");
        return this.substitutesService.getExercise(action.payload);
      }),
      map((exercise: SubstitutesExercise) => {
        this.close("substitutes-exercise");
        return new SubstitutesActions.SelectDone(exercise);
      }),
      catchError(() => {
        this.close("substitutes-exercise", "Erreur", false);
        return of(new SubstitutesActions.FetchFailed());
      })
    ));

  /**
   * Ctor.
   */
  constructor(private substitutesService: SubstitutesService,
              protected loadingService: LoadingService,
              protected snackBar: MatSnackBar,
              private actions$: Actions,
              private store$: Store<SubstitutesState>) {
    super(loadingService, snackBar);
  }
}
