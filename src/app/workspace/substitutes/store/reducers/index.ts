import {ActionReducerMap, createFeatureSelector} from "@ngrx/store";
import * as fromSubstitutes from "./substitutes.reducer";

export interface SubstitutesFeatureState {
  exercise: fromSubstitutes.SubstitutesState;
}

export const reducers: ActionReducerMap<SubstitutesFeatureState> = {
  exercise: fromSubstitutes.substitutesReducer
};

export const getSubstitutesFeatureState = createFeatureSelector<SubstitutesFeatureState>("substitutesFeature");
