import {SubstitutesActions, SubstitutesActivityActions} from "../actions";
import {SubstitutesExercise} from "../../../../shared/model/substitutes/substitutes-exercise";
import {plainToInstance} from "class-transformer";
import {SubstitutesActivity} from "../../../../shared/model/substitutes/substitutes-activity";

export interface SubstitutesState {
  exercises: SubstitutesExercise[];
  selectedExercise: SubstitutesExercise;
  activities: SubstitutesActivity[];
  selectedActivity: SubstitutesActivity;
  lastActivities: SubstitutesActivity[];
}

export const initialState: SubstitutesState = {
  exercises: [],
  selectedExercise: null,
  activities: [],
  selectedActivity: null,
  lastActivities: []
};

export function substitutesReducer(state = initialState, action: SubstitutesActions.Actions | SubstitutesActivityActions.Actions): SubstitutesState {

  switch (action.type) {
    case SubstitutesActions.ActionTypes.Select:
      return {
        ...state,
        activities: [],
        selectedExercise: null
      };

    case SubstitutesActions.ActionTypes.SelectDone:
      return {
        ...state,
        selectedExercise: action.payload
      };

    case SubstitutesActions.ActionTypes.FetchAll:
      return state;

    case SubstitutesActions.ActionTypes.FetchAllDone:
      return {
        ...state,
        exercises: action.payload,
        selectedExercise: state.selectedExercise ?? action.payload[0]
      };

    case SubstitutesActions.ActionTypes.Save:
      return state;

    case SubstitutesActions.ActionTypes.SaveDone:
      const itemExists: boolean = !!state.exercises.find(ex => ex.Id === action.payload.Id);
      if (!itemExists) {
        return {
          ...state,
          selectedExercise: action.payload,
          exercises: [
            ...state.exercises,
            action.payload
          ]
        };
      } else {
        return {
          ...state,
          selectedExercise: action.payload,
          exercises: [...state.exercises.map(ex => ex.Id === action.payload.Id ? action.payload : plainToInstance(SubstitutesExercise, {...ex}))]
        };
      }

    case SubstitutesActions.ActionTypes.Delete:
      return state;

    case SubstitutesActions.ActionTypes.DeleteDone:
      return {
        ...state,
        selectedExercise: null,
        exercises: [...state.exercises.filter(e => e.Id !== action.payload.Id)]
      };

    /** -------- Activities -------- **/
    case SubstitutesActivityActions.ActionTypes.FetchLastDone:
      return {
        ...state,
        lastActivities: action.payload
      };

    case SubstitutesActivityActions.ActionTypes.SelectDone:
      return {
        ...state,
        selectedActivity: action.payload
      };

    case SubstitutesActivityActions.ActionTypes.Load:
      return state;

    case SubstitutesActivityActions.ActionTypes.LoadDone:
      return {
        ...state,
        activities: [...state.activities || [], ...action.payload]
      };

    default:
      return state;
  }
}

export const getAllExercises = (state: SubstitutesState) => state.exercises;
export const getSelectedExercise = (state: SubstitutesState) => state.selectedExercise;
export const getAllActivities = (state: SubstitutesState) => state.activities;
export const getSelectedActivity = (state: SubstitutesState) => state.selectedActivity;
export const getLastActivities = (state: SubstitutesState) => state.lastActivities;
