import {createSelector} from "@ngrx/store";
import * as fromFeature from "../reducers";
import * as fromSubstitutes from "../reducers/substitutes.reducer";

export const getExerciseState = createSelector(
  fromFeature.getSubstitutesFeatureState,
  (state: fromFeature.SubstitutesFeatureState) => state.exercise
);

export const getAllExercises = createSelector(
  getExerciseState,
  fromSubstitutes.getAllExercises
);

export const getAllActivities = createSelector(
  getExerciseState,
  fromSubstitutes.getAllActivities
);

export const getSelectedExercise = createSelector(
  getExerciseState,
  fromSubstitutes.getSelectedExercise
);

export const getSelectedActivity = createSelector(
  getExerciseState,
  fromSubstitutes.getSelectedActivity
);

export const getLastActivities = createSelector(
  getExerciseState,
  fromSubstitutes.getLastActivities
);
