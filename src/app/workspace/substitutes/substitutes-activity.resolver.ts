import {Injectable} from "@angular/core";

import {Store} from "@ngrx/store";
import {first} from "rxjs/operators";

import * as fromRoot from "../../store";
import * as fromSubstitutes from "./store";

@Injectable()
export class SubstitutesActivitiesResolver {


  /**
   * Ctor.
   * @param store
   */
  constructor(private store: Store<fromRoot.State>) {
  }

  resolve() {
    return this.store.select(state => state)
      .pipe(first())
      .subscribe(id => this.store.dispatch(new fromSubstitutes.SubstitutesActivityActions.FetchAll()));
  }
}

@Injectable()
export class SubstitutesActivityResolver {

  /**
   * Ctor.
   * @param store
   */
  constructor(private store: Store<fromRoot.State>) {
  }

  resolve() {
    return this.store.select(state => state.routerReducer.state.params)
      .pipe(first())
      .subscribe(params => {
        const activityId = params.activity;
        const exerciseId = params.id;
        this.store.dispatch(new fromSubstitutes.SubstitutesActivityActions.Select(parseInt(activityId, 10)));
        this.store.dispatch(new fromSubstitutes.SubstitutesActions.Select(parseInt(exerciseId, 10)));
      });
  }
}
