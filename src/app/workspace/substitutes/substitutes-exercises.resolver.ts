import {Injectable} from "@angular/core";

import {Store} from "@ngrx/store";
import {filter, take} from "rxjs/operators";

import * as fromRoot from "../../store";
import * as fromSubstitutes from "./store";

@Injectable()
export class SubstitutesExercisesResolver {

  /**
   * Ctor.
   * @param store
   */
  constructor(private store: Store<fromRoot.State>) {
  }

  resolve() {
    return this.store.select(state => state)
      .pipe(take(1))
      .subscribe(id => this.store.dispatch(new fromSubstitutes.SubstitutesActions.FetchAll()));
  }
}

@Injectable()
export class SubstitutesExerciseResolver {

  /**
   * Ctor.
   * @param store
   */
  constructor(private store: Store<fromRoot.State>) {
  }

  resolve() {
    return this.store.select(state => state.routerReducer.state.params.id)
      .pipe(filter(id => id), take(1))
      .subscribe(id => this.store.dispatch(new fromSubstitutes.SubstitutesActions.Select(parseInt(id, 10))));
  }
}
