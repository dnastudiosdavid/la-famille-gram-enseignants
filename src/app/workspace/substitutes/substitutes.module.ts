import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {ActivityListComponent} from "./components/activity-list/activity-list.component";
import {ActivityDetailsComponent} from "./components/activity-details/activity-details.component";
import {ExerciseDetailsComponent} from "./components/exercise-details/exercise-details.component";
import {ExerciseListComponent} from "./components/exercise-list/exercise-list.component";
import {ActivityComponent} from "./components/activity/activity.component";
import {ExerciseComponent} from "./components/exercise/exercise.component";
import {RouterModule} from "@angular/router";
import {SharedModule} from "../../shared/shared.module";
import {StoreModule} from "@ngrx/store";
import {reducers, SubstitutesEffects} from "./store";
import {EffectsModule} from "@ngrx/effects";
import {SubstitutesExerciseResolver, SubstitutesExercisesResolver} from "./substitutes-exercises.resolver";
import {FormsModule} from "@angular/forms";
import {ExerciseDialogComponent} from "./components/exercise/exercise-dialog/exercise-dialog.component";
import {AppMaterialModule} from "../../app-material.module";
import {ExerciseGroupDialogComponent} from "./components/exercise-details/exercise-group-dialog/exercise-group-dialog.component";
import {SubstitutesActivitiesResolver, SubstitutesActivityResolver} from "./substitutes-activity.resolver";
import {SubstitutesActivityEffects} from "./store/effects/substitutes-activities-effects.service";

@NgModule({
  declarations: [
    ActivityComponent,
    ExerciseComponent,
    ActivityListComponent,
    ActivityDetailsComponent,
    ExerciseDetailsComponent,
    ExerciseListComponent,
    ExerciseDialogComponent,
    ExerciseGroupDialogComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
    SharedModule,
    StoreModule.forFeature("substitutesFeature", reducers),
    EffectsModule.forFeature([SubstitutesEffects, SubstitutesActivityEffects]),
    AppMaterialModule,
    FormsModule
  ],
  providers: [
    SubstitutesExerciseResolver,
    SubstitutesActivityResolver,
    SubstitutesActivitiesResolver,
    SubstitutesExercisesResolver
  ]
})
export class SubstitutesModule {
}
