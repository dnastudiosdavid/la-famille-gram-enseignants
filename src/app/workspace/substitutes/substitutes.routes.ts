import {Routes} from "@angular/router";
import {ExerciseComponent} from "./components/exercise/exercise.component";
import {ExerciseListComponent} from "./components/exercise-list/exercise-list.component";
import {ExerciseDetailsComponent} from "./components/exercise-details/exercise-details.component";
import {ActivityComponent} from "./components/activity/activity.component";
import {ActivityListComponent} from "./components/activity-list/activity-list.component";
import {SubstitutesExerciseResolver, SubstitutesExercisesResolver} from "./substitutes-exercises.resolver";
import {SubstitutesActivityResolver} from "./substitutes-activity.resolver";
import {ActivityDetailsComponent} from "./components/activity-details/activity-details.component";

export const SUBSTITUTES_ROUTES: Routes = [
  {
    path: "exercise", component: ExerciseComponent, children: [
      {path: "", component: ExerciseListComponent},
      {path: ":id", component: ExerciseDetailsComponent, resolve: {data: SubstitutesExerciseResolver}}
    ], resolve: {data: SubstitutesExercisesResolver}
  },
  {
    path: "activity", component: ActivityComponent, children: [
      {
        path: ":id", component: ActivityListComponent, pathMatch: "full", resolve: {data: SubstitutesExerciseResolver}
      },
      {
        path: ":id/details/:activity", component: ActivityDetailsComponent, resolve: {data: SubstitutesActivityResolver}
      }
    ], resolve: {data: SubstitutesExercisesResolver}
  }
];
