import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {MainComponent} from "./main/main.component";
import {RouterModule} from "@angular/router";
import {DashboardModule} from "./dashboard/dashboard.module";
import {AppMaterialModule} from "../app-material.module";
import {SubstitutesModule} from "./substitutes/substitutes.module";
import {ExercisesModule} from "./sentences/exercises/exercises.module";
import {ActivityModule} from "./sentences/activity/activity.module";

@NgModule({
  declarations: [MainComponent],
  imports: [
    CommonModule,
    RouterModule,
    DashboardModule,
    ExercisesModule,
    ActivityModule,
    SubstitutesModule,
    AppMaterialModule
  ],
  exports: [
    MainComponent
  ]
})
export class WorkspaceModule {
}
