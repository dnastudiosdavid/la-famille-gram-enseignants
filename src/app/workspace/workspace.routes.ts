import {Routes} from "@angular/router";
import {MainComponent} from "./main/main.component";
import {DASHBOARD_ROUTES} from "./dashboard/dashboard.routes";
import {SUBSTITUTES_ROUTES} from "./substitutes/substitutes.routes";
import {ACTIVITY_ROUTES} from "./sentences/activity/activity.routes";
import {EXERCISES_ROUTES} from "./sentences/exercises/exercises.routes";

export const WORKSPACE_ROUTES: Routes = [
  {
    path: "", component: MainComponent, children: [
      {path: "", redirectTo: "dashboard", pathMatch: "full"},
      {path: "dashboard", children: DASHBOARD_ROUTES},
      {path: "exercises", children: EXERCISES_ROUTES},
      {path: "activity", children: ACTIVITY_ROUTES},
      {path: "substitutes", children: SUBSTITUTES_ROUTES},
    ]
  }
];
