Le soleil se levait.
Dans leur trou, au pied de la colline, les Ratinos terminaient leur nuit. Tout à coup un tremblement de terre les secoua comme feuilles au vent et les réveilla en sursaut. Le plus audacieux alla jusqu'à l'entrée pour voir d'où venait cette secousse.
« Coucou », lui fit un éléphant qui le regardait gentiment.
Les Ratinos sortirent pour examiner cette montagne de chair qui avait ébranlé leur demeure.
« Toi, tu n'es pas d'ici, lui dirent-ils, tu nous déranges, tu ferais mieux de rentrer chez toi. »
L'éléphant fit la sourde oreille.
Comme tous les jours, les Ratinos se rendirent au point d'eau pour se désaltérer. La grosse bête les suivit et d'une seule goulée avala tout le liquide.
« Bonjour la soif ! », s'écrièrent les Ratinos scandalisés.  Puis l'éléphant fit pipi et alors là, bonjour l'inondation !
« Horreur, horreur, s'indignèrent les arrosés, rentre chez toi.»
