Un singe demeurait dans un grand arbre sur la rive d'un fleuve. Dans ce fleuve il y avait beaucoup de crocodiles. Une maman crocodile observa les singes pendant un long moment puis un jour dit à son fils : « Mon fils, va m'attraper un de ces singes. Je veux un cœur de singe pour mon repas. »
Le bébé crocodile lui demanda : « Comment ferai-je pour attraper un singe ? Je ne vais pas à terre et le singe ne vient pas dans l'eau. »
Sa mère lui répondit : « Fais travailler ton cerveau et tu trouveras un moyen. »
Le petit crocodile réfléchit longtemps, longtemps. À la fin il se dit : « Je sais ce que je vais faire. Je vais attraper le singe qui vit dans le grand arbre sur la rive du fleuve. Il rêve de traverser le fleuve pour aller manger les fruits bien mûrs de l'île. »
Le crocodile nagea jusqu'à l'arbre du singe. Mais il n'était pas très futé. Il appela : « Bonjour le Singe, viens avec moi jusqu'à l'île aux fruits mûrs. »
« Comment puis-je t'accompagner ? » lui demanda le singe. « Je ne sais pas nager. »
« Non, mais moi je sais. Je te prendrai sur mon dos, » lui répondit-il.
Le singe était gourmand et voulait les fruits mûrs, aussi sauta-t-il sur le dos du crocodile.
« En route ! » dit le crocodile.
« Quelle chouette balade tu m'offres là ! » lui dit le singe.
« Tu crois ? Alors, tu aimes ça ? » lui demanda-t-il en plongeant.
« Oh, non ! » cria le singe en s'enfonçant dans l'eau. Il avait peur de lâcher prise car il ne savait pas comment faire sous l'eau.
Quand le crocodile remonta à la surface, le singe lui demanda en suffoquant et en crachant : « Crocodile, pourquoi m'as-tu emmené sous l'eau ? »
« Je vais te tuer en te maintenant sous l'eau » lui répondit le crocodile. « Ma mère veut manger du cœur de singe et je vais lui apporter le tien. »
« Tu aurais dû me le dire que tu voulais mon cœur, » lui dit le singe, « je l'aurais peut-être emporté avec moi. »
« Pas possible ! » dit le crocodile pas futé. « Tu veux dire que tu l'as laissé là-haut dans l'arbre ? »
« Et oui, c’est ça ! » lui répondit le singe. « Si tu veux mon cœur, il faut retourner le chercher sur l'arbre. Mais on est juste à côté de l'île aux fruits mûrs, amène-moi là-bas en premier. »
Mais le crocodile lui répondit : « Non, le singe, je te ramène à ton arbre directement. Ne nous occupons pas des fruits mûrs. Attrape ton coeur et rapporte-le moi tout de suite. On verra après pour l'île. »
« Très bien,» lui dit le singe.
Mais à peine eut-il sauté sur la rive, Pshitt, qu’il se précipita dans l'arbre ; et, de la plus haute branche, il appela le crocodile dans l'eau au-dessous de lui : « Mon cœur est tout là-haut ; si tu le veux, viens le chercher, viens donc le chercher ! »
