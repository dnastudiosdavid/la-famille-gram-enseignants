export const environment = {
  production: true,
  authEndpoint: "https://api.la-famille-gram.ch/oauth/v2/token",
  apiEndpoint: "https://api.la-famille-gram.ch/api",
  languagesEndpoint: "https://api.la-famille-gram.ch/languages.json",
  legacyEndpoint: "https://api-legacy.la-famille-gram.ch/api/v1"
};
