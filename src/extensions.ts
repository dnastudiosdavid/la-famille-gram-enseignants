export {};

declare global {

  interface Array<T> {

    /**
     * Gets a random item from the array.
     */
    random(): T;

    /**
     * Shuffles the array and returns it. This method
     * does change the original array.
     */
    shuffle(): T[];

    /**
     * Returns true if the array contains the given item.
     */
    contains(el: T): boolean;

    /**
     * Removes the given item and returns true if the
     * item has been found and was deleted.
     */
    remove(el: T);
  }
}

/**
 * Simply returns a random item from the array using its length.
 */
if (!Array.prototype.random) {
  Array.prototype.random = function <T>(this: T[]): T {
    if (this.length === 0) {
      return null;
    }
    return this[Math.floor(Math.random() * this.length)];
  };
}

/**
 * Randomly shuffles the array. This method mutates the original array.
 */
if (!Array.prototype.shuffle) {
  Array.prototype.shuffle = function <T>(this: T[]): T[] {
    let j;
    let x;
    let i;
    for (i = this.length - 1; i > 0; i--) {
      j = Math.floor(Math.random() * (i + 1));
      x = this[i];
      this[i] = this[j];
      this[j] = x;
    }
    return this;
  };
}

/**
 * Returns true if the array contains the given item. Uses the
 * indexOf method - no other search is performed.
 */
if (!Array.prototype.contains) {
  Array.prototype.contains = function <T>(this: T[], el: T): boolean {
    return this.indexOf(el) !== -1;
  };
}

/**
 * Removes the item from the array and returns true if the
 * removal was successful. Uses indexOf to search for the
 * item.
 */
if (!Array.prototype.remove) {
  Array.prototype.remove = function <T>(this: any[], el: T): boolean {
    const i = this.indexOf(el);
    if (i !== -1) {
      this.splice(i, 1);
      return true;
    }
    return false;
  };
}

